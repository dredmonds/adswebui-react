
# Gathers CI Metadata during job
VERSION=$(shell echo ${CI_COMMIT_SHA})
APP_PATH=$(shell echo ${CI_PROJECT_PATH})
APP_NAME=$(shell echo ${CI_PROJECT_NAME})
LC_APP_PATH=$(shell echo ${CI_PROJECT_PATH} | tr '[:upper:]' '[:lower:]')
LC_APP_NAME=$(shell echo ${CI_PROJECT_NAME} | tr '[:upper:]' '[:lower:]')

# ECR
AWS_ACCOUNT=066738799188
AWS_CLI_REGION=us-west-2
DOCKER_REPO=$(AWS_ACCOUNT).dkr.ecr.$(AWS_CLI_REGION).amazonaws.com

# HELP
# This will output the help for each task
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

# DOCKER TASKS
# Build the container
build: ## Build the container
	docker build -t $(LC_APP_PATH) .

build.net: ## Build the container for .Net Core Projects
	docker build -t $(LC_APP_PATH) . -f $(APP_NAME)/Dockerfile

build-nc: ## Build the container without caching
	docker build --no-cache -t $(LC_APP_PATH) .

release: build publish ## Make a release by building and publishing the `{version}` ans `latest` tagged containers to ECR

# Docker publish
publish: repo-login publish-latest publish-version ## Publish the `{version}` ans `latest` tagged containers to ECR

publish-latest: tag-latest ## Publish the `latest` taged container to ECR
	@echo 'publish latest to $(DOCKER_REPO)'
	docker push $(DOCKER_REPO)/$(LC_APP_PATH):latest

publish-version: tag-version ## Publish the `{version}` taged container to ECR
	@echo 'publish $(VERSION) to $(DOCKER_REPO)'
	docker push $(DOCKER_REPO)/$(LC_APP_PATH):$(VERSION)

# Docker tagging
tag: tag-latest tag-version ## Generate container tags for the `{version}` and `latest` tags

tag-latest: ## Generate container `{version}` tag
	@echo 'create tag latest'
	docker tag $(LC_APP_PATH) $(DOCKER_REPO)/$(LC_APP_PATH):latest

tag-version: ## Generate container `latest` tag
	@echo 'create tag $(VERSION)'
	docker tag $(LC_APP_PATH) $(DOCKER_REPO)/$(LC_APP_PATH):$(VERSION)

# HELPERS

# generate script to login to aws docker repo
CMD_REPOLOGIN := "eval $$\( aws ecr"
ifdef AWS_CLI_PROFILE
CMD_REPOLOGIN += " --profile $(AWS_CLI_PROFILE)"
endif
ifdef AWS_CLI_REGION
CMD_REPOLOGIN += " --region $(AWS_CLI_REGION)"
endif
CMD_REPOLOGIN += " get-login --no-include-email \)"

# login to AWS-ECR
repo-login: ## Auto login to AWS-ECR unsing aws-cli
	@eval $(CMD_REPOLOGIN)

version: ## Output the current version
	@echo $(VERSION)
	