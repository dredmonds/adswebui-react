# Stage 1 - the build process
FROM node:8.9.4 as build-deps
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
COPY webpack.config.js /src/webpack.config.js
RUN yarn
COPY . ./
RUN yarn build

# Stage 2 - the production environment
FROM nginx:1.12-alpine

LABEL Company="AJW Group"
LABEL Application="ADS Frontend-React"
LABEL Maintainer="Edmond de los Reyes"
LABEL Dept="Digital Engineering"
LABEL Version="v2.0"
LABEL Description="This docker image includes the required \
dependencies to run ADS Frontend"

COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

#Local Development:
#
# docker build -t ads-frontend .
# docker run -it -p 8080:80 -v ${PWD}:/usr/share/nginx/html ads-frontend