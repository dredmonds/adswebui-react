// src/reducers/pbhorder.reducer.js

import { pbhOrderConstants } from '../constants';

export function pbhorder(state = {}, action) {
  switch (action.type) {
    // PBH Orders
    case pbhOrderConstants.PBH_ORDERS_REQUEST:
      return {retrievingPBH: true};
    case pbhOrderConstants.PBH_ORDERS_SUCCESS:
      return {orders: action.orders};
    case pbhOrderConstants.PBH_ORDERS_FAILURE:
      return {};

    // PBH Order Details
    case pbhOrderConstants.PBH_ORDER_DETAILS_REQUEST:
      return {retrievingPBHD: true};
    case pbhOrderConstants.PBH_ORDER_DETAILS_SUCCESS:
      return {order: action.orders};
    case pbhOrderConstants.PBH_ORDER_DETAILS_FAILURE:
      return {};

    // PBH Orders Search Query 
    case pbhOrderConstants.PBH_ORDERS_SEARCH_REQUEST:
      return {searchingPBH: true};
    case pbhOrderConstants.PBH_ORDERS_SEARCH_SUCCESS:
      return {orders: action.orders};
    case pbhOrderConstants.PBH_ORDERS_SEARCH_FAILURE:
      return {};
      
    default:
      return state
  }
}