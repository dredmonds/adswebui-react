// src/reducers/authentication.reducer.js

import { userConstants } from '../constants';
import config from 'config';

let user = JSON.parse(sessionStorage.getItem(`ads.user.id.key${config.adsMsalConfig.clientID}`));
const initialState = user ? { loggedIn: true, user } : {} ;

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {loggingIn: true, user: action.user};
    case userConstants.LOGIN_SUCCESSS:
      return {loggedIn: false, user: action.user};
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:
      return {};

    case userConstants.MSAL_LOGIN_REQUEST:
      return {msalLoggingIn: true, user: action.msalLogin };
    case userConstants.MSAL_LOGIN_SUCCESSS:
      return {msalLoggingIn: false, user: action.msalUser };
    case userConstants.MSAL_LOGIN_FAILURE:
      return {};
      
    default:
      return state
  }
}