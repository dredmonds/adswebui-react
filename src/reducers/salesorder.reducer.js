// src/reducers/salesorder.reducer.js

import { salesOrderConstants } from '../constants';

export function salesorder(state = {}, action) {
  switch (action.type) {
    // Sales Orders
    case salesOrderConstants.SALES_ORDERS_REQUEST:
      return {retrievingSO: true};
    case salesOrderConstants.SALES_ORDERS_SUCCESS:
      return {orders: action.orders};
    case salesOrderConstants.SALES_ORDERS_FAILURE:
      return {};

    // Sales Order Details
    case salesOrderConstants.SALES_ORDER_DETAILS_REQUEST:
      return {retrievingSOD: true};
    case salesOrderConstants.SALES_ORDER_DETAILS_SUCCESS:
      return {order: action.order};
    case salesOrderConstants.SALES_ORDER_DETAILS_FAILURE:
      return {};

    // Sales Orders Search Query 
    case salesOrderConstants.SALES_ORDERS_SEARCH_REQUEST:
      return {searchingSO: true};
    case salesOrderConstants.SALES_ORDERS_SEARCH_SUCCESS:
      return {orders: action.orders};
    case salesOrderConstants.SALES_ORDERS_SEARCH_FAILURE:
      return {};
      
    default:
      return state
  }
}