// src/reducers/challenged.reducer.js

import { userConstants } from '../constants';

export function challenged(state = {}, action) {
  switch (action.type) {
    case userConstants.CHALLENGE_REQUEST:
      return {
        challenging: true,
        user: action.user
      };
    case userConstants.CHALLENGE_SUCCESS:
      return {};
    case userConstants.CHALLENGE_FAILURE:
      return {};
    default:
      return state
  }
}