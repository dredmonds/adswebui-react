// src/services/salesorder.service.js

import config from 'config';
import client from '../helpers/client';

export const salesOrderService = {
  getSalesOrders,
  getSalesOrderDetails,
  getSalesOrderSearch,
  searchOrders,
  searchOrdersList,
  getOrdersList,
  getOrdersData,
  searchInvoiced,
  searchInvoicedList,
  //searchShipment,
  getShipmentData,
  //searchStock,
  getShipmentStockImages,
  //searchShipmentImagesAndDocs,
  getShipmentImagePresignedURL,
};

function getSalesOrders() {
  return new Promise(function(resolve, reject) {
    client.get(`${config.API}/api/Order`).then(result => {
      return resolve (result.data);
    }, error => {
      return reject (error);
    });
  });
}

function getSalesOrderDetails(id) {
  return new Promise(function(resolve, reject) {
    client.get(`${config.API}/api/SalesOrder/${id}`).then(result => {
      return resolve (result.data);
    }, error => {
      return reject (error);
    });
  });
}

function getSalesOrderSearch(query) {
  return new Promise(function(resolve, reject) {
    client.post(`${config.API}/api/SalesOrderFilter`).then(result => {
      return resolve (result.data);
    }, error => {
      return reject (error);
    });
  });
}

/** 
 * ODATA Entity Data Queries
*/

function searchOrders(strQryA, objNameQryA, strQryB, objNameQryB, rows, skip) {
  let objNameA = `${objNameQryA}`, objNameB = `${objNameQryB}`, rowNums = `${rows}`, skipRows = `${skip}`;
  let queryString = objNameA != 'SaleOrderDetailAutoKey' ? 
                    `${config.API}/odata/order?$filter=contains(toupper(${objNameA}), toupper('${strQryA}')) eq true and ${objNameB} eq ${strQryB}&$orderby=dateCreated desc&$top=${rowNums}&$skip=${skipRows}&$count=true` : 
                    `${config.API}/odata/order?$filter=${objNameA} eq ${strQryA}&$orderby=dateCreated desc&$top=${rowNums}&$skip=${skipRows}&$count=true` ;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

function searchOrdersList(strQryA, objNameQryA, rows, skip) {
  let objNameA = `${objNameQryA}`, rowNums = `${rows}`, skipRows = `${skip}`;
  let queryString = objNameA != 'SaleOrderDetailAutoKey' ? 
                    `${config.API}/odata/order?$filter=contains(toupper(${objNameA}), toupper('${strQryA}')) eq true` : 
                    `${config.API}/odata/order?$filter=${objNameA} eq ${strQryA}` ;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

function getOrdersList(strQryA, objNameQryA, rows, skip) {
  let objNameA = `${objNameQryA}`, rowNums = `${rows}`, skipRows = `${skip}`;
  let queryString = objNameA != 'SaleOrderDetailAutoKey' ? 
                    `${config.API}/odata/order?$filter=contains(toupper(${objNameA}), toupper('${strQryA}')) eq true&$orderby=dateCreated desc&$top=${rowNums}&$skip=${skipRows}&$count=true&$expand=shipment,stock` : 
                    `${config.API}/odata/order?$filter=${objNameA} eq ${strQryA}&$orderby=dateCreated desc&$top=${rowNums}&$skip=${skipRows}&$count=true&$expand=shipment,stock` ;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

function getOrdersData(strQuery) {
  let queryString = `${config.API}/odata/order?$filter=${strQuery}`;
  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      return resolve ({data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

function searchInvoiced(strQryA, objNameQryA, rows, skip) {
  let objNameA = `${objNameQryA}`, rowNums = `${rows}`, skipRows = `${skip}`;
  let queryString = objNameA != 'SaleOrderDetailAutoKey' ? 
                    `${config.API}/odata/order?$filter=${objNameA} ge ${strQryA}&$orderby=dateCreated desc&$top=${rowNums}&$skip=${skipRows}&$count=true` : 
                    `${config.API}/odata/order?$filter=${objNameA} eq ${strQryA}&$orderby=dateCreated desc&$top=${rowNums}&$skip=${skipRows}&$count=true` ;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

function searchInvoicedList(strQryA, objNameQryA, rows, skip) {
  let objNameA = `${objNameQryA}`, rowNums = `${rows}`, skipRows = `${skip}`;
  let queryString = objNameA == 'invocieNumber' && strQryA != '' ?
                    `${config.API}/odata/shipment?$filter=contains(toupper(${objNameA}), toupper('${strQryA}')) eq true&$orderby=shipDate desc&$top=${rowNums}&$skip=${skipRows}&$count=true` :
                    `${config.API}/odata/shipment?$orderby=shipDate desc&$top=${rowNums}&$skip=${skipRows}&$count=true` ;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

function searchShipment(strQryA, objNameQryA, rows, skip) {
  let objNameA = `${objNameQryA}`, rowNums = `${rows}`, skipRows = `${skip}`;
  let queryString = (objNameA != 'saleOrderDetailAutoKey' && objNameA != 'StockReservationsAutoKey') ? 
                    `${config.API}/odata/shipment?$filter=contains(toupper(${objNameA}), toupper('${strQryA}')) eq true` : 
                    `${config.API}/odata/shipment?$filter=${objNameA} eq ${strQryA}` ;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

function getShipmentData(strQuery) {
  let queryString = `${config.API}/odata/shipment?$filter=${strQuery}`;
  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      return resolve ({data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

function searchStock(strQryA, objNameQryA, rows, skip) {
  let objNameA = `${objNameQryA}`, rowNums = `${rows}`, skipRows = `${skip}`;
  let queryString = objNameA != 'saleOrderDetailAutoKey' ? 
                    `${config.API}/odata/stock?$filter=contains(toupper(${objNameA}), toupper('${strQryA}')) eq true` : 
                    `${config.API}/odata/stock?$filter=${objNameA} eq ${strQryA}` ;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

function getShipmentStockImages(srcPkQryStr, srcTableQryStr, imgCodeQryStr) {
  let queryString = `${config.API}/odata/quantumimages?$filter=sourcePk in (${srcPkQryStr}) and sourceTable in (${srcTableQryStr}) and imageCodeAutoKey in (${imgCodeQryStr})`;
  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      return resolve ({data: result.data.value});
    }, error => {
      return reject (error);
    });
  });
}

/** 
 * ADS Entity Data Queries
*/

function searchShipmentImagesAndDocs(sourceTable, sourcePk) {
  return new Promise(function(resolve, reject) {
    client.get(`${config.API}/api/Images/SourceTableAndPk?sourceTable=${sourceTable}&sourcePk=${sourcePk}`).then(result => {
      return resolve (result.data);
    }, error => {
      return reject (error);
    });
  });
}

function getShipmentImagePresignedURL(GuId) {
  return new Promise((resolve, reject) => {
    client.get(`${config.API}/api/Images/PresignedURL?Guid=${GuId}`).then(result => {
      return resolve (result.data);
    }, error => {
      return reject (error);
    });
  });
}