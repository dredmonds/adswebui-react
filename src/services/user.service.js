// src/services/user.service.js

import config from 'config';
import { authHeader } from '../helpers';

export const userService = {
  login,
  logout,
  register,
  getIdentity,
  challengedUser,
  getAll,
  getById,
  update,
  delete: _delete
};

function login(username, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username, password })
  };

  return fetch(`${config.AUTH}/Auth`, requestOptions).then(handleResponse)
          .then(user => {
            // validates "accesstoken" from http 200 response
            if(user.result.accessToken) {
              sessionStorage.clear();
              // store auth details in local storage
              sessionStorage.setItem(`ads.auth.token.key${config.adsMsalConfig.clientID}`, JSON.stringify(user.result));
              // Get Identity
              return getIdentity();
            }
            return user.result;
          });
}

function logout() {
  // remove user from local storage to log out
  sessionStorage.removeItem(`ads.auth.token.key${config.adsMsalConfig.clientID}`);
  sessionStorage.removeItem(`ads.user.id.key${config.adsMsalConfig.clientID}`);
}

function register(user) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(user)
  };

  return fetch(`${config.AUTH}/Register`, requestOptions).then(handleResponse);
}

function getIdentity() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${config.API}/api/Identity`, requestOptions).then(handleResponse)
          .then(identity => {
            return identity;
          });
}

function challengedUser(ans) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json'},
    body: JSON.stringify(ans)
  };

  return fetch(`${config.AUTH}/Challenge`, requestOptions).then(handleResponse);
}

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${config.API}/api/Users`, requestOptions).then(handleResponse);
}

function getById(id) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${config.API}/api/Users/${id}`, requestOptions).then(handleResponse);
}

function update(user) {
  const requestOptions = {
    method: 'PUT',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(user)
  };

  return fetch(`${config.API}/api/Users/${user.id}`, requestOptions).then(handleResponse);
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader()
  };

  return fetch(`${config.API}/api/Users/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if(!response.ok) {
      if(response.status === 401) {
        // auto logout if 401 response returned from api
        logout();
        location.reload(true);
      }
      if(response.status === 404) {
        // response.statusText = 'Not found'
        const error = (data && data.result.resultDetailMetaData.Message) || response.statusText;
        return Promise.reject(error);
      }

      const error = (data && data.result.resultDetail) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}