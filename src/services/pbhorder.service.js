// src/services/pbhorder.service.js

import config from 'config';
import client from '../helpers/client';

export const pbhOrderService = {
  searchPBHOrders,
  getPBHOrdersList,
  getAircraftRegistrationList,
  getShipToList,
  searchPBHOrderByQuery,
  getPBHActions,
  postPbhAction,
  getUploadedImageUrl
};

/**
 * ODATA Entity Data Queries
 */
function searchPBHOrderByQuery(strQryA, objNameQryA, rows, skip) {
  let objNameA = `${objNameQryA}`, rowNums = `${rows}`, skipRows = `${skip}`;
  let isIntDataTypeObj = objNameA == 'ExchangeRequestId' ? true :
                         objNameA == 'companyAutoKey' ? true :
                         objNameA == 'priority' ? true 
                                                : false;
  let queryString = isIntDataTypeObj == true ? 
                    `${config.API}/odata/pbhmain?$filter=${objNameA} eq ${strQryA}&$orderby=creationDate desc&$top=${rowNums}&$skip=${skipRows}&$count=true` :
                    `${config.API}/odata/pbhmain?$filter=contains(toupper(${objNameA}), toupper('${strQryA}')) eq true&$orderby=creationDate desc&$top=${rowNums}&$skip=${skipRows}&$count=true` ;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    });
  }, error => {
    return reject(error);
  });
}

function getPBHOrdersList(rows, skip) {
  let rowNums = `${rows}`, skipRows = `${skip}`;
  let queryString = `${config.API}/odata/pbhmain?$orderby=creationDate desc&$top=${rowNums}&$skip=${skipRows}&$count=true` ;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    });
  }, error => {
    return reject(error);
  });
}

function getAircraftRegistrationList() {
  let queryString = `${config.API}/odata/pbhmain`;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve({count: count[1], data: result.data.value});
    });
  }, error => {
    return reject(error);
  });
}

function getShipToList(companyAutoKey) {
   let queryString = `${config.API}/odata/companysites?$filter=companyAutoKey eq ${companyAutoKey}`;
  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve({count: count[1], data: result.data.value});
    });
  }, error => {
    return reject(error);
  });
}

function searchPBHOrders(id) {
  // let queryString = `${config.API}/api/Orders/` + id;
  let queryString = `https://action.api.next.cloud.ajw-group.com/api/Orders/` + id;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // Order by date time desc
      result.data.actions.sort((a, b) => (a.actionTime > b.actionTime) ? 1 : -1).reverse()
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data.value});
    })
    .catch(error => {
      if(error.response != undefined )
      {
        window.alert('Sorry there has been an error.\nThe following exception has occured: ' + error.response.statusText + ' \nError code: ' +  error.response.status + '\n' + error.response.data);        
      }
      else
      {
        window.alert('Sorry there has been an error.');
      }
      reject();
    })
  });
}

function getPBHActions(id) {
  // let queryString = `${config.API}/api/Orders/` + id;
  let queryString = `https://action.api.next.cloud.ajw-group.com/api/Orders/` + id + `/Actions`;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      // hack way to get "@odata.count"
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data});
    })
    .catch(error => {
      console.log(error);
      reject();
    });      
  }, error => {
    return reject(error);
  });
}

function getUploadedImageUrl(id, fileKey) {
  let queryString = `https://action.api.next.cloud.ajw-group.com/api/Orders/` + id + `/DownloadFile?fileKey=` + fileKey;

  return new Promise((resolve, reject) => {
    client.get(`${queryString}`).then(result => {
      let count = Object.keys(result.data).map(e => {return result.data[e]});
      return resolve ({count: count[1], data: result.data});
    });
  }, error => {
    return reject(error);
  });
}

function postPbhAction(queryString, object) {
  return new Promise((resolve, reject) => {
    // setTimeout(() => {
    //   window.alert('Timeout');
    // }, 5000);

    return client.post(`${queryString}`, object).then(result => {
		  let count = Object.keys(result.data).map(e => {return result.data[e]});
		  return resolve({count: count[1], data: result.data});
    })
    .catch(error => {
      if(error.response != undefined )
      {
        window.alert('Sorry there has been an error.\nThe following exception has occured: ' + error.response.statusText + ' \nError code: ' +  error.response.status + '\n' + error.response.data);        
      }
      else
      {
        window.alert('Sorry there has been an error.\nThe following exception has occured: ' + error.message);
      }
      reject();
    })
  });
}