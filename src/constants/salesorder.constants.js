// src/constants/salesorder.constant.js

export const salesOrderConstants = {
  SALES_ORDERS_REQUEST: 'SALES_ORDERS_REQUEST',
  SALES_ORDERS_SUCCESS: 'SALES_ORDERS_SUCCESS',
  SALES_ORDERS_FAILURE: 'SALES_ORDERS_FAILURE',

  SALES_ORDER_DETAILS_REQUEST: 'SALES_ORDER_DETAILS_REQUEST',
  SALES_ORDER_DETAILS_SUCCESS: 'SALES_ORDER_DETAILS_SUCCESS',
  SALES_ORDER_DETAILS_FAILURE: 'SALES_ORDER_DETAILS_FAILURE',

  SALES_ORDERS_SEARCH_REQUEST: 'SALES_ORDERS_SEARCH_REQUEST',
  SALES_ORDERS_SEARCH_SUCCESS: 'SALES_ORDERS_SEARCH_SUCCESS',
  SALES_ORDERS_SEARCH_FAILURE: 'SALES_ORDERS_SEARCH_FAILURE'
};