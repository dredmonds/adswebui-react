// src/constants/pbhorder.constant.js

export const pbhOrderConstants = {
  PBH_ORDERS_REQUEST: 'PBH_ORDERS_REQUEST',
  PBH_ORDERS_SUCCESS: 'PBH_ORDERS_SUCCESS',
  PBH_ORDERS_FAILURE: 'PBH_ORDERS_FAILURE',

  PBH_ORDER_DETAILS_REQUEST: 'PBH_ORDER_DETAILS_REQUEST',
  PBH_ORDER_DETAILS_SUCCESS: 'PBH_ORDER_DETAILS_SUCCESS',
  PBH_ORDER_DETAILS_FAILURE: 'PBH_ORDER_DETAILS_FAILURE',

  PBH_ORDERS_SEARCH_REQUEST: 'PBH_ORDERS_SEARCH_REQUEST',
  PBH_ORDERS_SEARCH_SUCCESS: 'PBH_ORDERS_SEARCH_SUCCESS',
  PBH_ORDERS_SEARCH_FAILURE: 'PBH_ORDERS_SEARCH_FAILURE'
};