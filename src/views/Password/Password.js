import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Container, Form, FormFeedback, Row, Col, Card, CardBody, Button, Input, InputGroup, InputGroupAddon, InputGroupText} from 'reactstrap';

import { userActions } from '../../actions';

class PasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      confirmPassword: '',
      submitted: false
    };

    // Binds methods of events
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNavLink = this.handleNavLink.bind(this);
  }

  // Methods of events
  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ submitted: true });
    const { password, confirmPassword } = this.state;
    const { dispatch } = this.props;
    if(password && confirmPassword) {
      dispatch( userActions.submitPassword(password, confirmPassword) );
    }
  }

  handleNavLink() {
    this.props.history.push('/login');
  }

  render() {
    const { challenging } = this.props;
    const { password, confirmPassword, submitted } = this.state;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form name="registerForm" id="registerForm" onSubmit={this.handleSubmit}>
                    <h1>New Password</h1>
                    <p className="text-muted">Create your new password</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" name="password" id="password" placeholder="Password" invalid={submitted && !password} value={password} onChange={this.handleChange} />
                      <FormFeedback>Password is required</FormFeedback>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password" invalid={submitted && !confirmPassword} value={confirmPassword} onChange={this.handleChange} />
                      <FormFeedback>Confirmed password is required</FormFeedback>
                    </InputGroup>

                    <Row>
                      <Col xs="12" sm="6">
                        <Button color="primary" block>
                          {challenging && <i className="fa fa-spinner fa-pulse fa-fw" />}
                          Submit Password
                        </Button>
                      </Col>
                      <Col xs="12" sm="6">
                        <Button onClick={this.handleNavLink} color="danger" block>Cancel</Button>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }

}

function mapStateToProps(state) {
  const { challenging } = state.challenged;
  return { challenging };
}

const connectedPasswordPage = connect(mapStateToProps)(PasswordPage);
export default connectedPasswordPage;