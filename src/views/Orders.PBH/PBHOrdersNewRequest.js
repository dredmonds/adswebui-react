import React, { Component } from "react";
import {connect } from "react-redux";
import { FormErrors } from './Actions/FormErrors';
import {pbhOrderService} from '../../services';
import { Row, Col, InputGroup, InputGroupAddon, InputGroupText, Form, FormGroup, Label, Input, FormFeedback, ButtonGroup, Button, Popover, ListGroup, ListGroupItem } from "reactstrap";
import {pbhOrdersActions} from '../../actions';
import config from 'config';

class PBHOrdersNewRequest extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      aircraftRegList: [],
      contractList: [],
      shipToList: [],
      shipTo: {},
      shippingPriorityList: [],
      shipDropdownOpen: false,

      shippingPriority: '0',
      nextFlightDateTime: '',
      aircraftRegistration: '',
      contractId: '',
      purchaseOrderNo: '',
      requestPartNumber: '',
      dateRequired: '',
      partRemovalReason: '',
      partRemovalReasonOther: '',
      contactName: this.props.user.name,
      emailAddress: this.props.user.email,
      destinationName: '',
      streetAddress: '',
      streetAddress2: '',
      streetAddress3: '',
      city: '',
      postCode: '',
      countryCode: '',
      
      shippingPriorityValid: false,
      nextFlightDateTimeValid: true,
      aircraftRegistrationValid: false,
      purchaseOrderNoValid: false,
      requestPartNumberValid: false,
      dateRequiredValid: false,
      partRemovalReasonValid: false,
      partRemovalReasonOtherValid: true,
      contactNameValid: true,
      emailAddressValid: true,
      destinationNameValid: false,
      streetAddressValid: false,
      streetAddress2Valid: true,
      streetAddress3Valid: true,
      cityValid: false,
      postCodeValid: false,
      countryCodeValid: false,
      contractListValid: false,
      formValid: false,
      formErrors: {shippingPriority: '', nextFlightDateTime: '', aircraftRegistration: '',purchaseOrderNo: '',requestPartNumber: '',requestPartNumber: '',dateRequired: '',dateRequired: '',partRemovalReason: '', partRemovalReasonOther: '',contactName: '',emailAddress: '',destinationName: '',streetAddress: '',streetAddress2: '',streetAddress3: '',city: '',postCode: '',countryCode: '', contractList: ''},
      
      shipPriority: [
        { code: null, value: "Please select" },
        { code: 1, value: "AOG" },
        { code: 2, value: "Imminent AOG/Critical" },
        { code: 2, value: "IOR/MEL Deferred Defects" },
        { code: 4, value: "Replenishment of MBK" },
        { code: 5, value: "Routine" }
      ]
    };
    this.getShipToList = this.getShipToList.bind(this);
  }

  
  postNewPbhOrder(e) {
    e.preventDefault();
    var newOrderRequestDetails = {
      "companyId": this.props.user.companyId,
      "customerPonumber": this.state.purchaseOrderNo,
      "contactName": `${this.state.contactName}`,
      "contactEmail": this.state.emailAddress,
      "priority": this.state.shippingPriority,
      "dateNeeded": this.state.dateRequired,
      "removalReason": this.getPartRemovalReason(),
      "partNumber": this.state.requestPartNumber,
      "acReg": this.state.aircraftRegistration,
      "contractId": this.state.contractId,
      "destinationName": this.state.destinationName,
      "destinationAddr1": this.state.streetAddress,
      "destinationAddr2": this.state.streetAddress2,
      "destinationAddr3": this.state.streetAddress3,
      "destinationCity": this.state.city,
      "destinationPostCode": this.state.postCode,
      "destinationCountryCode": this.state.countryCode,
      "nextFlightDateTime": this.state.nextFlightDateTime,
      "destinationMetaData": "1.00"
    };
    
    this.props.pbhDataViewToggleTab('1');
    this.props.clearPbhOrdersAndActions();
    
    var queryString = `https://ads.api.next.cloud.ajw-group.com/api/ExchangeRequests`;
    var pbhAction= pbhOrderService.postPbhAction(queryString, newOrderRequestDetails);
        pbhAction.then(pbhAction => {
          let dataRowsCache = config.baseDataGridRows * 3; //set buffer to 3x from baseDataGridRows
          this.props.dispatch(pbhOrdersActions.getPBHOrders(dataRowsCache));

          var pbhActions= pbhOrderService.getPBHActions(pbhAction.data.id);
          this.props.getPbhActionPromiseResult(pbhActions);
          this.props.setExchangeRequestId(pbhAction.data.id);
          var pbhOderResult= pbhOrderService.searchPBHOrders(pbhAction.data.id);
          this.props.getPbhOrderPromiseResult(pbhOderResult);
        }).catch(e => {
          alert("An error has occured, please try again.");
          console.log(e);
        });
  }
  
  getPartRemovalReason(){    
    if(this.state.partRemovalReason != 'Other')
    {
      return this.state.partRemovalReason;
    }
    else
    {
      return this.state.partRemovalReasonOther;
    }
  };

  
  handleUserInput(e){
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},() => { this.validateField(name, value) });
  };

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let shippingPriorityValid = this.state.shippingPriorityValid;
    let nextFlightDateTimeValid = this.state.nextFlightDateTimeValid;
    let aircraftRegistrationValid= this.state.aircraftRegistrationValid;
    let purchaseOrderNoValid = this.state.purchaseOrderNoValid;
    let requestPartNumberValid = this.state.requestPartNumberValid;
    let dateRequiredValid= this.state.dateRequiredValid;
    let partRemovalReasonValid = this.state.partRemovalReasonValid;
    let partRemovalReasonOtherValid = this.state.partRemovalReasonOtherValid;
    let contactNameValid = this.state.contactNameValid;
    let emailAddressValid = this.state.emailAddressValid;
    let destinationNameValid = this.state.destinationNameValid;
    let streetAddressValid = this.state.streetAddressValid;
    let streetAddress2Valid = this.state.streetAddress2Valid;
    let streetAddress3Valid = this.state.streetAddress3Valid;
    let cityValid = this.state.cityValid;
    let postCodeValid = this.state.postCodeValid;
    let countryCodeValid = this.state.countryCodeValid;
    let formValid= this.state.formValid;
    let contractListValid= this.state.contractListValid;

    switch(fieldName) {
      case 'shippingPriority':
        shippingPriorityValid = this.notSelectedValidation(this.state.shippingPriority)
        // 1 = AOG
        if(this.state.shippingPriority == '1')
        {
          nextFlightDateTimeValid = this.notNullValidation(this.state.nextFlightDateTime)
          fieldValidationErrors.nextFlightDateTimeValid = nextFlightDateTimeValid ? '' : 'Please select the next flight date.';
        }
        else
        {
          this.setState({nextFlightDateTime: ''});
        }
        fieldValidationErrors.shippingPriorityValid = shippingPriorityValid ? '' : 'Please select shipping priority.';
        break;
      case 'nextFlightDateTime':
        nextFlightDateTimeValid = this.notNullValidation(this.state.nextFlightDateTime)
        fieldValidationErrors.nextFlightDateTimeValid = nextFlightDateTimeValid ? '' : 'Please select the next flight date.';
        break;
      case 'contractId':
        contractListValid = this.notSelectedValidation(this.state.contractId)
          fieldValidationErrors.contractListValid = contractListValid ? '' : 'Please select contract from the list.';
          break;
        case 'aircraftRegistration':
            aircraftRegistrationValid = this.notSelectedValidation(this.state.aircraftRegistration)
            fieldValidationErrors.aircraftRegistrationValid = aircraftRegistrationValid ? '' : 'Please select an aircraft registration.';
        break;
        case 'purchaseOrderNo':
            purchaseOrderNoValid = this.notNullValidation(this.state.purchaseOrderNo)
            fieldValidationErrors.purchaseOrderNoValid = purchaseOrderNoValid ? '' : 'Please enter a valid purchase order No.';
        break;
        case 'requestPartNumber':
            requestPartNumberValid = this.notNullValidation(this.state.requestPartNumber)
            fieldValidationErrors.requestPartNumberValid = requestPartNumberValid ? '' : 'Please enter a valid part number.';
        break;
        case 'dateRequired':
            dateRequiredValid = this.notNullValidation(this.state.dateRequired)
            fieldValidationErrors.dateRequiredValid = dateRequiredValid ? '' : 'Please enter a valid date.';
        break;
        case 'partRemovalReason':
            partRemovalReasonValid = this.notSelectedValidation(this.state.partRemovalReason)
            if(this.state.partRemovalReason == 'Other')
            {
              partRemovalReasonOtherValid = this.notNullValidation(this.state.partRemovalReasonOther);
              fieldValidationErrors.partRemovalReasonOtherValid = partRemovalReasonOtherValid ? '' : 'Please enter a valid part removal reason.';
            }
            fieldValidationErrors.partRemovalReasonValid = partRemovalReasonValid ? '' : 'Please enter a valid part removal reason.';
        break;
        case 'partRemovalReasonOther':
            partRemovalReasonOtherValid = this.notNullValidation(this.state.partRemovalReasonOther)
            fieldValidationErrors.partRemovalReasonOtherValid = partRemovalReasonOtherValid ? '' : 'Please enter a valid part removal reason.';
        break;
        case 'contactName':
            contactNameValid = this.notNullValidation(this.state.contactName)
            fieldValidationErrors.contactNameValid = contactNameValid ? '' : 'Please enter a valid contact name.';
        break;
        case 'emailAddress':
              emailAddressValid = this.notNullValidation(this.state.emailAddress)
              if (!emailAddressValid)
              {
                fieldValidationErrors.emailAddressValid = 'Please enter a email address.';
                break;
              }

              emailAddressValid = this.emailRegexValidation(this.state.emailAddress)
              fieldValidationErrors.emailAddressValid = emailAddressValid ? '' : 'Please enter a valid email address.';
        break;
        case 'destinationName':
            destinationNameValid = this.notNullValidation(this.state.destinationName)
            fieldValidationErrors.destinationNameValid = destinationNameValid ? '' : 'Please enter a valid destination name.';
        break;
        case 'streetAddress':
            streetAddressValid = this.notNullValidation(this.state.streetAddress)
            fieldValidationErrors.streetAddressValid = streetAddressValid ? '' : 'Please enter a valid street address.';
        break;
        case 'city':
            cityValid = this.notNullValidation(this.state.city);
            fieldValidationErrors.cityValid = cityValid ? '' : 'Please enter a valid city.';
        break;
        case 'postCode':
            postCodeValid = this.notNullValidation(this.state.postCode)
            fieldValidationErrors.postCodeValid = postCodeValid ? '' : 'Please enter a valid post code.';
        break;
        case 'countryCode':
        countryCodeValid = this.notNullValidation(this.state.countryCode)
        if (!countryCodeValid)
        {
          fieldValidationErrors.countryCodeValid = countryCodeValid ? '' : 'Please enter a country code.';
          break;
        }
        countryCodeValid = this.lengthValidation(this.state.countryCode, 2);
        if (!countryCodeValid)
        {
          fieldValidationErrors.countryCodeValid = countryCodeValid ? '' : 'Please enter a valid country code. (2 characters in length)';
          break;
        }
        countryCodeValid = this.alphaNumericValidation(this.state.countryCode);
        fieldValidationErrors.countryCodeValid = countryCodeValid ? '' : 'Please enter a valid country code. (alpha numeric characters)';
        break;
      default:
        break;
    }
    this.setState({formErrors: fieldValidationErrors, shippingPriorityValid: shippingPriorityValid,aircraftRegistrationValid : aircraftRegistrationValid ,purchaseOrderNoValid : purchaseOrderNoValid ,requestPartNumberValid : requestPartNumberValid ,dateRequiredValid : dateRequiredValid,partRemovalReasonValid : partRemovalReasonValid,partRemovalReasonOtherValid : partRemovalReasonOtherValid,contactNameValid : contactNameValid ,emailAddressValid :emailAddressValid ,destinationNameValid :destinationNameValid ,streetAddressValid :streetAddressValid ,streetAddress2Valid :streetAddress2Valid ,streetAddress3Valid :streetAddress3Valid ,cityValid :cityValid ,postCodeValid :postCodeValid ,countryCodeValid :countryCodeValid , contractListValid: contractListValid, formValid :formValid}, this.validateForm);
  }

  alphaNumericValidation(value){
    var regExpression = new RegExp(/^[a-z0-9]+$/i);
    var isAlphaNumeric = regExpression.test(value);    
    return isAlphaNumeric;
  }

  lengthValidation(value, length){
    return value.length == length;
  }

  notNullValidation(value){
    return value.length != null && value.length != 0;
  }

  notSelectedValidation(value){
    return value != "Please select";
  }

  numericRegexValidation(value){
    var regExpression = new RegExp(/^[0-9]+$/i);
    var isNumeric = regExpression.test(value);
    return isNumeric;
  }

  emailRegexValidation(value){
    var regExpression = new RegExp(/^[A-Za-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?/);
    var isNumeric = regExpression.test(value);
    return isNumeric;
  }

  alphaRegexValidation(value){
    var regExpression = new RegExp(/^[a-z]+$/i);
    var isNumeric = regExpression.test(value);
    return isNumeric;
  }

  errorClass(error) {
    return(error == undefined || error.length === 0 ? '' : 'has-error');
  }

  validateForm() {
    var valid = (this.state.shippingPriorityValid && this.state.nextFlightDateTimeValid && this.state.aircraftRegistrationValid && this.state.purchaseOrderNoValid && this.state.requestPartNumberValid && this.state.dateRequiredValid && this.state.partRemovalReasonValid && this.state.partRemovalReasonOtherValid && this.state.contactNameValid && this.state.emailAddressValid && this.state.destinationNameValid && this.state.streetAddressValid && this.state.streetAddress2Valid && this.state.streetAddress3Valid && this.state.cityValid && this.state.postCodeValid && this.state.countryCodeValid && this.state.contractListValid);
    this.setState({formValid: valid});
  }

  componentDidMount() {
    this.getAircraftRegistionList();
    this.getShipToList();
    this.getContractList();
  }

  shipDropdownToggle() {
    this.setState({
      shipDropdownOpen: !this.state.shipDropdownOpen
    });
  }

  getAircraftRegistionList() {
    // populated dummy for aircraftRegList
    // todo - create and implement API to populate this data.
    const aircraftRegList = [
      { code: null, value: "Please select" },
      { code: 0, value: "VH-VNB" },
      { code: 1, value: "VH-VNC" },
      { code: 2, value: "VH-VND" }
    ];
    this.setState({ aircraftRegList: aircraftRegList });
  }

  getContractList() {
    const contractList = [
      { code: null, value: "Please select" },
    ];
    this.props.user.company.companyContracts.forEach(contract => {
      contractList.push({ code: contract.contractId, value: contract.contract.name},)
    });

    this.setState({ contractList: contractList });
  }

  getShipToList() {
    const shipToListHc = [];
    var companyAutoKey = this.props.user.company.companyAutoKey
    var shipToList= pbhOrderService.getShipToList(companyAutoKey);
    shipToList.then(shipTo => {
      shipTo.data.map((companySite) => (
        shipToListHc.push
        ({
          destinationName: companySite.siteDescription,
          destinationAddr1: companySite.address1,
          destinationAddr2: companySite.address2,
          destinationAddr3: companySite.address3,
          destinationCity: companySite.city,
          destinationPostCode: companySite.zipcode,
          destinationCountryCode: companySite.countryCode
        })
      ))
    });
    this.setState({ shipToList: shipToListHc });
  }

  setShipTo(item) {
      this.setState({destinationName: item.destinationName, 
                      streetAddress: item.destinationAddr1, 
                      streetAddress2: item.destinationAddr2, 
                      streetAddress3: item.destinationAddr3, 
                      city: item.destinationCity, 
                      postCode: item.destinationPostCode, 
                      countryCode: item.destinationCountryCode, 
                      destinationNameValid: true, 
                      streetAddressValid: true, 
                      streetAddress2Valid: true, 
                      streetAddress3Valid: true, 
                      cityValid: true, 
                      postCodeValid: true, 
                      countryCodeValid: true },
                      this.validateForm);
  }

  pbhNewOrderDetailViewForm() {
    // todo - create and implement API to populate this data.
    const partRemovalReasons = [
      { code: null, value: "Please select" },
      { code: "On Condition", value: "On Condition" },
      { code: "Scheduled", value: "Scheduled" },
      { code: "Check", value: "Check" },
      { code: "Other", value: "Other (Please enter reason below)" }
    ];
    const shipToForm = () => {
      return (
        <div name="newShipToForm">
          <Row name="contactNameEmailRow">
            <Col>
              <Label for="contactName">
                <small>Contact Name</small>
              </Label>
              <InputGroup className="mb-4">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="icon-user" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="contactName" bsSize="sm" name="contactName" value={this.state.contactName} onChange={(e) => this.handleUserInput(e)}/>
                <FormFeedback>Contact Person is required</FormFeedback>
              </InputGroup>
            </Col>
            <Col>
              <Label for="contactEmail">
                <small>Email Address</small>
              </Label>
              <InputGroup className="mb-4">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fa fa-envelope-o" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="emailAddress" bsSize="sm" name="emailAddress" value={this.state.emailAddress} onChange={(e) => this.handleUserInput(e)}/>

                <FormFeedback>Email Address is required</FormFeedback>
              </InputGroup>
            </Col>
          </Row>

          <Row name="destinationNameRow">
            <Col>
              <Label for="destinationName">
                <small>Destination Name</small>
              </Label>
              <InputGroup className="mb-4">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fa fa-university" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="destinationName" bsSize="sm" name="destinationName" value={this.state.destinationName} onChange={(e) => this.handleUserInput(e)}/>
                <FormFeedback>Destination Name is required</FormFeedback>
              </InputGroup>
            </Col>
          </Row>

          <Row name="destinationAddressRow">
            <Col name="rightColAddress">
              <Label for="destinationAddr1">
                <small>Street Address</small>
              </Label>
              <InputGroup className="mb-4">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fa fa-map-marker" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="destinationAddr1" bsSize="sm" placeholder="Street Address" name="streetAddress" value={this.state.streetAddress} onChange={(e) => this.handleUserInput(e)}/>
                <FormFeedback>Street Address is required</FormFeedback>
              </InputGroup>

              <Label for="destinationAddr2">
                <small>Street Address 2</small>
              </Label>
              <InputGroup className="mb-4">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fa fa-map-marker" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="destinationAddr2" bsSize="sm" placeholder="Street Address 2 (optional)" name="streetAddress2" value={this.state.streetAddress2} onChange={(e) => this.handleUserInput(e)}/>
              </InputGroup>

              <Label for="destinationAddr3">
                <small>Street Address 3</small>
              </Label>
              <InputGroup className="mb-4">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fa fa-map-marker" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="destinationAddr3" bsSize="sm" placeholder="Street Address 3 (optional)" name="streetAddress3" value={this.state.streetAddress3} onChange={(e) => this.handleUserInput(e)}/>
              </InputGroup>
            </Col>

            <Col name="leftColAddress">
              <Label for="destinationCity">
                <small>City</small>
              </Label>
              <InputGroup className="mb-4">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fa fa-map-marker" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="destinationCity" bsSize="sm" placeholder="City" name="city" value={this.state.city} onChange={(e) => this.handleUserInput(e)}/>
                <FormFeedback>City is required</FormFeedback>
              </InputGroup>

              <Label for="destinationPostCode">
                <small>Post Code</small>
              </Label>
              <InputGroup className="mb-4">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fa fa-map-marker" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="destinationPostCode" bsSize="sm" placeholder="Post Code" name="postCode" value={this.state.postCode} onChange={(e) => this.handleUserInput(e)}/>
                <FormFeedback>Post Code is required</FormFeedback>
              </InputGroup>

              <Label for="destinationCountryCode">
                <small>Country Code</small>
              </Label>
              <InputGroup className="mb-4">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fa fa-globe" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="destinationCountryCode" bsSize="sm" placeholder="Country Code" name="countryCode" value={this.state.countryCode} onChange={(e) => this.handleUserInput(e)}/>
                <FormFeedback>Country Code is required</FormFeedback>
              </InputGroup>
            </Col>
          </Row>
          <Row>
            <Col style={{ textAlign: "center" }}>
              <button className="btn btn-primary column col-5" type="submit" disabled={!this.state.formValid}>Submit</button>
            </Col>
          </Row>
        </div>
      );
    };

    const shipToDropdown = () => {
      return (
        <ButtonGroup name="shipToDropdown">
          <Button id="shipToPopover" color="link" style={{ padding: "0.175rem 0.55rem" }} onClick={this.shipDropdownToggle.bind(this)} >
            <div className="text-center">
              <i className="fa fa-angle-down" />
            </div>
          </Button>
          <Popover placement="bottom" target="shipToPopover" isOpen={this.state.shipDropdownOpen} toggle={this.shipDropdownToggle.bind(this)} onClick={this.shipDropdownToggle.bind(this)} >
            <ListGroup flush>
              {this.state.shipToList.map((item, index) => (
                <ListGroupItem tag="a" key={index} value={index} onClick={() => this.setShipTo(item)} action >
                  <div className="text-left">
                    <small>{`${item.destinationName}, ${ item.destinationAddr1 }, ${item.destinationCity}, ${ item.destinationCountryCode }`}</small>
                  </div>
                </ListGroupItem>
              ))}
            </ListGroup>
          </Popover>
        </ButtonGroup>
      );
    };

    return (
      <form id="form" className="form-group columns col-mx-auto container"  onSubmit={(e) => this.postNewPbhOrder(e)}>
      <div name="pbhOrderFormTemplate">
        <Form name="pbhOrderForm" id="pbhOrderForm">
          <FormGroup>
            <Row>
              <Col lg="12">
                <div className={`form-group ${this.errorClass(this.state.formErrors)}`}>
                  <div className="panel panel-default">
                    <FormErrors formErrors={this.state.formErrors} />
                  </div>
                </div>
              </Col>
            </Row>
            <Row name="partsDetailsHeader">
              <Col>
                <div className="form-header parts">
                  <h5>Parts Request Details</h5>
                </div>
              </Col>
            </Row>
            <Row>
              <Col name="rightFormColumn">
                <div>
                  <div style={this.state.shippingPriority == '1' ? {width:'50%',float:'left'} : {width:'100%' }}>
                <Label for="shippingPriority">
                  <small>Shipping Priority</small>
                </Label>
                <InputGroup className="mb-4">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-plus" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="select" id="shippingPriority" name="shippingPriority" bsSize="sm" onChange={(e) => this.handleUserInput(e)}>
                    {this.state.shipPriority.map(item => (
                      <option key={item.value} value={item.code}>
                        {item.value}
                      </option>
                    ))}
                  </Input>
                  <FormFeedback>Shipping Priority is required</FormFeedback>
                </InputGroup>
                </div>
                {/* 1 = AOG */}
                {this.state.shippingPriority == '1' && 
                <div style={{ marginLeft:'10px', float:'left', width:'45%'}}>
                <Label for="nextFlightDateTime">
                  <small>Next flight date/time</small>
                </Label>
                <InputGroup className="mb-4">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-plus" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="date" name="nextFlightDateTime" id="nextFlightDateTime" bsSize="sm" name="nextFlightDateTime" value={this.state.nextFlightDateTime} onChange={(e) => this.handleUserInput(e)}/>
                  <FormFeedback>Next flight date/time is required</FormFeedback>
                </InputGroup>
                </div>
                }
              </div>

                <Label for="purchaseOrder">
                  <small>Purchase Order No.</small>
                </Label>
                <InputGroup className="mb-4">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-list-ul" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" id="purchaseOrder" name="purchaseOrderNo" bsSize="sm" placeholder="Purchase Order No.(Auto-generated)" value={this.state.purchaseOrderNo} onChange={(e) => this.handleUserInput(e)}/>
                  <FormFeedback>Purchase Order No. is required</FormFeedback>
                </InputGroup>

                <Label for="dateRequired">
                  <small>Date Required</small>
                </Label>
                <InputGroup className="mb-4">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-calendar" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="date" name="date" id="dateRequired" bsSize="sm" name="dateRequired" value={this.state.dateRequired} onChange={(e) => this.handleUserInput(e)}/>
                  <FormFeedback>Date Required is required</FormFeedback>
                </InputGroup>

                <Label for="contract">
                  <small>Contract</small>
                </Label>
                <InputGroup className="mb-4">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-certificate" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="select" id="Contract" name="contractId" bsSize="sm" value={this.state.contractId} onChange={(e) => this.handleUserInput(e)}>
                    {this.state.contractList.map(item => (
                      <option key={item.code} value={item.code}>
                        {item.value}
                      </option>
                    ))}
                  </Input>
                  <FormFeedback>Aircraft Registration is required</FormFeedback>
                </InputGroup>
              </Col>

              <Col name="leftFormColumn">
                <Label for="aircraftReg">
                  <small>Aircraft Registration</small>
                </Label>
                <InputGroup className="mb-4">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-certificate" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="select" id="aircraftReg" name="aircraftRegistration" bsSize="sm" value={this.state.aircraftRegistration} onChange={(e) => this.handleUserInput(e)}>
                    {this.state.aircraftRegList.map(item => (
                      <option key={item.code} value={item.code}>
                        {item.value}
                      </option>
                    ))}
                  </Input>
                  <FormFeedback>Aircraft Registration is required</FormFeedback>
                </InputGroup>

                <Label for="partNumber">
                  <small>Request Part Number</small>
                </Label>
                <InputGroup className="mb-4">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-cogs" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" id="partNumber" placeholder="Part Number" bsSize="sm" name="requestPartNumber" value={this.state.requestPartNumber} onChange={(e) => this.handleUserInput(e)}/>
                  <FormFeedback>Part Number is required</FormFeedback>
                </InputGroup>

                <Label for="partRemovalReason">
                  <small>Part Removal Reason</small>
                </Label>
                <InputGroup className="mb-4">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-pencil-square-o" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="select" id="partRemovalReason" bsSize="sm" name="partRemovalReason" value={this.state.partRemovalReason} onChange={(e) => this.handleUserInput(e)}>
                    {partRemovalReasons.map(item => (
                      <option key={item.code} value={item.code}>
                        {item.value}
                      </option>
                    ))}
                  </Input>
                  <FormFeedback>Part Removal Reason is required</FormFeedback>
                </InputGroup>
                {this.state.partRemovalReason == 'Other' && <div>
                <Label for="contract">
                  <small>Part Removal Reason 'Other'</small>
                </Label>
                <InputGroup className="mb-4">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-certificate" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" id="partRemovalReasonOther" placeholder="Part Removal Reason" bsSize="sm" name="partRemovalReasonOther" value={this.state.partRemovalReasonOther} onChange={(e) => this.handleUserInput(e)}/>
                  <FormFeedback>Part Number is required</FormFeedback>
                  </InputGroup>
                </div>
                }
              </Col>
              </Row>
            
            <Row name="shipToHeader">
              <Col>
                <div className="form-header ship">
                  <h5>
                    Ship To{" "}
                    <span className="float-right">{shipToDropdown()}</span>
                  </h5>
                </div>
              </Col>
            </Row>
            {shipToForm()}
          </FormGroup>
        </Form>
      </div>
      </form>
    );
  }

  pbhOrdersDetailViewHeader() {
    const pbhNewOrderDetailViewForm = this.pbhNewOrderDetailViewForm();
    return (
      <div>{pbhNewOrderDetailViewForm}</div>
    );
  }

  pbhOrdersDetailsViewMain() {
    return (
      <div className="animated fadeIn">{this.pbhOrdersDetailViewHeader()}</div>
    );
  }

  render()
  {
    return <div>{this.pbhOrdersDetailsViewMain()}</div>;
  }
}

function mapStateToProps(state) {
  const { user } = state.authentication;
  return {user};
}

const connectedPBHOrdersNewRequest = connect(mapStateToProps)(PBHOrdersNewRequest);
export default connectedPBHOrdersNewRequest;