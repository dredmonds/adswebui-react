import React, { Component } from "react";
import { connect } from "react-redux";
import classnames from "classnames";
import {Form, FormGroup, Nav,  NavItem,  NavLink,  TabContent,TabPane} from "reactstrap";
import { userActions, alertActions } from "../../actions";
import PBHOrdersContent from "./PBHOrdersContent";
import PBHOrderActions from "./PBHOrderActions";
import { ProgressSpinner } from "primereact/progressspinner";
import PBHOrdersNewRequest from "./PBHOrdersNewRequest";

class PBHOrdersDetailViewPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      aircraftRegList: [],
      shipToList: [],
      shipTo: {},
      shipDropdownOpen: false,
      activeActionButton: "",
      actionElement: []
    };
  }

  pbhNewOrderDetailViewForm() {
    return <PBHOrdersNewRequest orders={this.props.orders} clearPbhOrdersAndActions ={this.props.clearPbhOrdersAndActions.bind(this)} pbhDataViewToggleTab={this.props.pbhDataViewToggleTab.bind(this)} setExchangeRequestId={this.props.setExchangeRequestId.bind(this)} getPbhOrderPromiseResult={this.props.getPbhOrderPromiseResult.bind(this)} getPbhActionPromiseResult={this.props.getPbhActionPromiseResult.bind(this)}/>
  };
  

  pbhOrderDetail() {
    return (
      <div name="pbhOrderDetailsForm">
        <Form name="pbhOrderDetails" id="pbhOrderDetails">
          <FormGroup>
            {this.props.pbhActions && <PBHOrderActions pbhActions={this.props.pbhActions} pbhOrders={this.props.pbhOrders} exchangeRequestId={this.props.exchangeRequestId} clearPbhOrdersAndActions ={this.props.clearPbhOrdersAndActions.bind(this)} refreshPbhOrdersAndActions ={this.props.refreshPbhOrdersAndActions.bind(this)}/>}
            {this.props.pbhOrders && <PBHOrdersContent pbhOrders={this.props.pbhOrders} exchangeRequestId={this.props.exchangeRequestId} refreshPbhOrdersAndActions ={this.props.refreshPbhOrdersAndActions.bind(this)}/>}
            {(!this.props.pbhActions && !this.props.pbhOrders) && <div style={{textAlign:'center'}}><ProgressSpinner/></div>}
          </FormGroup>
        </Form>
      </div>
    );
  }

  httpErrorHandler(error) {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if (httpStatus >= 500 || httpStatus == 404) {
      // Added to handle error(s)
      this.setState({ loading: false });
      this.props.dispatch(alertActions.error("Network Connection Error."));
    } else if (httpStatus == 401) {
      userActions.msalLogout();
    } else {
      this.setState({ loading: false });
      this.props.dispatch(failure(error.toString()));
      this.props.dispatch(alertActions.error(error.toString()));
    }
  }

  pbhOrdersDetailViewHeader() {
    const pbhNewOrderDetailsForm = this.pbhNewOrderDetailViewForm();
    const pbhOrderDetail = this.pbhOrderDetail();
    return (
      <div>
        {/* <div style={{ backgroundColor: "#f0f3f5" }}>
          <Nav pills style={{ color: "#3e515b" }}>
            <NavItem>
              <NavLink
                className={classnames({
                  active: this.props.pbhDetailViewNavActiveTab === "1"
                })}
                onClick={() => {
                  this.props.pbhDataViewToggleTab("1");
                }}
                style={{ cursor: "pointer" }}
              >
                <div style={{ textAlign: "center" }}>
                  <i className="fa fa-cogs" />
                  <div>
                    <small>PBH Order Details</small>
                  </div>
                </div>
              </NavLink>
            </NavItem>
          </Nav>
        </div> */}
        <TabContent activeTab={this.props.pbhDetailViewNavActiveTab}>
          <TabPane tabId="0" style={{ height: "852.3px", overflowY: "scroll" }}>
            <div>{pbhNewOrderDetailsForm}</div>
          </TabPane>
          <TabPane tabId="1" style={{ height: "852.3px", overflowY: "scroll" }}>
            <div>{pbhOrderDetail}</div>
          </TabPane>
        </TabContent>
      </div>
    );
  }

  pbhOrdersDetailsViewMain() {
    return (
      <div className="animated fadeIn">{this.pbhOrdersDetailViewHeader()}</div>
    );
  }

  render() {
    return <div>{this.pbhOrdersDetailsViewMain()}</div>;
  }
}

function mapStateToProps(state) {
  const { user } = state.authentication;
  return {user};
}

const connectedPBHOrdersDetailViewPage = connect(mapStateToProps)(
  PBHOrdersDetailViewPage
);
export default connectedPBHOrdersDetailViewPage;
