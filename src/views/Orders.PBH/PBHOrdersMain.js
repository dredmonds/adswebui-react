import React, {Component} from 'react';
import {connect} from 'react-redux';
import classnames from 'classnames';

import {Row, Col, Card, CardTitle, CardSubtitle, CardBody,
        Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';

import PBHOrdersComponentPage from './PBHOrdersComponent';
import PBHReportComponentPage from './PBHReportComponent';
import PBHStockComponentPage from './PBHStockComponent';

class PBHOrdersMainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pbhOrdersMainActiveTab: '1',
    };
  }

  pbhOrdersMainToggleTab(tab) {
    this.state.pbhOrdersMainActiveTab !== tab ? this.setState({pbhOrdersMainActiveTab: tab}) : null;
  }

  pbhOrdersMain() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12" lg="12">
            <Card>
              <CardBody>
                <div style={{backgroundColor: '#e3f2fd'}}>
                  <Nav pills style={{color: '#3e515b'}}>
                    <NavItem>
                      <NavLink className={classnames({ active: this.state.pbhOrdersMainActiveTab === '1'})}
                        onClick={() => { this.pbhOrdersMainToggleTab('1'); }} style={{cursor: 'pointer'}}>
                        <i className="fa fa-list-ul" />&nbsp;PBH Orders
                      </NavLink>
                    </NavItem>
                    {/* <NavItem>
                      <NavLink className={classnames({ active: this.state.pbhOrdersMainActiveTab === '2'})}
                        onClick={() => { this.pbhOrdersMainToggleTab('2'); }} style={{cursor: 'pointer'}}>
                        <i className="fa fa-line-chart" />&nbsp;Report Suite
                      </NavLink>
                    </NavItem> */}
                    {/* <NavItem>
                      <NavLink className={classnames({ active: this.state.pbhOrdersMainActiveTab === '3'})}
                        onClick={() => { this.pbhOrdersMainToggleTab('3'); }} style={{cursor: 'pointer'}}>
                        <i className="fa fa-cubes" />&nbsp;My Stock
                      </NavLink>                  
                    </NavItem> */}
                  </Nav>                
                </div>

                <TabContent activeTab={this.state.pbhOrdersMainActiveTab} style={{borderStyle: 'none'}}>
                  <TabPane tabId="1" style={{padding: '0em'}}>
                    <div>{ <PBHOrdersComponentPage /> }</div>
                  </TabPane>
                  <TabPane tabId="2" style={{padding: '0em'}}>
                  <div>{ <PBHReportComponentPage /> }</div>
                  </TabPane>
                  <TabPane tabId="3" style={{padding: '0em'}}>
                  <div>{ <PBHStockComponentPage /> }</div>
                  </TabPane>
                </TabContent>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }

  render() {
    return (
      <div>{this.pbhOrdersMain()}</div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const connectedPBHOrdersMainPage = connect(mapStateToProps)(PBHOrdersMainPage);
export default connectedPBHOrdersMainPage;