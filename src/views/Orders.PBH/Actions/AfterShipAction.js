import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row,  Col} from "reactstrap";

class AfterShipAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: `https://worley9gmailcom.aftership.com/${this.props.shipmentContent.airwayBill}`
    };
  }

  afterShipAction() {
    return (
      <div className="animated fadeIn brand-color col-lg-7" style={{ margin: 'auto' }}>
        <Row>
          <Col lg="12">
            <Row>
              <Col lg="12">
                <span className="icon icon-cross cursor-pointer" style={{ float: 'right' }} onClick={() => this.props.close()}></span>
              </Col>
            </Row>
            <Row>
              <Col lg="12" style={{ textAlign: 'center' }}>
                <span className="icon icon-3x icon-forward"></span>
              </Col>
            </Row>
            <Row>
              <Col lg="12" style={{ textAlign: 'center' }}>
                <p>Tracking</p>
              </Col>
            </Row>              
            <Row>
              <Col lg="12" style={{ textAlign: 'center' }}>
                <iframe style={{height:'500px', width:'100%'}} src={this.state.url}></iframe></Col>
            </Row>
          </Col>
        </Row>      
      </div>
    ); 
  }

  render() {
    return (
      <div  className="animated fadeIn">
        {this.afterShipAction()}
      </div>
    );
  }
}

function mapStateToProps() {
  return {
  };
}

const connectedAfterShipAction = connect(mapStateToProps)(AfterShipAction);
export default connectedAfterShipAction;