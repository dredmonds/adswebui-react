import React, { Component } from 'react';
import { connect } from 'react-redux';
import {pbhOrderService} from '../../../services';
import {  Row,  Col}  from "reactstrap";

class CoveredAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      partCovered : null
    };
  }

  partCovered(partCovered){
    this.setState({partCovered: partCovered});
  }

  postCovered() {
    var partCoveredObject = {
      "isCovered": this.state.partCovered
    };
    
    var queryString = `https://action.api.next.cloud.ajw-group.com/api/Orders/${this.props.exchangeRequestId}/IsCovered`;
    var pbhActions= pbhOrderService.postPbhAction(queryString, partCoveredObject);
    this.props.close();
    this.props.clearPbhOrdersAndActions();
    
    pbhActions.then(pbhAction => {
      this.props.refreshPbhOrdersAndActions()      
    })   
  };

  coveredAction() {
    return (
      <div className="animated fadeIn brand-color col-lg-7" style={{ margin: 'auto' }}>
        <form id="form" className="form-group columns col-mx-auto container"  onSubmit={() => this.postCovered()}>
          <Row>
            <Col lg="12">
              <Row>
                <Col lg="12">
                  <span className="icon icon-cross" style={{ float: 'right' }} onClick={() => this.props.close()}></span>
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{ textAlign: 'center' }}>
                  <span className="icon icon-3x icon-check"></span>
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{ textAlign: 'center' }}>
                  <h3>Part Covered?</h3>
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{ textAlign: 'center' }}>
                  <p>Select if the part requested is covered under contract.</p>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col lg="5" style={{ textAlign: 'center' }}>
              <button onClick={() => this.partCovered(true)} type="submit" className="btn btn-primary column col-12">
                Covered
                <span className="icon icon-check"></span>
              </button>
            </Col>
            <Col lg="2" style={{ textAlign: 'center' }}></Col>
            <Col lg="5" style={{ textAlign: 'center' }}>
              <button onClick={() => this.partCovered(false)} type="submit" className="btn btn-primary column col-12">
                Not Covered
                <span className="icon icon-cross"></span>
              </button>
            </Col>
          </Row>
        </form>
      </div>
    );
  }

  render() {
    return (
      <div  className="animated fadeIn">
        {this.coveredAction()}
      </div>
    );
  }
}

function mapStateToProps() {
  return {
  };
}

const connectedCoveredAction = connect(mapStateToProps)(CoveredAction);
export default connectedCoveredAction;