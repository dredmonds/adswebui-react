import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormErrors } from './FormErrors';
import {  Row,  Col, Input} from "reactstrap";
import {pbhOrderService} from '../../../services';

class ShippedAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slug: null,
      airwayBill: null,
      shipmentDate: null,
      formErrors: {airwayBill: '', slug: '', shipmentDate: ''},
      airwayBillValid: false,
      slugValid: false,
      shipmentDateValid: false,
      formValid: false
    };
	  this.postShipmentDetails = this.postShipmentDetails.bind(this);

  }

  postShipmentDetails() {
    var shipmentObject = {
      "airwayBill": this.state.airwayBill,
      "slug": this.state.slug,
      "shipmentDate": this.state.shipmentDate
    };
    
    var queryString = `https://action.api.next.cloud.ajw-group.com/api/Orders/${this.props.exchangeRequestId}/Tracking`;
    var pbhActions= pbhOrderService.postPbhAction(queryString, shipmentObject);
    this.props.close();
    this.props.clearPbhOrdersAndActions();
    
    pbhActions.then(pbhAction => {
      this.props.refreshPbhOrdersAndActions()
    })
  };

  handleUserInput(e){
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},() => { this.validateField(name, value) });
  };

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let slugValid = this.state.slugValid;
    let airwayBillValid = this.state.airwayBillValid;
    let shipmentDateValid = this.state.shipmentDateValid;

    switch(fieldName) {
      case 'airwayBill':
        var lengthValidation = this.lengthValidation(value);
        var alphaNumericValidation = this.alphaNumericValidation(value);
        airwayBillValid = (alphaNumericValidation && lengthValidation);
        var airwayBillErrors = ''

        if(!lengthValidation){
          airwayBillErrors += 'Airway bill is too short'
        }
        if(!alphaNumericValidation && !lengthValidation){
          airwayBillErrors += ' & '
        }
        if(!alphaNumericValidation){
          airwayBillErrors += 'Airway bill needs to be an alpha numberic value.'
        }

        fieldValidationErrors.airwayBill = airwayBillValid ? '' : airwayBillErrors;
        break;

      case 'slug':
        var lengthValidation = this.lengthValidation(value);
        var alphaNumericValidation = this.alphaNumericValidation(value);
        slugValid = (alphaNumericValidation && lengthValidation);

        var slugErrors = ''
         if(!lengthValidation){
          slugErrors += 'Slug is too short'
        }
        if(!alphaNumericValidation && !lengthValidation){
          slugErrors += ' & '
        }
        if(!alphaNumericValidation){
          slugErrors += 'Slug needs to be an alpha numberic value.'
        }
        fieldValidationErrors.slug = slugValid ? '' : slugErrors;
       break;
       case 'shipmentDate':
        shipmentDateValid = this.dateValidation(value);
        fieldValidationErrors.shipmentDateValid = shipmentDateValid ? '' : 'Shipment date is not valid date format. (dd/mm/yyyy)';
       break;
      default:
        break;
    }
    this.setState({formErrors: fieldValidationErrors, airwayBillValid: airwayBillValid, slugValid: slugValid, shipmentDateValid: shipmentDateValid}, this.validateForm);
  }

  lengthValidation(value){
    return value.length == 0 || value.length >= 6;
  }

  dateValidation(value){
    var regExpression = new RegExp(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/);
    var isDateFormat = regExpression.test(value);    
    return isDateFormat;
  }

  alphaNumericValidation(value){
    var regExpression = new RegExp(/^[a-z0-9]+$/i);
    var isAlphaNumeric = regExpression.test(value);
    return isAlphaNumeric;
  }

  errorClass(error) {
    return(error == undefined || error.length === 0 ? '' : 'has-error');
  }

  validateForm() {
    var valid = (this.state.airwayBillValid && this.state.slugValid && this.state.shipmentDateValid)
    this.setState({formValid: valid});
  }

  shippedAction() {
    return (
      <div className="animated fadeIn brand-color col-lg-7" style={{ margin: 'auto' }}>
      <form id="form" className="form-group columns col-mx-auto container"  onSubmit={this.postShipmentDetails}>
          <Row>
            <Col lg="12">
              <Row>
                <Col lg="12">
                  <span className="icon icon-cross" style={{ float: 'right' }} onClick={() => this.props.close()}></span>
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{ textAlign: 'center' }}>
                  <span className="icon icon-3x icon-location"></span>
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{ textAlign: 'center' }}>
                  <h3>Shipment Details</h3>
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{ textAlign: 'center' }}>
                  <p>Enter shipment details below.</p>
                </Col>
              </Row>
              <Row>
                <Col lg="12">
                  <div  className={`form-group ${this.errorClass(this.state.formErrors)}`}>       
                    <div className="panel panel-default">
                      <FormErrors formErrors={this.state.formErrors} /> 
                    </div>
                  </div>
                  <p>Airway Bill</p>
                  <Input id="awb" type="text" name="airwayBill" placeholder="Airway Bill" className="form-input" style={{width:'100%'}} value={this.state.airwayBill} onChange={(e) => this.handleUserInput(e)}></Input>
                </Col>
              </Row>
              <Row>
                <Col lg="12">
                  <p>Slug</p>
                  <Input id="slug" type="text" name="slug" placeholder="Slug" className="form-input" style={{width:'100%'}} value={this.state.slug} onChange={(e) => this.handleUserInput(e)}></Input>
                </Col>
              </Row>
              <Row>
                <Col lg="12">
                  <p>Shipment Date</p>
                  <Input id="shipmentDate" type="date" placeholder="dd/mm/yyyy" name="shipmentDate" className="form-input" style={{width:'100%'}} value={this.state.shipmentDate} onChange={(e) => this.handleUserInput(e)}></Input>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col lg="12" style={{ textAlign: 'center' }}>
              <button className="btn btn-primary column col-5" disabled={!this.state.formValid}>
                  Post Shipment
              </button>
            </Col>
          </Row>
        </form>
      </div>
    );
  }

  render() {
    return (
      <div  className="animated fadeIn">
        {this.shippedAction()}
      </div>
    );
  }
}

function mapStateToProps() {
  return {
  };
}

const connectedCommentAction = connect(mapStateToProps)(ShippedAction);
export default connectedCommentAction;