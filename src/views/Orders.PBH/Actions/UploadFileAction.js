import React, { Component } from 'react';
import { connect } from 'react-redux';
import {pbhOrderService} from '../../../services';
import { Row,  Col, Input} from "reactstrap";
import { FormErrors } from './FormErrors';

const STATUS_INITIAL = 0;
const STATUS_SAVING = 1;
const STATUS_SUCCESS = 2;
const STATUS_FAILED = 3;

class UploadFileAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadedFile: '',
      uploadedFileDescription : '',
      formErrors: {uploadedFile: '', uploadedFileDescription: ''},
      uploadedFileValid: false,
      uploadedFileDescriptionValid: false,
      formValid: false,
      currentStatus : null
    };
  }

  mounted() {
    this.reset();
  }

  isInitial(){
    return this.currentStatus === STATUS_INITIAL;
  }

  isSaving(){
    return this.currentStatus === STATUS_SAVING;
  }

  isSuccess(){
    return this.currentStatus === STATUS_SUCCESS;
  }

  isFailed(){
    return this.currentStatus === STATUS_FAILED;
  }

  reset() {
    this.currentStatus = STATUS_INITIAL;
    this.uploadError = null;
  }

  postUploadFile() {
    var uploadedFile = this.state.uploadedFile;
    var queryString = `https://action.api.next.cloud.ajw-group.com/api/Orders/${this.props.exchangeRequestId}/UploadFile?description="${this.state.uploadedFileDescription}"`;
    var pbhActions= pbhOrderService.postPbhAction(queryString, uploadedFile);
    this.props.close();
    this.props.clearPbhOrdersAndActions();
    
    pbhActions.then(pbhAction => {
        this.props.refreshPbhOrdersAndActions();
      }).catch(e => {
        this.props.refreshPbhOrdersAndActions();
      });
    };
  
  handleUserInput(e){
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},() => { this.validateField(name, value) });
  };

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let uploadedFileValid = this.state.uploadedFileValid;
    let uploadedFileDescriptionValid = this.state.uploadedFileDescriptionValid;
    let uploadedFile = this.state.uploadedFile;

    switch(fieldName) {
        case 'uploadedFile':
        uploadedFileValid = this.uploadedFileValidation();
        fieldValidationErrors.uploadedFile = uploadedFileValid ? '' : 'Please select a file to upload.';
        if(!uploadedFileValid) break;
        this.uploadFile();

        break;
        case 'uploadedFileDescription':
        uploadedFileDescriptionValid = this.uploadedFileDescriptionValidation(value);
        fieldValidationErrors.uploadedFileDescription = uploadedFileDescriptionValid ? '' : 'File description needs to be more than 8 characters.';
        break;
      default:
        break;
    }
    this.setState({formErrors: fieldValidationErrors, uploadedFileValid: uploadedFileValid, uploadedFileDescriptionValid: uploadedFileDescriptionValid}, this.validateForm);
  }

  uploadedFileValidation(){
    var uploadedFileElement = document.getElementById("UploadedFile");
    var fileCount = uploadedFileElement.files.length;
   return fileCount != 0;
  }

  uploadedFileDescriptionValidation(value){
    return value.length >= 8;
  }
  
  uploadFile(){
    var uploadError = null
    var uploadedFileElement = document.getElementById("UploadedFile");
    var files = uploadedFileElement.files;
    var fileCount = files.length;
    const formData = new FormData();

    const file = files[0];
    formData.append(`file`, file, file.name);
    this.setState({uploadedFile: formData});
  }

  errorClass(error) {
    return(error == undefined || error.length === 0 ? '' : 'has-error');
  }

  validateForm() {
    var valid = (this.state.uploadedFileValid && this.state.uploadedFileDescriptionValid);
    this.setState({formValid: valid});
  }

  uploadFileAction() {
    return (
      <div className="animated fadeIn brand-color col-lg-7" style={{ margin: 'auto' }}>
        <form id="form" className="form-group columns col-mx-auto container"  onSubmit={() => this.postUploadFile()}>
            <Row>
              <Col lg="12">
                <Row>
                  <Col lg="12">
                    <span className="icon icon-cross cursor-pointer" style={{ float: 'right' }} onClick={() => this.props.close()}></span>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <span className="icon icon-3x icon-upload"></span>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <h3>UploadFile</h3>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <p>Upload the file below:</p>
                  </Col>
                </Row>              
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>                 
                    <div  className={`form-group ${this.errorClass(this.state.formErrors.uploadFile)}`}>       
                        <div className="panel panel-default">
                          <FormErrors formErrors={this.state.formErrors} /> 
                        </div>
                      <Input required type="file" className="col-lg-12" id="UploadedFile" placeholder="Upload File" name="uploadedFile" aria-required="true" aria-invalid="false" value={this.state.uploadFile} onChange={(e) => this.handleUserInput(e)}></Input>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <p>Description of uploaded file:</p>
                  </Col>
                </Row>              
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <div  className={`form-group ${this.errorClass(this.state.formErrors.uploadFile)}`}>
                        <div className="panel panel-default">
                          <FormErrors formErrors={this.state.formErrors} /> 
                        </div>
                      <Input required className="col-lg-12" id="UploadFileDescription" placeholder="Uploaded file description" name="uploadedFileDescription" aria-required="true" aria-invalid="false" value={this.state.uploadedFileDescription} onChange={(e) => this.handleUserInput(e)}></Input>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col lg="12" style={{ textAlign: 'center' }}>
                <button className="btn btn-primary column col-5" type="submit" disabled={!this.state.formValid}>
                  Post UploadFile
                </button>
              </Col>
            </Row>          
        </form>
      </div>
    ); 
  }

  render() {
    return (
      <div  className="animated fadeIn">
        {this.uploadFileAction()}
      </div>
    );
  }
}

function mapStateToProps() {
  return {
  };
}

const connectedUploadFileAction = connect(mapStateToProps)(UploadFileAction);
export default connectedUploadFileAction;