import React, { Component } from 'react';
import { connect } from 'react-redux';
import {pbhOrderService} from '../../../services';
import { Row,  Col} from "reactstrap";
import { FormErrors } from './FormErrors';

class CommentAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: '',
      formErrors: {comment: ''},
      commentValid: false,
      formValid: false
    };
  }

  postComment() {
    var comment = this.state.comment;
    var commentObject = {
      "message": comment
    };
    
    var queryString = `https://action.api.next.cloud.ajw-group.com/api/Orders/${this.props.exchangeRequestId}/AddComment`;
    var pbhActions= pbhOrderService.postPbhAction(queryString, commentObject);
    this.props.close();
    this.props.clearPbhOrdersAndActions();
    
    pbhActions.then(pbhAction => {
      this.props.refreshPbhOrdersAndActions()      
    })
  };
  
  handleUserInput(e){
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},() => { this.validateField(name, value) });
  };

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let commentValid = this.state.commentValid;

    switch(fieldName) {
      case 'comment':
        commentValid = this.commentValidation()
        fieldValidationErrors.comment = commentValid ? '' : 'Comment is too short';
        break;
      default:
        break;
    }
    this.setState({formErrors: fieldValidationErrors, commentValid: commentValid,}, this.validateForm);
  }

  commentValidation(){
    return this.state.comment.length >= 6;
  }

  errorClass(error) {
    return(error == undefined || error.length === 0 ? '' : 'has-error');
  }

  validateForm() {
    this.setState({formValid: this.state.commentValid});
  }

  commentAction() {
    return (
      <div className="animated fadeIn brand-color col-lg-7" style={{ margin: 'auto' }}>
        <form id="form" className="form-group columns col-mx-auto container"  onSubmit={() => this.postComment()}>
            <Row>
              <Col lg="12">
                <Row>
                  <Col lg="12">
                    <span className="icon icon-cross cursor-pointer" style={{ float: 'right' }} onClick={() => this.props.close()}></span>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <span className="icon icon-3x icon-message"></span>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <h3>Comment</h3>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <p>Enter your comment below</p>
                  </Col>
                </Row>              
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>                 
                    <div  className={`form-group ${this.errorClass(this.state.formErrors.comment)}`}>       
                        <div className="panel panel-default">
                          <FormErrors formErrors={this.state.formErrors} /> 
                        </div>
                      <textarea required className="col-lg-12" id="CommentTextArea" placeholder="Comment" name="comment" aria-required="true" aria-invalid="false" value={this.state.comment} onChange={(e) => this.handleUserInput(e)}></textarea>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col lg="12" style={{ textAlign: 'center' }}>
                <button className="btn btn-primary column col-5" type="submit" disabled={!this.state.formValid}>
                  Post Comment
                </button>
              </Col>
            </Row>          
        </form>
      </div>
    ); 
  }

  render() {
    return (
      <div  className="animated fadeIn">
        {this.commentAction()}
      </div>
    );
  }
}

function mapStateToProps() {
  return {
  };
}

const connectedCommentAction = connect(mapStateToProps)(CommentAction);
export default connectedCommentAction;