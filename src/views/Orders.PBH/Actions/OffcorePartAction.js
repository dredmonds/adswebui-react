import React, { Component } from 'react';
import { connect } from 'react-redux';
import {pbhOrderService} from '../../../services';
import { Row,  Col, Input} from "reactstrap";
import { FormErrors } from './FormErrors';

class OffcorePartAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      partNumber: '',
      serialNumber: '',
      customerContactName: '',
      hours: '',
      cycles: '',
      formErrors: {partNumber: '',serialNumber:'',hours: '', cycles: '', customerContactName: '' },
      partNumberValid: false,
      serialNumberValid: false,
      customerContactNameValid: false,
      hoursValid: false,
      cyclesValid: false,
      formValid: false
    };
  }

  postOffcorePart() {
    var offcorePartObject = {
      "partNumber": this.state.partNumber,
      "serialNumber": this.state.serialNumber,
      "hours": this.state.hours,
      "cycles": this.state.cycles,
      "customerContactName": this.state.customerContactName
    };
    
    var queryString = `https://action.api.next.cloud.ajw-group.com/api/Orders/${this.props.exchangeRequestId}/OffcorePart`;
    var pbhActions= pbhOrderService.postPbhAction(queryString, offcorePartObject);
    this.props.close();
    this.props.clearPbhOrdersAndActions();
    
    pbhActions.then(pbhAction => {
      this.props.refreshPbhOrdersAndActions()      
    })
  };
  
  handleUserInput(e){
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},() => { this.validateField(name, value) });
  };

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let partNumberValid = this.state.partNumberValid;
    let serialNumberValid = this.state.serialNumberValid;
    let customerContactNameValid = this.state.customerContactNameValid;
    let hoursValid = this.state.hoursValid;
    let cyclesValid = this.state.hoursValid;

    switch(fieldName) {
      case 'partNumber':
        partNumberValid = this.lengthValidation(this.state.partNumber)
        fieldValidationErrors.partNumber = partNumberValid ? '' : 'Part number is too short.';
      break;
      case 'serialNumber':
      serialNumberValid = this.lengthValidation(this.state.serialNumber)
        fieldValidationErrors.serialNumber = serialNumberValid ? '' : 'Serial number is too short.';
      break;
      case 'customerContactName':
      customerContactNameValid = this.lengthValidation(this.state.customerContactName)
        fieldValidationErrors.customerContactName = customerContactNameValid ? '' : 'Customer contact name is too short.';
      break;
      case 'hours':
        hoursValid = this.numericRegexValidation(this.state.hours)
        fieldValidationErrors.hours = hoursValid ? '' : 'Hours is not a number.';
      break;
      case 'cycles':
        cyclesValid = this.numericRegexValidation(this.state.cycles)
          fieldValidationErrors.cycles = cyclesValid ? '' : 'Cycles is not a number.';
      break;
      default:
        break;
    }
    this.setState({formErrors: fieldValidationErrors, partNumberValid: partNumberValid,}, this.validateForm);
  }

  lengthValidation(value){
    return value.length >= 6;
  }

  numericRegexValidation(value){
    var regExpression = new RegExp(/^[0-9]+$/i);
    var isNumeric = regExpression.test(value);    
    return isNumeric;
  }

  errorClass(error) {
    return(error == undefined || error.length === 0 ? '' : 'has-error');
  }

  validateForm() {
    this.setState({formValid: this.state.partNumberValid});
  }

  offcorePartAction() {
    return (
      <div className="animated fadeIn brand-color col-lg-7" style={{ margin: 'auto' }}>
        <form id="form" className="form-group columns col-mx-auto container"  onSubmit={() => this.postOffcorePart()}>
            <Row>
              <Col lg="12">
                <Row>
                  <Col lg="12">
                    <span className="icon icon-cross cursor-pointer" style={{ float: 'right' }} onClick={() => this.props.close()}></span>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <span className="icon icon-3x icon-message"></span>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12" style={{ textAlign: 'center' }}>
                    <h3>Off Core Part</h3>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12">
                    <div  className={`form-group ${this.errorClass(this.state.formErrors)}`}>
                      <div className="panel panel-default">
                        <FormErrors formErrors={this.state.formErrors} /> 
                      </div>
                    </div>
                    <p>Enter the partNumber below</p>
                  </Col>
                </Row>              
                <Row>
                  <Col lg="12">                 
                      <Input required className="col-lg-12" id="OffcorePartTextArea" placeholder="Offcore part" name="partNumber" aria-required="true" aria-invalid="false" value={this.state.partNumber} onChange={(e) => this.handleUserInput(e)}></Input>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12">
                    <label>Enter the serial number below</label>
                  </Col>
                </Row>              
                <Row>
                  <Col lg="12">
                      <Input required className="col-lg-12" id="serialNumber" placeholder="serial number" name="serialNumber" aria-required="true" aria-invalid="false" value={this.state.serialNumber} onChange={(e) => this.handleUserInput(e)}></Input>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12">
                    <p>input the amount of hours below</p>
                  </Col>
                </Row>              
                <Row>
                  <Col lg="12">                 
                      <Input required className="col-lg-12" id="hours" placeholder="hours" name="hours" aria-required="true" aria-invalid="false" value={this.state.hours} onChange={(e) => this.handleUserInput(e)}></Input>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12">
                    <p>input the amount of cycles below</p>
                  </Col>
                </Row>              
                <Row>
                  <Col lg="12">                 
                      <Input required className="col-lg-12" id="cycles" placeholder="cycles" name="cycles" aria-required="true" aria-invalid="false" value={this.state.cycles} onChange={(e) => this.handleUserInput(e)}></Input>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12">
                    <p>input the customer contact name</p>
                  </Col>
                </Row>              
                <Row>
                  <Col lg="12">                 
                      <Input required className="col-lg-12" id="customerContactName" placeholder="customer contact name" name="customerContactName" aria-required="true" aria-invalid="false" value={this.state.customerContactName} onChange={(e) => this.handleUserInput(e)}></Input>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col lg="12" style={{ textAlign: 'center' }}>
                <button className="btn btn-primary column col-5" type="submit" disabled={!this.state.formValid}>
                  Post OffcorePart
                </button>
              </Col>
            </Row>          
        </form>
      </div>
    ); 
  }

  render() {
    return (
      <div  className="animated fadeIn">
        {this.offcorePartAction()}
      </div>
    );
  }
}

function mapStateToProps() {
  return {
  };
}

const connectedOffcorePartAction = connect(mapStateToProps)(OffcorePartAction);
export default connectedOffcorePartAction;