import React, { Component } from 'react';
import { connect } from 'react-redux';
import {pbhOrderService} from '../../../services';
import {  Row,  Col} from "reactstrap";

class PartInStockAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      partInStock: null
    };
  }

  inStock(partInStock){
    this.setState({partInStock: partInStock});
  }

  postPartInStock() {
    var partInStockObject = {
      "isInstock": this.state.partInStock,
      "partNumber": this.props.pbhOrder.orderContent.partNumber
    };

    console.log(this.state.partInStock)
    var queryString = `https://action.api.next.cloud.ajw-group.com/api/Orders/${this.props.exchangeRequestId}/InStock`;
    var pbhActions= pbhOrderService.postPbhAction(queryString, partInStockObject);
    this.props.close();
    this.props.clearPbhOrdersAndActions();
    
    pbhActions.then(pbhAction => {
      this.props.refreshPbhOrdersAndActions()      
    })   
  };

  partInStockAction() {
    var filteredPbhOrder= this.props.pbhOrder.orderContent;

    return (
      <div className="animated fadeIn brand-color col-lg-7" style={{ margin: 'auto' }}>
        <form id="form" className="form-group columns col-mx-auto container"  onSubmit={() => this.postPartInStock()}>
          <Row>
            <Col lg="12">
              <Row>
                <Col lg="12">
                  <span className="icon icon-cross" style={{ float: 'right' }} onClick={() => this.props.close()}></span>
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{ textAlign: 'center' }}>
                  <span className="icon icon-3x icon-check"></span>
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{ textAlign: 'center' }}>
                  <h3>Part Instock?</h3>
                </Col>
              </Row>
              <Row>
                <Col lg="12" style={{ textAlign: 'center' }}>
                  <p>Select if the part {filteredPbhOrder.partNumber} is in stock?</p>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col lg="5" style={{ textAlign: 'center' }}>
              <button onClick={() => this.inStock(true)} type="submit" className="btn btn-primary column col-12">
                In Stock
                <span className="icon icon-check"></span>
              </button>
            </Col>
            <Col lg="2" style={{ textAlign: 'center' }}></Col>
            <Col lg="5" style={{ textAlign: 'center' }}>
              <button onClick={() => this.inStock(false)} type="submit" className="btn btn-primary column col-12">
                Not In Stock
                <span className="icon icon-cross"></span>
              </button>
            </Col>
          </Row>
        </form>
      </div>
    );
  }

  render() {
    return (
      <div  className="animated fadeIn">
        {this.partInStockAction()}
      </div>
    );
  }
}

function mapStateToProps() {
  return {
  };
}

const connectedPartInStockAction = connect(mapStateToProps)(PartInStockAction);
export default connectedPartInStockAction;