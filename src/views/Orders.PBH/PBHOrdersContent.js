import React, { Component } from 'react';
import { connect } from 'react-redux';
import {pbhOrderService} from '../../services';
import { ProgressSpinner } from "primereact/progressspinner";
import {Row, Col, InputGroup, Input, InputGroupButtonDropdown, Button, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';

const tableQuery = 
  [{objName: 'Any', displayVal: 'Any'},
   {objName: 'Covered', displayVal: 'Covered'},
   {objName: 'Instock', displayVal: 'In Stock'},
   {objName: 'Order', displayVal: 'Order'},
   {objName: 'Alternative', displayVal: 'Alternative'},
   {objName: 'Comment', displayVal: 'Comment'},
   {objName: 'Shipped', displayVal: 'Shipped'},
   {objName: 'AfterShip', displayVal: 'After Ship'},
   {objName: 'FileUpload', displayVal: 'File Upload'},
   {objName: 'OffcorePart', displayVal: 'Offcore Part'}];

class PBHOrdersContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeActionButton: '',
      loadingImageUrl: false,
      filterSearchTerm:  '',
      pbhOrdersToFilter: this.props.pbhOrders,
      pbhOrders: this.props.pbhOrders,
      searchSplitButtonOpen: false,
      searchByActionType: 'Any'
    };
  }

  pbhOrderDetail() {
    var pbhOrders = this.state.pbhOrdersToFilter;
    if(pbhOrders.length == 0){
      return <div style={{textAlign:'center'}}>No results found.</div>
    }

    return pbhOrders.map(pbhOrder => 
      <div key={pbhOrder.id}>
      <div className="row " style={{marginTop:"8px",  width:"100%", float:userInternal(pbhOrder.user.internal)}}>
        <div style={{width:"100%", color:"#fff"}}>
          <div style={{backgroundColor:backgroundColorConvo(pbhOrder.user.internal), width:"43%", borderRadius:"25px", float:userInternal(pbhOrder.user.internal)}}>
            <div style={{marginLeft:"16px", paddingTop:"9px"}}>  
              <h5><strong>{pbhOrder.user.name}</strong></h5> 
            </div>
            <Row>
              <Col style={{ marginBottom: "-40px" }}>
                <div className="mb-4 input-group" style={{ margin: "auto", width: "80%" }}>
                  <p style={{whiteSpace:'pre-line'}}>{this.convoContent(pbhOrder, this.props.exchangeRequestId, this)}</p>
                </div>
              </Col>
            </Row>
            <div style={{width: `0`, border: `11px solid ${backgroundColorConvo(pbhOrder.user.internal)}`,float:`${userInternal(pbhOrder.user.internal)}` }}></div>
          </div>
        </div>
          <div style={{textAlign:userInternal(pbhOrder.user.internal), marginTop:"10px", width:"100%"}}>
          {new Date(pbhOrder.actionTime).toLocaleString()}
          </div>
        </div>
          <div style={{float:"none"}}></div>
      </div>
      )

    function userInternal(internalValue) {
	    return internalValue ? "right" : "left";
    }

    function backgroundColorConvo(internalValue) {
	    return internalValue ? "#808285" : "#2b0f54";
    }
  }

  getUploadedImageUrl(fileKey, exchangeRequestId,self){
    var uploadedImageUrl= pbhOrderService.getUploadedImageUrl(exchangeRequestId, fileKey);
    self.setState({loadingImageUrl: true});
    uploadedImageUrl.then(uploadedImageUrl => {
      window.open(uploadedImageUrl.data, '_blank');
      self.setState({loadingImageUrl: false});
    })
  }
  
  convoContent(orderObj,exchangeRequestId, self) {
    switch (orderObj.actionType) {
      case "Covered": return `${orderObj.actionType}: ${orderObj.coveredContent.isCovered}`;
      case "Instock":
        var isInStock = orderObj.inStockContent.isInstock ? "' IS in stock." : "' is NOT in stock."
        return `Part: '${orderObj.inStockContent.partNumber} ${isInStock}`;
      case "Order": 
      var priority  = "";
      switch (orderObj.orderContent.priority) {
        case 1: priority  ="AOG" 
                break;
        case 2: priority  ="Imminent AOG/Critical";
                break;
        case 3: priority  ="IOR/MEL Deferred Defects";
                break;
        case 4: priority  ="Replenishment of MBK";
                break;
        case 5: priority  ="Routine";
                break;
        default: priority  = "Unknown";
                break;
      }
    return `CustomerPo: ${orderObj.orderContent.customerPO} \n Order for: ${orderObj.orderContent.quantityNeeded} x '${orderObj.orderContent.partNumber}' has been requested. \n Destination: ${orderObj.orderContent.destinationName} \n Removal reason: ${orderObj.orderContent.removalReason}. \n AC Reg: ${orderObj.orderContent.acReg} \n Sales Order Number: ${orderObj.orderContent.soNumber} \n Date needed: ${new Date(orderObj.orderContent.dateNeeded).toLocaleDateString()}\n Proiority: ${priority }\n Contact: ${orderObj.orderContent.contactName}`;
      case "Alternative": return `Alternative part: '${orderObj.alternativeContent.partNumber}' proposed.`;
      case "AcceptDecline":
        var customerDecision = orderObj.accepted.accepted ? "ACCEPTED " : "REJECTED ";
        return `Part: '${orderObj.accepted.partNumber}' has been ${customerDecision} by the customer.`;
      case "Comment": return orderObj.commentContent.message;
      case "Shipped": return orderObj.actionType;
      case "AfterShip": return orderObj.actionType;
      case "FileUpload": 
                        var content = "\n Description: " + orderObj.fileUploadContent.description + "\n Uploaded by: " + orderObj.user.name + "\n " + new Date(orderObj.actionTime).toGMTString()
                        return <span><a style={{color:'#fff', textDecoration:'underline', cursor:'pointer'}} onClick={() => this.getUploadedImageUrl(orderObj.fileUploadContent.fileKey, exchangeRequestId,self)}>{orderObj.fileUploadContent.fileName} </a> {content} </span>; 
      case "OffcorePart": return `Part number: ${orderObj.offcorePartContent.partNumber} \n Serial number: ${orderObj.offcorePartContent.serialNumber} \n Hours: ${orderObj.offcorePartContent.hours} \n Cycles: ${orderObj.offcorePartContent.cycles} \n Customer Contact: ${orderObj.offcorePartContent.customerContactName}`;
      default: return orderObj.actionType;
    }
  }

  resetFilter(){
    this.setState({pbhOrdersToFilter: this.state.pbhOrders});
  }

  filterOnSearchTerm(filterQuery){
    const value = filterQuery.target.value;
    this.setState({filterSearchTerm: value});
    
    if(value.length == 0){
      return this.resetFilter();
    }
    // only start filtering results after 3 or more characters.
    if(value.length <= 2){
      return;
    }
    
    var pbhOrderArray = [];
    var mappedPbhOrders = this.state.pbhOrders.map(pbhOrder => 
      {
        var convoContentResult = this.convoContent(pbhOrder, this.props.exchangeRequestId, this)
        // Upload data is returned as a react element in ConvoContent. 
        // Below gets the file name and description details to be filtered through.
        if(pbhOrder.actionType == "FileUpload")
        {
          convoContentResult = convoContentResult.props.children[0].props.children[0] + convoContentResult.props.children[2];
        }
        var pbhOrderToAdd = {pbhContent: convoContentResult,pbhOrderId: pbhOrder.id}
        pbhOrderArray.push(pbhOrderToAdd);
      })

    var pbhOrdersThatContainQuery = pbhOrderArray.filter(f => f.pbhContent.toLowerCase().includes(value.toLowerCase()));
    
    if(pbhOrdersThatContainQuery.length == 0){
      return this.setState({pbhOrdersToFilter: []});
    }

    var pbhOrderIds = pbhOrdersThatContainQuery.map(a => a.pbhOrderId);
    var filteredPbhOrders = this.state.pbhOrders.filter(f => pbhOrderIds.includes(f.id))

    this.setState({pbhOrdersToFilter: filteredPbhOrders});
  }

  pbhDataSearchToggleSplit(event) {
    this.setState({
      searchSplitButtonOpen: !this.state.searchSplitButtonOpen
    });
  }

  filterActionType(actionTypeToFilterOn){
    this.setState({searchByActionType: actionTypeToFilterOn})

    if(actionTypeToFilterOn == "Any"){
      return this.resetFilter();
    }
    
    var filteredPbhOrders = this.state.pbhOrders.filter(e => e.actionType == actionTypeToFilterOn)
    this.setState({pbhOrdersToFilter: filteredPbhOrders});
  }

  filterBar(){
    return(
      <div style={{marginTop:'5px'}}>
        <div style={{backgroundColor:'#f2f2f2', padding:'10px', border:'1px solid #c8c8c8'}}>
        <InputGroup size="sm">
          <InputGroupButtonDropdown addonType="prepend" isOpen={this.state.searchSplitButtonOpen} toggle={(e) => this.pbhDataSearchToggleSplit(e)}>
            {this.state.searchByActionType && <Button><i className="fa fa-search"/>&nbsp;&nbsp;By: {this.state.searchByActionType}</Button> }
            <DropdownToggle split outline/>
            <DropdownMenu onClick={(e) => this.filterActionType(e.target.value)}>
              {
                tableQuery.map((table) =>
                  <DropdownItem key={table.displayVal} value={table.objName}>
                    {table.displayVal}
                  </DropdownItem>
                )
              }
            </DropdownMenu>
            </InputGroupButtonDropdown>
            <Input required className="col-lg-11" id="filterSearchTermTextArea" placeholder="Filter" name="filterSearchTerm" aria-required="true" value={this.state.filterSearchTerm} onChange={(e) => this.filterOnSearchTerm(e)}>
              </Input>
          </InputGroup>
        </div>
      </div>
    )
  }
  
	pbhOrdersDetailsViewMain() {
    var pbhOrderDetail = this.pbhOrderDetail();
    var filterBar = this.filterBar();

    return (
      <div className="animated fadeIn">
        {filterBar}
        {(this.state.loadingImageUrl) && <div style={{textAlign:'center'}}><ProgressSpinner/></div>}
        <Col id="PbhResultDiv" style={{ margin: '10px' }}>
          {pbhOrderDetail}
        </Col>
      </div>
    );
  }

  render() {
    return (
      <div>{this.pbhOrdersDetailsViewMain()}</div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const connectedPBHOrdersContent = connect(mapStateToProps)(PBHOrdersContent);
export default connectedPBHOrdersContent;
