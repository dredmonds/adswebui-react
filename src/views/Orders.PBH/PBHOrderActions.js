import React, { Component } from 'react';
import { connect } from 'react-redux';
import CommentAction from './Actions/CommentAction';
import PartInStockAction from './Actions/PartInStockAction';
import ShippedAction from './Actions/ShippedAction';
import CoveredAction from './Actions/CoveredAction';
import AfterShipAction from './Actions/AfterShipAction';
import OffcorePartAction from './Actions/OffcorePartAction';
import UploadFileAction from './Actions/UploadFileAction';
import { Row,  Col, Container} from "reactstrap";

class PBHOrderActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeActionButton: ''
    };
    this.setActiveActionButton = this.setActiveActionButton.bind(this)
    this.close = this.close.bind(this)
  }

  pbhActionViewForm() {
    var pbhOrders = this.props.pbhOrders;
    var activeActionButton = this.state.activeActionButton;

	  switch (activeActionButton) {
	  case "": return;
      case "Comment": return <CommentAction close={this.close.bind(this)} exchangeRequestId={this.props.exchangeRequestId} refreshPbhOrdersAndActions={this.props.refreshPbhOrdersAndActions.bind(this)}  clearPbhOrdersAndActions={this.props.clearPbhOrdersAndActions.bind(this)}/>;
      case "Shipped": return <ShippedAction close={this.close.bind(this)} exchangeRequestId={this.props.exchangeRequestId} refreshPbhOrdersAndActions={this.props.refreshPbhOrdersAndActions.bind(this)} clearPbhOrdersAndActions={this.props.clearPbhOrdersAndActions.bind(this)}/>;
      case "Covered?": return <CoveredAction close={this.close.bind(this)} exchangeRequestId={this.props.exchangeRequestId} refreshPbhOrdersAndActions={this.props.refreshPbhOrdersAndActions.bind(this)} clearPbhOrdersAndActions={this.props.clearPbhOrdersAndActions.bind(this)}/>;
      case "OffcorePart": return <OffcorePartAction close={this.close.bind(this)} exchangeRequestId={this.props.exchangeRequestId} refreshPbhOrdersAndActions={this.props.refreshPbhOrdersAndActions.bind(this)} clearPbhOrdersAndActions={this.props.clearPbhOrdersAndActions.bind(this)}/>;
      case "Upload File": return <UploadFileAction close={this.close.bind(this)} exchangeRequestId={this.props.exchangeRequestId} refreshPbhOrdersAndActions={this.props.refreshPbhOrdersAndActions.bind(this)} clearPbhOrdersAndActions={this.props.clearPbhOrdersAndActions.bind(this)}/>;
      case "AfterShip": 
              var filteredPbhOrder = filter(pbhOrders, "Shipped");
              return <AfterShipAction close={this.close.bind(this)} exchangeRequestId={this.props.exchangeRequestId} shipmentContent={filteredPbhOrder.shipmentContent}/>
      case "Instock": 
              var filteredPbhOrder = filter(pbhOrders, "Order");
              return <PartInStockAction close={this.close.bind(this)} pbhOrder={filteredPbhOrder} exchangeRequestId={this.props.exchangeRequestId} refreshPbhOrdersAndActions={this.props.refreshPbhOrdersAndActions.bind(this)} clearPbhOrdersAndActions={this.props. clearPbhOrdersAndActions.bind(this)}/>
    }
    
    function filter(pbhOrders, searchCriteria) {
      var filter =  pbhOrders.filter(function(pbhOrder)
      {
        return pbhOrder.actionType == searchCriteria;
      });

      return filter[0];
    }
  }

  setActiveActionButton(e, activeActionButton){
    e.preventDefault();
    this.state.activeActionButton !== activeActionButton ? this.setState({activeActionButton: activeActionButton}) : null;
  }

  close() {
    this.setState({activeActionButton: ""});
  }

  pbhActionButtons() {
  var pbhActions = this.props.pbhActions;
  return pbhActions.map(pbhAction => 
    <button  key={pbhAction} className="btn btn-primary" onClick={(e) => this.setActiveActionButton(e, pbhAction)}>{pbhAction}</button>
    )
  }
 

	pbhOrderActionsViewMain() {
  const pbhActionButtons = this.pbhActionButtons();
  const pbhActionViewForm = this.pbhActionViewForm();
    return (
      <div>
        <div style={{textAlign:'center'}}>{pbhActionButtons}</div>
        {pbhActionViewForm}
      </div>
    );
  }

  render() {
    return (
      <div  className="animated fadeIn">
        {this.pbhOrderActionsViewMain()}
      </div>
    );
  }
}

function mapStateToProps() {
  return {  
  };
}

const connectedPBHOrderActions = connect(mapStateToProps)(PBHOrderActions);
export default connectedPBHOrderActions;