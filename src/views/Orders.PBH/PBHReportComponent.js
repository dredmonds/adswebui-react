import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Row, Col} from 'reactstrap';

class PBHReportComponentPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  pbhReportComponentMain() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12" lg="12">PBH Report</Col>
        </Row>
      </div>
    );
  }

  render() {
    return (
      <div>{this.pbhReportComponentMain()}</div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const connectedPBHReportComponentPage = connect(mapStateToProps)(PBHReportComponentPage);
export default connectedPBHReportComponentPage;