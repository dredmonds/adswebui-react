import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Row, Col} from 'reactstrap';

class PBHStockComponentPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  pbhStockComponentMain() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12" lg="12">PBH Stock</Col>
        </Row>
      </div>
    );
  }

  render() {
    return (
      <div>{this.pbhStockComponentMain()}</div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const connectedPBHStockComponentPage = connect(mapStateToProps)(PBHStockComponentPage);
export default connectedPBHStockComponentPage;