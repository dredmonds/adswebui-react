import React, {Component} from 'react';
import {connect} from 'react-redux';
import config from 'config';
import {Row, Col, Button, Nav, DropdownToggle, DropdownMenu, DropdownItem, Dropdown, } from 'reactstrap';
import {DataScroller} from 'primereact/datascroller';
import {pbhOrderService} from '../../services';
import {userActions, alertActions} from '../../actions';
import PBHOrdersDataSearchPage from './PBHOrdersDataSearch';
import { ProgressSpinner } from "primereact/progressspinner";

const tableQuery = 
  [{objName: 'creationDate', displayVal: 'Creation Date'},
   {objName: 'customerPO', displayVal: 'PO No.'},
   {objName: 'partNumber', displayVal: 'Part No.'},
   {objName: 'AJWSO', displayVal: 'SO No.'},
   {objName: 'priority', displayVal: 'Priority'},
   {objName: 'companyName', displayVal: 'Customer'},
   {objName: 'status', displayVal: 'Status'}];
const shipPriority = 
  [{code: 0, value: 'AOG'},
   {code: 1, value: 'Imminent AOG/Critical'},
   {code: 2, value: 'IOR/MEL Deferred Defects'},
   {code: 3, value: 'Replenishment of MBK'},
   {code: 4, value: 'Routine'}];

class PBHOrdersDataViewPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortBySplitButtonOpen: false,
      sortBy: 'Creation Date',
      orderBy: 'desc',
      queryStr: null,
      selectedOrder: null,
      loading: false,
      rowCount: 0,
      rows: config.baseDataGridRows * 3, //set buffer to 3x from baseDataGridRows
    };
    this.pbhTableOnFilterQuery.bind(this);
  }
  
  // Methods of events
  componentDidMount() {
    this.datasource = this.props.orders;
    this.setState({
        orders: this.datasource.data,
        rowCount: this.datasource.count
    });
  }

  updateHandleSelectedData() {
    this.props.handleSelectedData(this.state);    
  }

  pbhHandleDataSearch(pbhDataSearchResults) {
    this.datasource = pbhDataSearchResults.data.data;
    this.setState({
      orders: this.datasource,
      rowCount: pbhDataSearchResults.data.count,
      queryStr: pbhDataSearchResults.queryStr,
      queryObjName: pbhDataSearchResults.queryObjName,
    });
  }

  pbhDataSortByToggleSplit(event) {
    this.setState({
      sortBySplitButtonOpen: !this.state.sortBySplitButtonOpen
    });
  }

  pbhOrdersHandleDataSort() {
    let sortBy = this.pbhTableOnFilterQuery(this.state.sortBy);
    let sortedData = this.state.orderBy != 'desc' ?
              this.state.orders.sort((a, b) => (a[`${sortBy}`] > b[`${sortBy}`]) ? 1 : -1 ) :
              this.state.orders.sort((a, b) => (a[`${sortBy}`] > b[`${sortBy}`]) ? -1 : 1 ) ;
    this.setState({orders: sortedData});
  }

  pbhOrdersDataViewOrderBy() {
    return (
      <div style={{textAlign: 'right'}}>
        <Dropdown group size="sm" isOpen={this.state.sortBySplitButtonOpen} toggle={(e) => this.pbhDataSortByToggleSplit(e)}>
          <DropdownToggle color="link" className="nav-link" caret>{this.state.sortBy}&nbsp;</DropdownToggle>
          <DropdownMenu onClick={(e) => this.setState({sortBy: e.target.value, queryStr: ''}, this.pbhOrdersHandleDataSort)}>
            {
              tableQuery.map((table) =>
                <DropdownItem key={table.objName} value={table.displayVal}>
                  {table.displayVal}
                </DropdownItem>
              )
            }
          </DropdownMenu>
          {this.state.orderBy == 'asc' && <Button color="link" onClick={(e) => this.setState({orderBy: 'desc'}, this.pbhOrdersHandleDataSort)} className="fa fa-sort-amount-asc"/> }
          {this.state.orderBy == 'desc' && <Button color="link" onClick={(e) => this.setState({orderBy: 'asc'}, this.pbhOrdersHandleDataSort)} className="fa fa-sort-amount-desc"/> }
        </Dropdown>
      </div>
    );
  }

  pbhTableOnFilterQuery(key) {
    let objNameVal = tableQuery.filter(obj => {return key == obj.displayVal ? obj.objName : null});
    return objNameVal[0].objName;
  }

  pbhOrdersLoadData(event) {
    const isScrollEOL = event.first > 0 && event.first <= this.state.rowCount ? true : false;
    const isEmpty = (str) => { return (!str || 0 === str.length); }

    if(isScrollEOL) {
      this.setState({loading: true});
      let rows = this.state.rows, skip = this.state.orders.length, rowCount = this.state.rowCount;
      let objNameQryA = this.pbhTableOnFilterQuery(this.state.sortBy);

      if(skip <= rowCount) { // get more items if true
        let getPBHOrderData = isEmpty(this.state.queryStr) ?
                  pbhOrderService.getPBHOrdersList(rows, skip) :
                  pbhOrderService.searchPBHOrders(strQryA, objNameQryA, rows, skip) ;

        getPBHOrderData.then(pbhData => {
          this.datasource = [...this.datasource, ...pbhData.data];
          this.setState({orders: this.datasource, loading: false});
        }).catch(error => {this.httpErrorHandler(error)});
      }
    }
  }

  httpErrorHandler(error) {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if(httpStatus >= 500 || httpStatus == 404) {  // Added to handle error(s)
      this.setState({loading: false});
      this.props.dispatch(alertActions.error('Network Connection Error.') );
    } else if(httpStatus == 401) {
      userActions.msalLogout()
    } else {
      this.setState({loading: false});
      this.props.dispatch(failure(error.toString()) );
      this.props.dispatch(alertActions.error(error.toString()) );
    }
  }

  pbhOrdersDataViewItemClick(pbh) {
    let idx1 = document.getElementById(`${pbh.ExchangeRequestId}`);
    let idx2 = this.state.selectedOrder != null ? document.getElementById(`${this.state.selectedOrder.ExchangeRequestId}`) : null;
    if(idx2 != null) {
      idx2.style.backgroundColor = null;
      idx1.style.backgroundColor = '#DCDFDE';
    } else {
      idx1.style.backgroundColor = '#DCDFDE';
    }

    this.props.pbhDataViewToggleTab('1');
    var pbhOderResult= pbhOrderService.searchPBHOrders(pbh.ExchangeRequestId);
    var pbhActions= pbhOrderService.getPBHActions(pbh.ExchangeRequestId);

    if(this.state.selectedOrder == null || this.state.selectedOrder.ExchangeRequestId != pbh.ExchangeRequestId)
    {
      this.props.setExchangeRequestId(pbh.ExchangeRequestId);
      this.props.getPbhOrderPromiseResult(pbhOderResult);
      this.props.getPbhActionPromiseResult(pbhActions);
    }
    this.setState({selectedOrder: pbh}, this.updateHandleSelectedData);
  }

  pbhOrdersDataTemplate(pbh) {
    if(!pbh) { return; }

    const divStyle = {borderBottom: '1px solid #d9d9d9', paddingBottom: '4px', cursor: 'pointer',};
    const rowStyle = {padding: '2px'};
    const col1Style = {padding: 'none'};
    const colNStyle = {padding: 'none'};
    const labelStyle = {fontSize: '0.75em'};
    const activeStyl = {fontSize: '0.65em', color: '#000000', fontWeight: 'bold'};

    const getDateFormat = (dateStr) => {
      let sysLocalFormat = window.navigator.userLanguage || window.navigator.language;
      let date = new Date(dateStr);
      let options = {year: 'numeric', month: '2-digit', day: '2-digit'};
      return new Intl.DateTimeFormat(sysLocalFormat, options).format(date);
    }
    const getShipPriority = (code) => {
      let priority = shipPriority.filter(pri => {return code == pri.code ? pri.value : null});
      return priority[0].value;
    }
    const activeSortBy = this.pbhTableOnFilterQuery(this.state.sortBy);

    return (
      <div id={pbh.ExchangeRequestId} className="list-group-item-action" style={divStyle} onClick={(e) => {this.pbhOrdersDataViewItemClick(pbh)}}>
        <Row style={rowStyle}>
          <Col xs="12" md="4" lg="4" style={col1Style}>
            {activeSortBy == 'creationDate' && <div className="text-muted"><small style={activeStyl}>{`Creation Date`}</small></div>}
            {activeSortBy != 'creationDate' && <div className="text-muted" style={labelStyle}><small>{`Creation Date`}</small></div>}
            <small>{`${getDateFormat(pbh.creationDate)}`}</small>
          </Col>
          <Col xs="12" md="4" lg="4" style={colNStyle}>
            {activeSortBy == 'customerPO' && <div className="text-muted"><small style={activeStyl}>{`Customer PO`}</small></div>}
            {activeSortBy != 'customerPO' && <div className="text-muted" style={labelStyle}><small>{`Customer PO`}</small></div>}
            <small>{pbh.customerPO}</small>
          </Col>
          <Col xs="12" md="4" lg="4" style={colNStyle}>
            {activeSortBy == 'partNumber' && <div className="text-muted"><small style={activeStyl}>{`Mfg Part#`}</small></div>}
            {activeSortBy != 'partNumber' && <div className="text-muted" style={labelStyle}><small>{`Mfg Part#`}</small></div>}
            <small>{pbh.partNumber}</small>
          </Col>
        </Row>
        <Row style={rowStyle}>
          <Col xs="12" md="4" lg="4" style={col1Style}>
            {activeSortBy == 'AJWSO' && <div className="text-muted"><small style={activeStyl}>{`AJW SO`}</small></div>}
            {activeSortBy != 'AJWSO' && <div className="text-muted" style={labelStyle}><small>{`AJW SO`}</small></div>}
            <small>{pbh.AJWSO}</small>
          </Col>
          <Col xs="12" md="4" lg="4" style={colNStyle}>
            {activeSortBy == 'priority' && <div className="text-muted"><small style={activeStyl}>{`Priority`}</small></div>}
            {activeSortBy != 'priority' && <div className="text-muted" style={labelStyle}><small>{`Priority`}</small></div>}
            <small>{`${getShipPriority(pbh.priority)}`}</small>
          </Col>
          <Col xs="12" md="4" lg="4" style={colNStyle}>
            {activeSortBy == 'Address' && <div className="text-muted"><small style={activeStyl}>{`Address`}</small></div>}
            {activeSortBy != 'Address' && <div className="text-muted" style={labelStyle}><small>{`Address`}</small></div>}
            <small>{pbh.address}</small>
          </Col>
          <Col xs="12" md="4" lg="4" style={colNStyle}>
          </Col>
        </Row>
        <Row style={rowStyle}>
          <Col xs="12" md="4" lg="4" style={col1Style}>
            {activeSortBy == 'companyName' && <div className="text-muted"><small style={activeStyl}>{`Customer`}</small></div>}
            {activeSortBy != 'companyName' && <div className="text-muted" style={labelStyle}><small>{`Customer`}</small></div>}
            <small>{pbh.companyName}</small>
          </Col>
          <Col xs="12" md="4" lg="4" style={colNStyle}>
            {activeSortBy == 'status' && <div className="text-muted"><small style={activeStyl}>{`Status`}</small></div>}
            {activeSortBy != 'status' && <div className="text-muted" style={labelStyle}><small>{`Status`}</small></div>}
            <small>{pbh.status}</small>
          </Col>
          <Col xs="12" md="4" lg="4" style={colNStyle}>
            {activeSortBy == 'DateNeeded' && <div className="text-muted"><small style={activeStyl}>{`DateNeeded`}</small></div>}
            {activeSortBy != 'DateNeeded' && <div className="text-muted" style={labelStyle}><small>{`DateNeeded`}</small></div>}
            <small>{`${getDateFormat(pbh.dateNeeded)}`}</small>

          </Col>
        </Row>
      </div>
    );
  }

  pbhOrdersDataViewHeader() {
    return (
      <div>
        <Row>
          <Col xs="12" md="8" lg="8" style={{ padding: '5px' }}>
          <PBHOrdersDataSearchPage 
              pbhHandleDataSearch={this.pbhHandleDataSearch.bind(this)}
              refreshPbhOrdersAndActions ={this.props.refreshPbhOrdersAndActions.bind(this)}
              rows={this.state.rows} /> 
          </Col>
          <Col xs="12" md="4" lg="4" style={{padding: '5px'}}>
            {this.pbhOrdersDataViewOrderBy()}
          </Col>
        </Row>
      </div>
    );
  }

  pbhOrdersDataViewFooter() {
    return (
      <div>
        {this.state.loading && <div style={{padding: '2px 0', border: '1px solid #c2cfd6'}} className="text-center"><i className="fa fa-circle-o-notch fa-spin fa-1x fa-fw" /> </div>}
        {this.state.loading && this.state.rowCount <= 0 && <div style={{ textAlign: 'center' }}> <ProgressSpinner /> </div>}
        {!this.state.loading && this.state.rowCount == 0 && <div style={{ textAlign: 'center' }}> No results found </div>}
      </div>
    );
  }

  PBHOrdersDataViewMain() {
    const pbhDataViewHeader = this.pbhOrdersDataViewHeader();
    const pbhDataViewFooter = this.pbhOrdersDataViewFooter();
	const pbhRetrivingState = this.props.retrievingPBH;
    return (
      <div className="animated fadeIn">
        <div id="pbhDataScrollerContainer">
          <DataScroller value={this.state.orders} id="pbhDataScroller" style={{ padding: '0.25em 0.25em !important' }}
            header={pbhDataViewHeader}
            inline={true}
            rows={this.state.rows}
            buffer={0.9}
            scrollHeight="800px"
            lazy={true}
            onLazyLoad={this.pbhOrdersLoadData.bind(this)}
            itemTemplate={this.pbhOrdersDataTemplate.bind(this)} />
          {pbhDataViewFooter}
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.props.orders && this.PBHOrdersDataViewMain()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {orders, retrievingPBH} = state.pbhorder;
  return {orders, retrievingPBH};
}

const connectedPBHOrdersDataViewPage = connect(mapStateToProps)(PBHOrdersDataViewPage);
export default connectedPBHOrdersDataViewPage;