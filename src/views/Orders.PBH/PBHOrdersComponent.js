import React, {Component} from 'react';
import {connect} from 'react-redux';
import config from 'config';
import {pbhOrderService} from '../../services';
import {Row, Col, Card, CardBody, CardTitle, Nav,  NavItem,  NavLink} from 'reactstrap';
import {pbhOrdersActions} from '../../actions';
import { ProgressSpinner } from "primereact/progressspinner";
import PBHOrdersDataViewPage from './PBHOrdersDataView';
import PBHOrdersDetailViewPage from './PBHOrdersDetailView';
import classnames from "classnames";

class PBHOrdersComponentPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOrder: null,
      pbhMainActiveTab: '1',
      pbhDetailViewNavActiveTab: '1',
      pbhOrders: null,
      pbhActions: null,
      exchangeRequestId : null
    };
    this.pbhDataViewToggleTab = this.pbhDataViewToggleTab.bind(this);
    this.getPbhOrderPromiseResult = this.getPbhOrderPromiseResult.bind(this);
    this.getPbhActionPromiseResult = this.getPbhActionPromiseResult.bind(this);
    this.setExchangeRequestId = this.setExchangeRequestId.bind(this);
    this.refreshPbhOrdersAndActions = this.refreshPbhOrdersAndActions.bind(this);
    this.clearPbhOrdersAndActions = this.clearPbhOrdersAndActions.bind(this);
  }
  
  refreshPbhOrdersAndActions() {
    var pbhOrders = pbhOrderService.searchPBHOrders(this.state.exchangeRequestId);
    var pbhActions = pbhOrderService.getPBHActions(this.state.exchangeRequestId);
    this.getPbhOrderPromiseResult(pbhOrders);
    this.getPbhActionPromiseResult(pbhActions);
  }

  clearPbhOrdersAndActions() {
    this.setPbhOrder(null);
    this.setPbhActions(null); 
  }


  setExchangeRequestId(exchangeRequestId){
    this.state.exchangeRequestId !== exchangeRequestId ? this.setState({exchangeRequestId: exchangeRequestId}) : null;
  }

  pbhDataViewToggleTab(tab) {
    this.state.pbhDetailViewNavActiveTab !== tab ? this.setState({pbhDetailViewNavActiveTab: tab}) : null;
  }

  setPbhOrder(pbhOrders) {
    this.state.pbhOrders !== pbhOrders ? this.setState({pbhOrders: pbhOrders}) : null;    
  }

  setPbhActions(pbhActions) {
    this.state.pbhActions !== pbhActions ? this.setState({pbhActions: pbhActions}) : null;
  }

  getPbhOrderPromiseResult(pbhOrders) {
    this.setPbhOrder(null)
    pbhOrders.then(pbhOrder => {
      var pbhOrderArray = [];
      pbhOrder.count.map(orderObj => {
        pbhOrderArray.push(orderObj)
      });
      this.setPbhOrder(pbhOrderArray)
    })
    .catch(error => {
      console.log('error retrieving Pbh Order:' + error);
    })
  }

  getPbhActionPromiseResult(pbhActions) {
    this.setPbhActions(null)
    pbhActions.then(pbhAction => {
      var pbhActionArray = [];
      pbhAction.data.map(orderObj => {
        pbhActionArray.push(orderObj)
      })      
      this.setPbhActions(pbhActionArray)
    })
    .catch(error => {
      console.log('error retrieving Pbh actions:' + error);
    })
  }

  pbhDataViewSelectedHandler(item) {
    this.setState({selectedOrder: item});
  }

  pbhMainToggleTab(tab) {
    this.state.pbhMainActiveTab !== tab ? this.setState({pbhMainActiveTab: tab}) : null;
  }

  pbhOrderSummaryPage() {
    return (
      <div>
        <div name="summaryHeader" style={{backgroundColor: '#f0f3f5', height: '40px'}}>
        </div>

        <div name="summaryBody" style={{height: '852.3px'}}>
          <Row>
            <Col xs="12" sm="12" lg="12">
              <Card style={{height: '852.3px'}}>
                <CardBody>
                  <Row>
                    <Col xs="12" sm="12" lg="12">
                      <div className="dashboard-box orders">
                        <i className="fa fa-cogs" />
                        <div className="chart-wrapper"></div>
                        <ul>
                          <li>
                            <strong>123</strong>
                            <span>Order Shipped</span>
                          </li>
                          <li>
                            <strong>114</strong>
                            <span>Working Request</span>
                          </li>
                          <li>
                            <strong>135</strong>
                            <span>Order In Progress</span>
                          </li>
                        </ul>
                      </div>
                    </Col>
                  </Row> 
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }

  pbhOrdersComponentMain() {
    const pbhOrdersSummaryPage = this.pbhOrderSummaryPage();
    const pbhOrdersDetailViewPage = <PBHOrdersDetailViewPage handleSelectedData={this.pbhDataViewSelectedHandler.bind(this)} pbhDataViewToggleTab={this.pbhDataViewToggleTab.bind(this)} pbhDetailViewNavActiveTab={this.state.pbhDetailViewNavActiveTab} pbhOrders={this.state.pbhOrders} pbhActions={this.state.pbhActions} exchangeRequestId={this.state.exchangeRequestId} clearPbhOrdersAndActions={this.clearPbhOrdersAndActions.bind(this)} refreshPbhOrdersAndActions={this.refreshPbhOrdersAndActions.bind(this)} setExchangeRequestId={this.setExchangeRequestId.bind(this)} getPbhOrderPromiseResult={this.getPbhOrderPromiseResult.bind(this)} getPbhActionPromiseResult={this.getPbhActionPromiseResult.bind(this)}/>;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col style={{marginTop:'10px', marginBottom:'3px'}}>
          <Nav pills style={{ color: "#3e515b" }}>
            <NavItem>
              <NavLink
                className={classnames({
                  active: this.state.pbhDetailViewNavActiveTab === "0"
                })}
                onClick={() => {
                  this.pbhDataViewSelectedHandler('1');
                  this.pbhDataViewToggleTab("0");
                }}
                style={{ cursor: "pointer", border:'1px solid #000'}}
              >
                <div style={{ textAlign: "center" }}>
                  <i className="fa fa-plus" />
                  <div>
                    <small>New Request</small>
                    </div>
                  </div>
                </NavLink>
              </NavItem>

              <NavItem>
              <NavLink onClick={() => {this.props.dispatch(pbhOrdersActions.getPBHOrders(17));}} style={{ cursor: "pointer", border:'1px solid #000', marginLeft: '10px'}}>
                <div style={{ textAlign: "center" }}>
                  <i className="fa fa-refresh" />
                  <div>
                    <small>Refresh PBH Orders</small>
                    </div>
                  </div>
                  </NavLink>
              </NavItem>
            </Nav>
          </Col>
          </Row>
        <Row>
          <Col xs="12" md="5" lg="5" style={{paddingRight: '0px'}}>
            <div>{this.props.orders && <PBHOrdersDataViewPage handleSelectedData={this.pbhDataViewSelectedHandler.bind(this)} pbhDataViewToggleTab={this.pbhDataViewToggleTab.bind(this)} getPbhOrderPromiseResult={this.getPbhOrderPromiseResult.bind(this)} getPbhActionPromiseResult={this.getPbhActionPromiseResult.bind(this)} setExchangeRequestId={this.setExchangeRequestId.bind(this)} refreshPbhOrdersAndActions={this.refreshPbhOrdersAndActions.bind(this)} />}</div>
            <div>{!this.props.orders && <div style={{ textAlign: 'center' }}><ProgressSpinner /></div>}</div>
          </Col>
          <Col xs="12" md="7" lg="7" style={{paddingLeft: '0px'}}>
            {!this.state.selectedOrder && <div>{pbhOrdersSummaryPage}</div>}
            {this.state.selectedOrder && <div>{pbhOrdersDetailViewPage}</div>}
          </Col>
        </Row>
      </div>
    );
  }

  render() {
    const {retrievingPBH} = this.props;
    const data = this.props.orders;
    if(!data && !retrievingPBH) {
      let dataRowsCache = config.baseDataGridRows * 3; //set buffer to 3x from baseDataGridRows
      this.props.dispatch(pbhOrdersActions.getPBHOrders(dataRowsCache));      
    }
    return (
      <div>
        {this.pbhOrdersComponentMain()}
        </div>
    );
  }
}

function mapStateToProps(state) {
  const {orders, retrievingPBH} = state.pbhorder;
  return {
    orders, retrievingPBH
  };
}

const connectedPBHOrdersComponentPage = connect(mapStateToProps)(PBHOrdersComponentPage);
export default connectedPBHOrdersComponentPage;