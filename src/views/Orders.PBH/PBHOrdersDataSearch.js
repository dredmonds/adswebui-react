import React, {Component}  from 'react';
import {connect} from 'react-redux';
import {Button, InputGroup, Input, InputGroupButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, } from 'reactstrap';
import {pbhOrderService} from '../../services'
import {userActions, alertActions} from '../../actions';
import {pbhOrdersActions} from '../../actions';

const tableQuery = 
  [//{objName: 'creationDate', displayVal: 'Creation Date'},
   {objName: 'customerPO', displayVal: 'PO No.'},
   {objName: 'partNumber', displayVal: 'Part No.'},
   {objName: 'AJWSO', displayVal: 'SO No.'},
   {objName: 'priority', displayVal: 'Priority'},
   {objName: 'companyName', displayVal: 'Customer'},
   {objName: 'status', displayVal: 'Status'}];
const shipPriority = 
  [{code: 0, value: 'AOG'},
   {code: 1, value: 'Imminent AOG/Critical'},
   {code: 2, value: 'IOR/MEL Deferred Defects'},
   {code: 3, value: 'Replenishment of MBK'},
   {code: 4, value: 'Routine'}];

class PBHOrdersDataSearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchSplitButtonOpen: false,
      queryStr: '',
      searchBy: 'PO No.',
      data: [],
      searching: false,
    };
  }

  // Methods of events
  componentWillMount() {
    this.timer =  null;
  }

  updateHandleDataSearch() {
    this.props.pbhHandleDataSearch(this.state);
  }

  pbhDataSearchToggleSplit(event) {
    this.setState({
      searchSplitButtonOpen: !this.state.searchSplitButtonOpen
    });
  }

  pbhTableOnFilterQuery(key) {
    let objNameVal = tableQuery.filter(obj => {return key == obj.displayVal ? obj.objName : null});
    return objNameVal[0].objName;
  }

  pbhDataViewSearchOnChange(event) {
    const {value} = event.target;
    clearTimeout(this.timer);
    this.setState({queryStr: value, searching: true}, () => {
      this.timer = setTimeout(() => {
        this.pbhOrdersOnSearch(value);
      },1000);
    });
  }

  pbhOrdersOnSearch(queryStr) {
    let searchBy = this.state.searchBy, rows = this.props.rows, skip = 0;
    let objNameQryA = this.pbhTableOnFilterQuery(searchBy);

    const isEmpty = (str) => { return (!str || 0 === str.length); }
    const getPriCode = (str) => {
      let lowerStr = str.toLowerCase();
      let priority = shipPriority.filter(pri => pri.value.toLowerCase().includes(lowerStr) == true );
      return priority.length > 0 ? priority[0].code : shipPriority.length; // if false, set value as fallback
    }

    let strQryA = objNameQryA == 'priority' ? getPriCode(queryStr) : queryStr ; // get priority code that includes str
    
    
    const pbhOrderDataSearch = isEmpty(strQryA) ? 
                pbhOrderService.getPBHOrdersList(rows, skip) :
                pbhOrderService.searchPBHOrderByQuery(strQryA, objNameQryA, rows, skip) ;

    pbhOrderDataSearch.then(result => 
      {
      // set data results to state and update data handler
      this.setState({data: result, queryObjName: objNameQryA, queryStr: queryStr, searching: false}, this.updateHandleDataSearch);
    })
    .catch(error => 
      {
        this.httpErrorHandler(error);
      });
  }

  httpErrorHandler(error) {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if(httpStatus >= 500 || httpStatus == 404) {  // Added to handle error(s)
      this.setState({loading: false});
      this.props.dispatch(alertActions.error('Network Connection Error.') );
    } else if(httpStatus == 401) {
      userActions.msalLogout()
    } else {
      this.setState({loading: false});
      this.props.dispatch(failure(error.toString()) );
      this.props.dispatch(alertActions.error(error.toString()) );
    }
  }

  pbhOrdersDataViewSearch() {
    return (
      <div>
        <InputGroup size="sm">
          <InputGroupButtonDropdown addonType="prepend" isOpen={this.state.searchSplitButtonOpen} toggle={(e) => this.pbhDataSearchToggleSplit(e)}>
            {this.state.searchBy && <Button><i className="fa fa-search"/>&nbsp;&nbsp;By: {this.state.searchBy}</Button> }
            <DropdownToggle split outline/>
            <DropdownMenu onClick={(e) => this.setState({searchBy: e.target.value, queryStr: ''})}>
              {
                tableQuery.map((table) =>
                  <DropdownItem key={table.objName} value={table.displayVal}>
                    {table.displayVal}
                  </DropdownItem>
                )
              }              
            </DropdownMenu>            
            </InputGroupButtonDropdown>
          <Input type="text" name="keySearch" id="inputKey" placeholder="key search" onChange={this.pbhDataViewSearchOnChange.bind(this)} value={this.state.queryStr} />
          {this.state.searching && <div style={{padding: '5px 0'}} className="text-center">&nbsp;&nbsp;<i className="fa fa-spinner fa-pulse fa-1x fa-fw" /></div> }
        </InputGroup>
      </div>
    );
  }

  render() {
    return (
      <div>{this.pbhOrdersDataViewSearch()}</div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const connectedPBHOrdersDataSearchPage = connect(mapStateToProps)(PBHOrdersDataSearchPage);
export default connectedPBHOrdersDataSearchPage;