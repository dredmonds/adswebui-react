import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Container, Form, FormFeedback, Row, Col, CardGroup, Card, CardBody, Button, Input, InputGroup, InputGroupAddon, InputGroupText, Table} from 'reactstrap';

import { userActions } from '../../actions';

class LoginPage extends Component {
  constructor(props) {
    super(props);

    // reset login status
    // this.props.dispatch( userActions.logout() );
    this.state = {
      username: '',
      password: '',
      submitted: false
    };
    // Binds methods of events
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNavLink = this.handleNavLink.bind(this);
    this.handleAdalAuth = this.handleAdalAuth.bind(this);
  }

  componentDidMount() {
  }

  // Methods of events
  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ submitted: true });
    const { username, password } = this.state;
    const { dispatch } = this.props;
    if(username && password) {
      dispatch( userActions.login(username, password) );
    }
  }

  handleNavLink() {
    this.props.history.push('/register');
  }
  
  handleAdalAuth() {
    this.props.dispatch(userActions.msalLogin());
  }

  render() {
    const { loggingIn, msalLoggingIn } = this.props;
    const { username, password, submitted } = this.state;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form name="loginForm" id="loginForm" onSubmit={this.handleSubmit}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" name="username" id="username" placeholder="Username" invalid={submitted && !username} value={username} onChange={this.handleChange} />
                        <FormFeedback>Username is required</FormFeedback>
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" name="password" id="password" placeholder="Password" invalid={submitted && !password} value={password} onChange={this.handleChange} />
                        <FormFeedback>Password is required</FormFeedback>
                      </InputGroup>
                      <Row>
                        <Col xs="12" sm="6">
                          <Button color="primary" block className="px-4">
                            {loggingIn && <i className="fa fa-spinner fa-pulse fa-fw" />}&nbsp;&nbsp;
                            Login
                          </Button>
                        </Col>
                        <Col xs="12" sm="6" className="text-right">
                          <Button color="link" className="px-0" block>Forgot password?</Button>
                        </Col>
                      </Row>
                      <br/>
                      <Row className="text-center">
                        <Col xs="12" sm="12">- OR -</Col>
                      </Row>
                      <br/>
                      <Row>
                        <Col xs="12" sm="12">
                          <Button outline color="primary" block onClick={this.handleAdalAuth}>
                            {msalLoggingIn && <i className="fa fa-spinner fa-pulse fa-fw" />}&nbsp;&nbsp;
                            <i className="fa fa-windows"/>&nbsp;&nbsp;Logon using Microsoft Account
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white  py-5 d-md-down-none" style={{ width: 44 + '%', background: 'rgba(43, 15, 84, 1.00)' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Welcome</h2>
                      <h3>AJW Digital Services</h3>
                      <p>[Solutions tag line here]</p>
                      <Button onClick={this.handleNavLink} color="primary" className="mt-3" active>Register Now!</Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { loggingIn, msalLoggingIn } = state.authentication;
  return { loggingIn, msalLoggingIn };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export default connectedLoginPage;
