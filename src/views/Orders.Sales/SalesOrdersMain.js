import React, {Component} from 'react';
import {connect} from 'react-redux';

import { Row, Col, Card, CardTitle, CardSubtitle, CardHeader, CardBody } from 'reactstrap';
import {TabMenu} from 'primereact/tabmenu';
import {Chart} from 'primereact/chart';

import SalesOrdersDatagrid from './SalesOrdersDataGrid';
import PurchaseOrderDataGrid from './PurchaseOrdersDataGrid';
import InvoicesDataGrid from './InvoicesDataGrid';

class SalesOrdersMainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItemIndex: 0,
    };
    // Bind methods
    this.onClickMenuTab = this.onClickMenuTab.bind(this);
  }

  // Method of events
  onClickMenuTab(event) {
    this.setState({
      activeItemIndex: event
    });
  }

  getDoughnutChartData() {
    // Gathered individual tab item data
    return [];
  }

  getStackedChartData(scope) {
    // Gathered individual tab item data based from summary scope
    return [];
  }

  getStackedChartScopeData(scope) {
    // Returned summary scope data
    let scopeVal = 3, scopeArr = [];
    let d = new Date();
    for(i = 0; i < scopeVal; i--) {
    }
    return [];
  }

  salesOrdersMain() {
    const tabItems = [
      {label: 'Orders', icon: 'fa fa-list-ul'},
      {label: 'Exchanges', icon: 'fa fa-cogs'},
      {label: 'Invoices', icon: 'fa fa-file-text-o'},
    ];
    const data = this.props.orders;
    let sOrders = data.sOrders.count;
    let pOrders = data.pOrders.count;
    let invoices = data.invoices.count;
    const doughnutChartData = {
      labels: [`${tabItems[0].label}`, `${tabItems[1].label}`, `${tabItems[2].label}`],
      datasets: [{
        data: [sOrders, pOrders, invoices],
        backgroundColor: [
          "#FF6384",
          "#36A2EB",
          "#FFCE56"
        ],
        hoverBackgroundColor: [
          "#FF6384",
          "#36A2EB",
          "#FFCE56"
        ]
        }],
    };
    const doughnutChartOptions = {
      responsive: true,
      onClick: (evt, element) => {
        element.length != 0 ? this.onClickMenuTab(element[0]._index) : null;
      },
      legend: {
        onClick: (evt, element) => {
          element ? this.onClickMenuTab(element.index) : null;
        }
      },
    };
    const stackedChartData = {
      labels: [`${'January'}`,`${'February'}`,`${'March'}`],
      datasets: [{
        type: 'bar',
        label: `${tabItems[0].label}`,
        backgroundColor: '#66BB6A',
        data: [50, 25, 12]
      }, {
        type: 'bar',
        label: `${tabItems[1].label}`,
        backgroundColor: '#FFCA28',
        data: [21, 84, 24]
      }, {
        type: 'bar',
        label: `${tabItems[2].label}`,
        backgroundColor: '#42A5F5',
        data: [41, 52, 24]
      }],
    };
    const stackedChartOptions = {
      tooltips: {mode: 'index', intersect: false},
      responsive: true,
      scales: {
        xAxes: [{stacked: true}],
        yAxes: [{stacked: true}]
      }
    };

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12" lg="12">
            <Card>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" lg="6">
                    <CardTitle><i className="fa fa-list-alt" />&nbsp;&nbsp;Orders</CardTitle>
                    <CardSubtitle>Customer Sales Orders</CardSubtitle>
                  </Col>
                  <Col xs="12" sm="6" lg="6" className="text-lg-right text-sm-right text-xs-left"></Col>
                </Row>
                <hr />
                <Row>
                  <Col xs="12" sm="6" lg="6">
                    <CardSubtitle>General Summary</CardSubtitle>
                    <div className="doughnut-chart implementations">
                      <Chart id='doughnutChart' type="doughnut" data={doughnutChartData} options={doughnutChartOptions} 
                            style={{width: '380px', height: '170px'}} />
                    </div>
                  </Col>
                  <Col xs="12" sm="6" lg="6">
                    <CardSubtitle>Quarter Summary</CardSubtitle>
                    <div className="stacked-chart implementations">
                      <Chart type="bar" data={stackedChartData} options={stackedChartOptions} style={{width: '380px', height: '170px'}} />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardBody>
                <TabMenu model={tabItems} activeItem={tabItems[this.state.activeItemIndex]} onTabChange={(e) => this.onClickMenuTab( tabItems.indexOf(e.value) ) } />
                {this.state.activeItemIndex == 0 && <SalesOrdersDatagrid tabValue={tabItems[0].label}/> }
                {this.state.activeItemIndex == 1 && <PurchaseOrderDataGrid tabValue={tabItems[1].label}/>}
                {this.state.activeItemIndex == 2 && <InvoicesDataGrid tabValue={tabItems[2].label}/>}
              </CardBody>
            </Card>
            
          </Col>
        </Row>
      </div>
    );
  }

  render() {
    const data = this.props.orders;
    return (
      <div>
        {this.props.retrievingSO && <div style={{padding: '25px 0'}} className="text-center"><i className="fa fa-spinner fa-pulse fa-2x fa-fw" /> &nbsp;</div>}
        {data && this.salesOrdersMain()}
      </div>
    );
  }

}

function mapStateToProps(state) {
  const {orders, retrievingSO} = state.salesorder;
  return {
    orders, retrievingSO
  };
}

const connectedSalesOrdersMainPage = connect(mapStateToProps)(SalesOrdersMainPage);
export default connectedSalesOrdersMainPage;