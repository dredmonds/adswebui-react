import React, {Component} from 'react';
import {connect} from 'react-redux';
import config from 'config';

import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Sidebar} from 'primereact/sidebar';
import {Dialog} from 'primereact/dialog';

import {salesOrderService} from '../../services';
import {userActions, alertActions} from '../../actions';
import {entityDataGenerator} from '../../helpers';

import SalesOrdersControls from './SalesOrdersControls';
import DataGridRowDetails from './DataGridRowDetails';

const odataEntityQuery = 
  [{tabVal: 'Orders', objNameQryA: 'routeCode', strQryA: 'S', objNameQryB: 'itemNumber', strQryB: 1}, 
   {tabVal: 'Exchanges', objNameQryA: 'routeCode', strQryA: 'E', objNameQryB: 'itemNumber', strQryB: 1}, 
   {tabVal: 'Invoices', objNameQryA: 'invocieNumber', strQryA: ''}];

class InvoicesDataGrid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first: 0,
      rows: config.baseDataGridRows,
      rowCount: 0,
      queryStr: null,
      queryObjName: null,
      loading: true,
      selectedItem: null,
      visibleSidebarR: false,
      visibleSidebarF: false,
      visibleDialog: false
    };
  }

  componentDidMount() {
    this.datasource = this.props.orders.invoices.data;
    this.setState({
      orders: this.datasource.slice(this.state.first, this.state.rows),
      rowCount: this.props.orders.invoices.count,
      loading: false
    });
  }

  handleQueryData(queryDataResults) {
    this.datasource = queryDataResults.data.data;
    this.setState({
      first: queryDataResults.skip,
      rows: queryDataResults.rows,
      orders: this.datasource.slice(queryDataResults.skip, queryDataResults.rows),
      rowCount: queryDataResults.data.count,
      queryStr: queryDataResults.queryStr,
      queryObjName: queryDataResults.queryObjName,
      loading: false
    });
  }

  onRowIndexTemplate(rowObj) {
    return (
      <div className="text-center">
        <div className="btn-group" role="group">
          <button type="button" className="btn btn-link" style={{fontSize: '1em'}} onClick={(e) => this.setState({visibleSidebarR: true, selectedItem: rowObj })}>
            {/* {rowObj.SaleOrderDetailAutoKey} */}
            {rowObj.invocieNumber}
          </button>
        </div>
      </div>
    );
  }

  onRowExpandTemplate(rowObj) {
    return <DataGridRowDetails datagridRowData={rowObj} tabValue={this.props.tabValue} loading={true} />;
  }

  onRowActionsClickEvent(rowObj) {
    this.setState({selectedItem: rowObj.data});
  }

  onRowActionsTemplate(rowObj) {
    return (
      <div>
        <div className="btn-group" role="group">
          <button type="button" className="btn btn-link" onClick={(e) => this.setState({visibleSidebarF: true, selectedItem: rowObj })}>
            <i className="pi pi-window-maximize"></i>
          </button>
        </div>
      </div>
    );
  }

  onRowDoubleClickEvent(rowObj) {
    this.setState({selectedItem: rowObj.data, visibleDialog: true});
  }

  onRowDateFormatTemplate(rowObj) {
    const sysLocalFormat = window.navigator.userLanguage || window.navigator.language;
    let date = new Date(rowObj.dateCreated);
    let options = {year: 'numeric', month: '2-digit', day: '2-digit'};
    return (
      <div className="text-center"> { 
          new Intl.DateTimeFormat(sysLocalFormat, options).format(date)
        }
      </div>
    );
  }

  onRowSOrderStatusTemplate(rowObj) {
    return (
      <div> 
        { rowObj.openFlag == 'T' && <div style={{padding: "3px 3px", textAlign: "center"}}>{rowObj.soNumber}</div> }
        { rowObj.openFlag == 'F' && <div style={{padding: "3px 3px", textAlign: "center"}}>{rowObj.soNumber}</div> }
      </div>
    );
  }

  onRowPOrderStatusTemplate(rowObj) {
    return (
      <div>
        { rowObj.openFlag == 'T' && <div style={{padding: "3px 3px", textAlign: "center"}}>{rowObj.companyRefNumber}</div> } 
        { rowObj.openFlag == 'F' && <div style={{padding: "3px 3px", textAlign: "center"}}>{rowObj.companyRefNumber}</div> }
      </div>
    );
  }

  onRowOrderShipingTo(rowObj) {
    return (
      <div>
        {`${rowObj.shipAddress1}, ${rowObj.shipAddress2}`}
      </div>
    );
  }

  onPageChange(event) {
    const startIndex = event.first;
    const pageRows = event.rows;
    const endIndex = event.first + event.rows;
    
    let EOF = endIndex > this.datasource.length && this.datasource.length < this.state.rowCount ? true : false;

    if(EOF) {
      this.setState({loading: true});
      this.state.queryStr == null ? this.onPageHttpSkipNext(pageRows, startIndex) : this.onPageHttpSkipQuery(pageRows, startIndex) ;
    } else {
      this.setState({
        first: startIndex, 
        rows: pageRows,
        orders: this.datasource.slice(startIndex, endIndex),
        loading: false
      });
    }
  }

  onPageHttpSkipNext(pageRows, startIndex) {
    let strQryA = odataEntityQuery[2].strQryA; //Get Invoices with qtyInvoiced >= 1
    let objNameQryA = odataEntityQuery[2].objNameQryA;
    let getInvoiceDataList = salesOrderService.searchInvoicedList(strQryA, objNameQryA, pageRows, startIndex);
    
    getInvoiceDataList.then(invoiceData => {
      // let getInvoiceDataGen = entityDataGenerator.linkShipmentToOrder(invoiceData.data);
      let getInvoiceDataGen = entityDataGenerator.NEWlinkShipmentToOrder(invoiceData.data);
      
      getInvoiceDataGen.then(invoicedData => {
        this.datasource = [...this.datasource, ...invoicedData];
        this.setState({
          first: startIndex,
          rows: pageRows,
          orders: invoicedData,
          loading: false
        });
      }).catch(error => {this.httpErrorHandler(error)});
    }).catch(error => {this.httpErrorHandler(error)});
  }

  onPageHttpSkipQuery(pageRows, startIndex) {
    let strQryA = this.state.queryStr;
    let objNameQryA = this.state.queryObjName;
    let strQryB = 1;                // added to filter an orders with line item 1 only
    let objNameQryB = 'itemNumber'; // added to filter queries to eliminates duplicate entries
    let rows = pageRows;
    let skip = startIndex;

    const invoiceSkipQuery = objNameQryA != 'invocieNumber' ? 
              salesOrderService.searchOrders(strQryA, objNameQryA, strQryB, objNameQryB, rows, skip) :
              salesOrderService.searchInvoicedList(strQryA, objNameQryA, rows, skip);

    invoiceSkipQuery.then(result => {
      if(result.data.length > 0) {
        this.doSkipQuerySearch(result, objNameQryA, rows, skip);
      } else {
        this.setState({
          first: skip, rows: rows,
          orders: {data: {count: 0, data: []}},
          loading: false
        });
      }
    }, error => {this.httpErrorHandler(error)});
  }

  doSkipQuerySearch(result, objNameQryA, rows, skip) {
    if(objNameQryA != 'invocieNumber') {
      this.doLinkOrderToShipment(result, objNameQryA, rows, skip);
    } else {
      this.doLinkShipmentToOrder(result, objNameQryA, rows, skip);
    }
  }

  doLinkOrderToShipment(result, objNameQryA, rows, skip) {
    // let getInvoiceDataGen = entityDataGenerator.linkOrderToShipment(result.data);
    let getInvoiceDataGen = entityDataGenerator.NEWlinkOrderToShipment(result.data);
    getInvoiceDataGen.then(invoicedData => {
      this.datasource = [...this.datasource, ...invoicedData];
      this.setState({
        first: skip,
        rows: rows,
        orders: invoicedData,
        rowCount: result.count,
        loading: false
      });
    }, error => {this.httpErrorHandler(error)});
  }

  doLinkShipmentToOrder(result, objNameQryA, rows, skip) {
    // let getInvoiceDataGen = entityDataGenerator.linkShipmentToOrder(result.data);
    let getInvoiceDataGen = entityDataGenerator.NEWlinkShipmentToOrder(result.data);
    getInvoiceDataGen.then(invoicedData => {
      this.datasource = [...this.datasource, ...invoicedData];
      this.setState({
        first: skip,
        rows: rows,
        orders: invoicedData,
        rowCount: result.count,
        loading: false
      });
    }, error => {this.httpErrorHandler(error)});
  }

  httpErrorHandler(error) {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if(httpStatus >= 500 || httpStatus == 404) {  // Added to handle error(s)
      this.setState({loading: false});
      this.props.dispatch(alertActions.error('Network Connection Error.') );
    } else if(httpStatus == 401) {
      userActions.msalLogout()
    } else {
      this.setState({loading: false});
      this.props.dispatch(failure(error.toString()) );
      this.props.dispatch(alertActions.error(error.toString()) );
    }
  }

  invoicesDataGrid() {
    const _expanderStyle = {width: '4%', textAlign: 'center', padding: '0.5em 0.25em'};
    const _invStyle = {width: '11%', textAlign: 'center', padding: '0.50em 0.25em'};
    const _actionStyle = {width: '7%', textAlign: 'center', padding: '0.5em 0.25em'};
    const tableColumns = [
      {field: null, header: null, expander: true, expanderStyle: _expanderStyle},
      {field: 'invocieNumber', header: 'Invoice No.', body: (this.onRowIndexTemplate.bind(this)), sortable: false, filter: false, bodyStyle: _invStyle, headerStyle: _invStyle },
      {field: 'dateCreated', header: 'Date', body: (this.onRowDateFormatTemplate), sortable: false, filter: false, headerStyle: _invStyle },
      {field: 'soNumber', header: 'SO Ref', body: (this.onRowSOrderStatusTemplate), sortable: false, filter: false, headerStyle: _invStyle },
      {field: 'companyRefNumber', header: 'PO Ref', body: (this.onRowPOrderStatusTemplate), sortable: false, filter: false, headerStyle: _invStyle },
      {field: 'numberOfItems', header: 'Line Items', sortable: false, filter: false, bodyStyle: {textAlign: 'center'}, headerStyle: {padding: '0.50em 0.25em', width: '5%'} },
      {field: 'shipAddress1', body: (this.onRowOrderShipingTo.bind(this)), header: 'Shipping To', sortable: false, filter: false, headerStyle: {padding: '0.50em 0.25em', width: '22%'} },
      {field: 'SaleOrderDetailAutoKey', header: 'Actions', body: (this.onRowActionsTemplate.bind(this)), bodyStyle: _actionStyle, headerStyle: _actionStyle },
    ];
    const dynamicColumns = tableColumns.map((col, i) => {
      return <Column key={col.field} field={col.field} header={col.header} headerStyle={col.headerStyle} sortable={col.sortable} filter={col.filter} filterMatchMode="contains"
                     bodyStyle={col.bodyStyle} expander={col.expander} style={col.expanderStyle} body={col.body} />
    });
    const tableHeader = <SalesOrdersControls tabValue={this.props.tabValue} handleQueryData={this.handleQueryData.bind(this)} rows={this.state.rows} />;

    return (
      <div>
        <br />
        <div className="datagrid implementation">
          <DataTable value={this.state.orders} 
            header={tableHeader} 
            paginator={true} 
            paginatorLeft={""} 
            paginatorRight={""}
            first={this.state.first} 
            rows={this.state.rows} 
            rowsPerPageOptions={[10,30,50,75]}
            lazy={true}
            loading={this.state.loading}
            totalRecords={this.state.rowCount}
            onPage={this.onPageChange.bind(this)} 

            resizableColumns={true}
            columnResizeMode="fit" 
            autoLayout={true}

            // selectionMode="single" 
            // selection={this.state.onSelectedRows} 
            // onSelectionChange={(e) => {this.setState({onSelectedRows: e.data})}} 

            onRowDoubleClick={this.onRowDoubleClickEvent.bind(this)}  

            expandedRows={this.state.onExpandedRows} 
            onRowToggle={(e) => this.setState({onExpandedRows: e.data})}     
            rowExpansionTemplate={this.onRowExpandTemplate.bind(this)} 
            >

            { dynamicColumns }

          </DataTable>
          <br /><br /><br />

          {/* Sidebar Right */}
          <Sidebar visible={this.state.visibleSidebarR} 
            position="right"
            style={{width:'77%', overflow: 'scroll'}}
            baseZIndex={10000} 
            onHide={(e) => this.setState({visibleSidebarR: false, selectedItem: null})}
            dismissable={true}
            showCloseIcon={true}
            blockScroll={true} >
            
            {this.state.visibleSidebarR &&
              <DataGridRowDetails datagridRowData={this.state.selectedItem} tabValue={this.props.tabValue} loading={true} />
            }
            <div className="btn-group" role="group">
              <button type="button" className="btn btn-outline-secondary" onClick={(e) => this.setState({visibleSidebarR: false, selectedItem: null})}>
                Close
              </button>
            </div>
          </Sidebar>    

          {/* Sidebar Full */}
          <Sidebar visible={this.state.visibleSidebarF} 
            fullScreen={true}
            style={{overflow: 'scroll'}}
            baseZIndex={10000} 
            onHide={(e) => this.setState({visibleSidebarF: false, selectedItem: null})}
            dismissable={true}
            showCloseIcon={true}
            blockScroll={true} >

            <div>
              {this.state.visibleSidebarF &&
                <DataGridRowDetails datagridRowData={this.state.selectedItem} tabValue={this.props.tabValue} loading={true} />
              }
              <div className="btn-group" role="group">
                <button type="button" className="btn btn-outline-secondary" onClick={(e) => this.setState({visibleSidebarF: false, selectedItem: null})}>
                  Close
                </button>
              </div>
            </div>
          </Sidebar> 

          {/* Dialog (double-click event) */}
          <Dialog visible={this.state.visibleDialog}
            header={this.props.tabValue} 
            modal={true}
            style={{overflow: 'scroll'}}
            baseZIndex={10000}
            maximizable={true}
            blockScroll={true}
            onHide={(e) => this.setState({visibleDialog: false, selectedItem: null}) } >

            <div>
              {this.state.visibleDialog &&
                <DataGridRowDetails datagridRowData={this.state.selectedItem} tabValue={this.props.tabValue} loading={true} />
              }
              <div className="btn-group" role="group">
                <button type="button" className="btn btn-outline-secondary" onClick={(e) => this.setState({visibleDialog: false, selectedItem: null})}>
                  Close
                </button>
              </div>
            </div>
          </Dialog>

        </div>
      </div>
    );
  }

  render() {
    return (
      this.invoicesDataGrid()
    );
  }

}

function mapStateToProps(state) {
  const {orders, retrievingIN} = state.salesorder;
  return {
    orders, retrievingIN
  };
}

const connectedInvoicesDataGrid = connect(mapStateToProps)(InvoicesDataGrid);
export default connectedInvoicesDataGrid;