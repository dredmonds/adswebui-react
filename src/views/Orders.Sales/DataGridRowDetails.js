import React, {Component} from 'react';
import {Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';
import classnames from 'classnames';

import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Row, Col, InputGroup, Input} from 'reactstrap';

import {salesOrderService} from '../../services';
import {userActions} from '../../actions';

const tableQuery = [{entityTable: 'Orders', objName: 'soNumber'}, 
                    {entityTable: 'Stock', objName: 'saleOrderDetailAutoKey'}, 
                    {entityTable: 'Shipment', objName: 'saleOrderDetailAutoKey'}];
const txnQuery = [{code: 'H', display: 'Lease Adjustment'},
                  {code: 'I', display: 'Brokered Repair'},
                  {code: 'O', display: 'Overhaul'},
                  {code: 'R', display: 'Repair'},
                  {code: 'L', display: 'Labor'},
                  {code: 'T', display: 'Rental Lease Charge'},
                  {code: 'M', display: 'Credit Memo'},
                  {code: 'X', display: 'Misc'},
                  {code: 'J', display: 'Invoice Adjustment'},
                  {code: 'S', display: 'Part Sale'},
                  {code: 'E', display: 'Exchange'},
                  {code: 'A', display: 'Alternate Pricing'},
                  {code: 'F', display: 'Freight'},
                  {code: 'N', display: 'Non-Stock'},
                  {code: 'B', display: 'Rental Lease Part'},
                  {code: 'K', display: 'Kit'},
                  {code: 'C', display: 'Credit'}];
const sysLocalFormat = window.navigator.userLanguage || window.navigator.language;
const dateFormat = {year: 'numeric', month: '2-digit', day: '2-digit'};
const openWindowURI = (uri) => {
  var getPresignedURI = (uriKeyStr) => {
    return new Promise((resolve, reject) => {
      let isKeyStr = uriKeyStr.search('http');
      if(isKeyStr == 0) {
        window.open(uriKeyStr, '_blank');
        return resolve();
      } else {
        var getImagePresignedURL = salesOrderService.getShipmentImagePresignedURL(uriKeyStr);
        getImagePresignedURL.then(result => {
          window.open(result, '_blank');
          return resolve(result.data);
        }, err => {return reject(err)});
      }
    });
  } 
  return uri != 'Choose...' ? getPresignedURI(uri) : null;
};
const filterImagesFileExt = (fileType) => {
  let imagesFileExt = ['.pdf','.jpg','.png'];
  return imagesFileExt.filter(val => {
    return val == fileType;
  })
}

class DataGridRowDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      loadingOrderData: true,
      loadingShipmentData: true,
      orderDetailsData: [],
      shipmentDetailsData: [],
      shipmentDetailsCount: 0,
      shipmentImageGUID: '',
    };
  }

  componentDidMount() {
    let rowObj = this.props.datagridRowData;
    this.getOrderDetailsList(rowObj);
  }

  toggleTab(tab) {
    if(this.state.activeTab !== tab) {
      this.setState({activeTab: tab});
    }
  }

  onRowTxnTypeTemplate(rowObj) {
    let txnCodeDisplay = txnQuery.filter(txn => txn.code == rowObj.routeCode);
    return (
      <div> { 
        txnCodeDisplay[0].display 
       } 
      </div>
    );
  }

  onRowShipDateFormatTemplate(rowObj) {
    let date = new Date(rowObj.shipDate);
    return (
      <div> { rowObj.shipDate && new Intl.DateTimeFormat(sysLocalFormat, dateFormat).format(date) } </div>
    );
  }
  
  onRowAWBDocsFormatTemplate(rowData) {
    var {shipDocsData} = rowData;
    const filterAWB = (item) => {
      let {imageCode, fileExtension} = item;
      let toUpperImageCode = imageCode ? imageCode.toUpperCase() : null , toLowerFileExt = fileExtension ? fileExtension.toLowerCase() : null;
      return toUpperImageCode == 'AWB' && filterImagesFileExt(toLowerFileExt) ? true : false ;
    }
    const shipAWBOptions = shipDocsData.map(item => {
      return filterAWB(item) == true ? <option key={item.ImlAutoKey} value={item.imageGUID}>{`AWB ${item.ImlAutoKey}`}</option> : false ;
    });
    const filterShipAWBOptions = shipAWBOptions.filter(shipAWBOpts => {
      return shipAWBOpts != false ? shipAWBOpts : false ;
    });

    return (
      <div>
        {filterShipAWBOptions != false && 
          <InputGroup size="sm">
            <Input type="select" id="shipmentAWB" bsSize="sm" onChange={(e) => openWindowURI(e.target.value) }>
              <option defaultValue={`URI`}>Choose...</option>
              {filterShipAWBOptions}
            </Input>
          </InputGroup>
        }
      </div>
    );
  }

  onRowTrackAirwayBillFormatTemplate(rowData) {
    var {trackAirwayBill} = rowData;
    return(
      <div>
        {trackAirwayBill &&
          <div className="btn-group" role="group">
            <button type="button" className="btn btn-link" onClick={(e) => openWindowURI(trackAirwayBill) }>
              <i className="pi pi-window-maximize"></i>
            </button>
          </div>
        }
      </div>
    );
  }

  onRowPODFormatTemplate(rowData) {
    var {shipDocsData} = rowData;
    const filterPOD = (item) => {
      let {imageCode, fileExtension} = item;
      let toUpperImageCode = imageCode ? imageCode.toUpperCase() : null , toLowerFileExt = fileExtension ? fileExtension.toLowerCase() : null;
      return toUpperImageCode == 'POD' && filterImagesFileExt(toLowerFileExt) ? true : false ;
    }
    const shipPODOptions = shipDocsData.map(item => {
      return filterPOD(item) == true ? <option key={item.ImlAutoKey} value={item.imageGUID}>{`POD ${item.ImlAutoKey}`}</option> : false ;
    });
    const filterShipPODOptions = shipPODOptions.filter(shipPODOpts => {
      return shipPODOpts != false ? shipPODOpts : false ;
    });

    return (
      <div>
        {filterShipPODOptions != false &&
          <InputGroup size="sm">
            <Input type="select" id="shipmentPOD" bsSize="sm" onChange={(e) => openWindowURI(e.target.value) }>
              <option defaultValue={`URI`}>Choose...</option>
              {filterShipPODOptions}
            </Input>
          </InputGroup>
        }
      </div>
    );
  }

  onRowCusInvFormatTemplate(rowData) {
    var {shipDocsData} = rowData;
    const filterCusInv = (item) => {
      let {imageCode, fileExtension} = item;
      let toUpperImageCode = imageCode ? imageCode.toUpperCase() : null , toLowerFileExt = fileExtension ? fileExtension.toLowerCase() : null;
      return toUpperImageCode == 'CUSTOMS INV' && filterImagesFileExt(toLowerFileExt) ? true : false ;
    }
    const shipCusInvOptions = shipDocsData.map(item => {
      return filterCusInv(item) ? <option key={item.ImlAutoKey} value={item.imageGUID}>{`CUSTOMS INV ${item.ImlAutoKey}`}</option> : false ;
    });
    const filterShipCusInvOptions = shipCusInvOptions.filter(shipCusInvOpts => {
      return shipCusInvOpts != false ? shipCusInvOpts : false ;
    });

    return (
      <div>
        {filterShipCusInvOptions != false &&
          <InputGroup size="sm">
            <Input type="select" id="shipmentCusInv" bsSize="sm" onChange={(e) => openWindowURI(e.target.value) }>
              <option defaultValue={`URI`}>Choose...</option>
              {filterShipCusInvOptions}
            </Input>
          </InputGroup>
        }
      </div>
    );
  }

  onRowImagesFormatTemplate(rowData) {
    var {shipImagesData} = rowData;
    const filterCerts = (item) => {
      let {imageCode, fileExtension} = item;
      let toUpperImageCode = imageCode ? imageCode.toUpperCase() : null , toLowerFileExt = fileExtension ? fileExtension.toLowerCase() : null;
      return toUpperImageCode == 'CERTS' && filterImagesFileExt(toLowerFileExt) ? true : false ;
    }
    const filterPics = (item) => {
      let {imageCode, fileExtension} = item;
      let toUpperImageCode = imageCode ? imageCode.toUpperCase() : null , toLowerFileExt = fileExtension ? fileExtension.toLowerCase() : null;
      return toUpperImageCode == 'PICS' && filterImagesFileExt(toLowerFileExt) ? true : false ;
    }
    const shipImagesOptions = shipImagesData.map(item => {
      return filterCerts(item) ? <option key={item.ImlAutoKey} value={item.imageGUID}>{`CERTS ${item.ImlAutoKey}`}</option> :
                            filterPics(item) ? <option key={item.ImlAutoKey} value={item.imageGUID}>{`PICS ${item.ImlAutoKey}`}</option> : false ;
    });
    const filterShipImagesOptions = shipImagesOptions.filter(shipImagesOpts => {
      return shipImagesOpts != false ? shipImagesOpts : false ;
    });

    return (
      <div>
        {filterShipImagesOptions != false &&
          <InputGroup size="sm">
            <Input type="select" id="shipmentImages" bsSize="sm" onChange={(e) => openWindowURI(e.target.value) }>
              <option defaultValue={`URI`}>Choose...</option>
              {filterShipImagesOptions}
            </Input>
          </InputGroup>
        }
      </div>
    );
  }

  onRowNxtShipDateFormatTemplate(rowObj) {
    let date = new Date(rowObj.nextShipDate);
    return (
      <div> { rowObj.nextShipDate && new Intl.DateTimeFormat(sysLocalFormat, dateFormat).format(date) } </div>
    );
  }

  onRowPriceFormatTemplate(rowObj) {
    const sysLocalFormat = window.navigator.userLanguage || window.navigator.language;
    let options = {minimumFractionDigits: 2, maximumFractionDigits: 2};
    return (
      <div style={{textAlign: 'right'}}> {
        new Intl.NumberFormat(sysLocalFormat, options).format(rowObj.customerPrice)
       } 
      </div>
    );
  }

  getOrderDetailsList(rowObj) {
    const getOrderDetailsData = (objData) => {
      return new Promise((resolve, reject) => {
        let {strQryA, objNameQryA, pageRows, skip} = objData;
        let getOrdersList = salesOrderService.getOrdersList(strQryA, objNameQryA, pageRows, skip);
        getOrdersList.then(result => {
          let ordersListResult = result.data.sort((a, b) => (a.itemNumber > b.itemNumber) ? 1 : -1);
          return resolve(ordersListResult);
        }, err => {return reject(err)});
      });
    }
    const getShipmentDetailsData = (ordersDataList) => {
      return new Promise((resolve, reject) => {
        let getShipmentDetailsDataObj = this.getShipmentStockData(ordersDataList);
        getShipmentDetailsDataObj.then(dataRes => {
          return resolve(dataRes);
        }, err => {return reject(err)});
      });
    }
    const doGetOrdersShipmentDetailsData = (ordersData) => {
      return new Promise((resolve, reject) => {
        getOrderDetailsData(ordersData).then(orderListDataRes => {
          // Set response results to orderDetailsData
          this.setState({orderDetailsData: orderListDataRes, loadingOrderData: false, loadingShipmentData: true});
          getShipmentDetailsData(orderListDataRes).then(shipmentDetailsDataRes => {
            this.setState({loadingShipmentData: false});
            return resolve(shipmentDetailsDataRes);
          }, err => {return reject(err)});
        }, err => {return reject(err)});
      });
    }

    return new Promise((resolve, reject) => {
      let {soNumber, numberOfItems} = rowObj, objNameQryA = tableQuery[0].objName;
      let objData = {strQryA: soNumber, objNameQryA: objNameQryA, pageRows: numberOfItems, skip: 0};
      if(numberOfItems > 1) {
        doGetOrdersShipmentDetailsData(objData).then(shipmentDetailsDataRes => {
          return resolve(shipmentDetailsDataRes);
        }, err => {return reject(err)});
      } else {
        let orderData = [{...rowObj}];
        this.setState({orderDetailsData: orderData, loadingOrderData: false, loadingShipmentData: true});
        doGetOrdersShipmentDetailsData(objData).then(shipmentDetailsDataRes => {
          return resolve(shipmentDetailsDataRes);
        }, err => {return reject(err)});
      }
    });
  }

  getShipmentStockData(ordersDataList) {
    this.datasource = [];
    this.setState({shipmentDetailsData: [], shipmentDetailsCount: 0});

    const mergedShipmentDocsStockImagesData = (shipmentData, shipImagesData) => {
      //imageCode(s): 'AWB' = 96, 'POD' = 95, 'CUSTOM INV' = 205, 'CERTS' = 82, 'PICS' = 127
      return new Promise((resolve, reject) => {
        var shipmentDocsStockImagesData = [];
        const filterDocsData = (shipmentImages, sourcePk) => {
          return shipmentImages.filter(value => {
            return value.sourceTable == 'SM_BATCH' && value.sourcePk == sourcePk;
          });
        }
        const filterImagesData = (shipmentImages, sourcePk) => {
          return shipmentImages.filter(value => {
            return value.sourceTable == 'STOCK' && value.sourcePk == sourcePk;
          });
        }
        shipmentData.map((shipDataObj, idx) => {
          let {shipmentBatchAutoKey, stockAutoKey} = shipDataObj;
          let shipDocsDataObj = filterDocsData(shipImagesData, shipmentBatchAutoKey);
          let shipImagesDataObj = filterImagesData(shipImagesData, stockAutoKey);
          let shipmentDetailsData = {...shipDataObj,shipDocsData: shipDocsDataObj , shipImagesData: shipImagesDataObj};
          shipmentDocsStockImagesData.push(shipmentDetailsData);
          if(shipmentData.length == idx+1) {
            return resolve(shipmentDocsStockImagesData);
          }
        }, err => {reject(err)});
      });
    }
    const getShipmentDocsStockImages = (sourcePkQryStr) => {
      //imageCode(s): 'AWB' = 96, 'POD' = 95, 'CUSTOM INV' = 205, 'CERTS' = 82, 'PICS' = 127
      return new Promise((resolve, reject) => {
        let srcQryStr = sourcePkQryStr, srcTbQryStr = `'STOCK','SM_BATCH'`, imgCodeQryStr = `82,95,96,127,205`; 
        var getShipmentStockImagesData = salesOrderService.getShipmentStockImages(srcQryStr, srcTbQryStr, imgCodeQryStr);
        getShipmentStockImagesData.then(shipDocsStockImagesData => {
          return resolve(shipDocsStockImagesData);
        }, err => {reject(err)});
      });
    }
    const mapOrdersData = (ordersData) => {
      return new Promise((resolve, reject) => {
        var shipmentCount = 0, shipStockImagesQryStr = '', shipDocsStockImagesData = [];
        return ordersData.map((order, idx) => {
          const {Shipment, Stock} = order;
          
          const extractShipmentData = (shipData) => {
            var i, srcPkQryStr = '';
            for(i = 0; i < shipData.length; i++) {
              let shipmentBatchAutoKey = shipData[i].shipmentBatchAutoKey;
              let strPadding = i+1 < shipData.length ? ',' : '';
              if(shipmentBatchAutoKey) {
                srcPkQryStr += `${shipmentBatchAutoKey}${strPadding}`;
              } else {
                let paddingZero4Null = '0'; //set value to '0' if shipmentBatchAutoKey == null 
                srcPkQryStr += `${paddingZero4Null}${strPadding}`;
              }
            }
            return srcPkQryStr;
          }
          const extractStockData = (stockData) => {
            var i, srcPkQryStr = '';
            for(i = 0; i < stockData.length; i++) {
              let stockAutoKey = stockData[i].stockAutoKey;
              let strPadding = i+1 < stockData.length ? ',' : '';
              if(stockAutoKey) {
                srcPkQryStr += `${stockAutoKey}${strPadding}`;
              } else {
                let paddingZero4Null = '0'; //set value to '0' if stockAutoKey == null 
                srcPkQryStr += `${paddingZero4Null}${strPadding}`;
              }
            }
            return srcPkQryStr;
          }
          const mergedShipmentStockQueryStr = (srcShipQryStr, srcStockQryStr) => {
              let strPadding = idx+1 < ordersData.length ? ',' : '';
              return `${srcShipQryStr},${srcStockQryStr}${strPadding}`;
          }
          const mergedShipmentStockData = (shipData, stockData) => {
            return new Promise((resolve, reject) => {
              shipData.map((shipObj, idx) => {
                var mergedShipStockObj = {...shipObj, ...stockData[idx]};
                shipDocsStockImagesData.push(mergedShipStockObj);
                if(shipData.length == idx+1) { return resolve(shipDocsStockImagesData)}
              }, err => {reject(err)});
            });
          }

          if(Shipment.length > 0){
            // Shipment Count
            shipmentCount = (ordersData.length >= idx) ? Shipment.length + shipmentCount : idx ;
            
            // Generates quantum image string query
            var shipmentQryStr = extractShipmentData(Shipment), stockQryStr = extractStockData(Stock);
            shipStockImagesQryStr += mergedShipmentStockQueryStr(shipmentQryStr, stockQryStr);

            var mergedShipmentStockDataObj = mergedShipmentStockData(Shipment, Stock);
            mergedShipmentStockDataObj.then(shipStockDataRes => {
              if(ordersData.length == idx+1) {
                let ordersMapDataObj = {sourcePkQryStr: shipStockImagesQryStr, shipmentCount: shipmentCount, shipStockDataRes};
                return resolve(ordersMapDataObj);
              }
            });
          } else {
            if(ordersData.length == idx+1 && shipmentCount > 0) {
              shipStockImagesQryStr += '0'; //set value to '0' if Shipment == null
              let ordersMapDataObj = {sourcePkQryStr: shipStockImagesQryStr, shipmentCount: shipmentCount, shipStockDataRes: shipDocsStockImagesData};
                return resolve(ordersMapDataObj);
            } else if(ordersData.length == idx+1 && shipmentCount == 0) {
              let ordersMapDataObj = {sourcePkQryStr: shipStockImagesQryStr, shipmentCount: shipmentCount, shipStockDataRes: []};
                return resolve(ordersMapDataObj);
            }
          }

        }, err => {reject(err)});
      });
    }
    const doShipmentDocsStockImagesData = (ordersData) => {
      return new Promise((resolve, reject) => {
        var mapOrdersDataObj = mapOrdersData(ordersData);
        mapOrdersDataObj.then(ordersDataRes => {
          var {sourcePkQryStr, shipmentCount, shipStockDataRes} = ordersDataRes;
          if(shipmentCount > 0) {
            var getShipmentDocsStockImagesObj = getShipmentDocsStockImages(sourcePkQryStr);
            getShipmentDocsStockImagesObj.then(shipDocsStockImagesDataRes => {
              var mergedShipmentDocsStockImagesDataObj = mergedShipmentDocsStockImagesData(shipStockDataRes, shipDocsStockImagesDataRes.data);
              mergedShipmentDocsStockImagesDataObj.then(shipmentDetailsDocsImagesData => {
                let shipmentDetailsData = {shipmentCount: shipmentCount, shipmentDetailsDocsImagesData};
                return resolve(shipmentDetailsData);
              });
            }, err => {reject(err)});
          } else {
            let shipmentDetailsData = {shipmentCount: shipmentCount, shipmentDetailsDocsImagesData: []};
            return resolve(shipmentDetailsData);
          }

        }, err => {reject(err)});
      });
    }

    return new Promise((resolve, reject) => {
      return doShipmentDocsStockImagesData(ordersDataList).then(ordersMapDataRes => {
        let {shipmentCount, shipmentDetailsDocsImagesData} = ordersMapDataRes;
        this.setState({shipmentDetailsData: shipmentDetailsDocsImagesData, shipmentDetailsCount: shipmentCount}, () => {
          return resolve(shipmentDetailsDocsImagesData);
        });
      }, err => {return reject(err)});
    });
  }

  httpErrorHandler(error) {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if(httpStatus >= 500 || httpStatus == 404) {  // Added to handle error(s)
      // this.props.dispatch(alertActions.error('Network Error.') );
      this.setState({loadingOrderData: false, loadingShipmentData: false});
    } else if(httpStatus == 401) {
      userActions.msalLogout()
    } else {
      // this.props.dispatch(failure(error.toString()) );
      // this.props.dispatch(alertActions.error(error.toString()) );
      this.setState({loadingOrderData: false, loadingShipmentData: false});
    }
  }

  orderDetailsTemplate() {
    const tableColumn = [
      {field: 'routeCode', header: 'Txn Type', body: (this.onRowTxnTypeTemplate.bind(this)), style: {width: '9%', fontSize: '11px'}},
      {field: 'pnUpper', header: 'Part No', style: {width: '11%', fontSize: '10px'}},
      {field: 'itemNumber', header: 'Item No', style: {width: '6%', fontSize: '11px', textAlign: 'center'}},
      {field: 'qtyOrdered', header: 'Qty Ordered', style: {width: '8%', fontSize: '11px', textAlign: 'center'}},
      {field: 'qtyInvoiced', header: 'Qty Shipped', style: {width: '8%', fontSize: '11px', textAlign: 'center'}},
      {field: 'qtyReserved', header: 'Await Shipping', style: {width: '8%', fontSize: '11px', textAlign: 'center'}},
      {field: 'nextShipDate', header: 'Next Ship Date', body: (this.onRowNxtShipDateFormatTemplate.bind(this)), style: {width: '10%', fontSize: '11px', textAlign: 'center'}},
      {field: 'descriptionUpper', header: 'Part Desc', style: {width: '15%', fontSize: '10px'}},
      {field: 'customerPrice', header: 'Price', body: (this.onRowPriceFormatTemplate.bind(this)), style: {width: '10%', fontSize: '11px'}},
      {field: 'currencyCode', header: 'Currency', style: {width: '7%', fontSize: '11px', textAlign: 'center'}},
      {field: null, header: 'Invoice Docs', style: {width: '8%', fontSize: '11px'}},
    ];
    const dynamicColumns = tableColumn.map((col, i) => {
      return <Column key={col.field} field={col.field} header={col.header} headerStyle={col.style}
                     body={col.body} bodyStyle={col.style} />
    });

    return (
      <div className="row">
        <div className="col-12">
          <DataTable value={this.state.orderDetailsData} lazy={true} loading={this.state.loadingOrderData} 
                     resizableColumns={true} autoLayout={true} >
            {dynamicColumns}
          </DataTable>
        </div>
      </div>
    );
  }

  shippingDetailsTemplate() {
    const tableColumn = [
      {field: 'itemNumber', header: 'Item No', style: {width: '7%', fontSize: '11px', textAlign: 'center'}},
      {field: 'airwayBill', header: 'Airway Bill', style: {width: '13%', fontSize: '11px', textAlign: 'center'}},
      {field: 'shipDocsData', header: 'Airway Bill Docs', body: (this.onRowAWBDocsFormatTemplate.bind(this)), style: {width: '8%', fontSize: '11px'}},
      {field: 'trackAirwayBill', header: 'Track Airway Bill', body: (this.onRowTrackAirwayBillFormatTemplate.bind(this)), style: {width: '12%', fontSize: '11px', textAlign: 'center'}},
      {field: 'shipDate', header: 'Shipment Date', body: (this.onRowShipDateFormatTemplate.bind(this)), style: {width: '11%', fontSize: '11px', textAlign: 'center'}},
      {field: 'shipDocsData', header: 'POD Image', body: (this.onRowPODFormatTemplate.bind(this)), style: {width: '8%', fontSize: '11px'}},
      {field: 'shipDocsData', header: 'Customs Invoice', body: (this.onRowCusInvFormatTemplate.bind(this)), style: {width: '10%', fontSize: '11px'}},
      {field: 'invocieNumber', header: 'Invoice No', style: {width: '11%', fontSize: '11px', textAlign: 'center'}},
      {field: 'serialNumber', header: 'Serial No', style: {width: '11%', fontSize: '11px', textAlign: 'center'}},
      {field: 'shipImagesData', header: 'Images', body: (this.onRowImagesFormatTemplate.bind(this)), style: {width: '9%', fontSize: '11px'}},
    ];
    const dynamicColumns = tableColumn.map((col, i) => {
      return <Column key={col.field} field={col.field} header={col.header} headerStyle={col.style}
                     body={col.body} bodyStyle={col.style} />
    });

    return (
      <div className="row">
        <div className="col-12">
          <DataTable value={this.state.shipmentDetailsData} lazy={true} loading={this.state.loadingShipmentData}
                     resizableColumns={true} autoLayout={true} >
            {dynamicColumns}
          </DataTable>
        </div>
      </div>
    );
  }

  dataGridRowTemplate() {
    let tabValue = this.props.tabValue;
    let rowObj = this.props.datagridRowData;
    
    return  (
      <div className="p-grid p-fluid" style={{padding: '0.25em'}}>
        <div className="animated fadeIn">
        { rowObj &&
            <div>
              { tabValue == 'Orders' && <h4 style={{fontWeight:'bold'}}>{`Sales Order No:`} {rowObj.soNumber}</h4>}
              { tabValue == 'Exchanges' && <h4 style={{fontWeight:'bold'}}>{`Customer PO No:`} {rowObj.companyRefNumber}</h4>}
              { tabValue == 'Invoices' && <h4 style={{fontWeight:'bold'}}>{`${tabValue.substring(0,tabValue.length -1)} No:`} {rowObj.invocieNumber}</h4>}
                <div>
                  <Row>
                    <Col xs="12" sm="4" lg="4">
                      {tabValue == 'Orders' && <div> <div>Customer PO No.</div> <small className="text-muted">{rowObj.companyRefNumber}</small> </div>}
                      {tabValue == 'Exchanges' && <div> <div>Sales Order No.</div> <small className="text-muted">{rowObj.soNumber}</small> </div>}
                      {tabValue == 'Invoices' && <div> <div>Customer PO No.</div> <small className="text-muted">{rowObj.companyRefNumber}</small> </div>}
                    </Col>
                    <Col xs="12" sm="4" lg="4">
                      <div>For-The-Attention-Of(FAO)</div>
                      <small className="text-muted">{rowObj.attention}</small>
                    </Col>
                    <Col xs="12" sm="4" lg="4">
                      <div>Status</div>
                      { rowObj.openFlag == 'F' && <small className="text-muted" style={{padding: "0.5px 0.5px", textAlign: "center"}}>CLOSED</small>}
                      { rowObj.openFlag == 'T' && <small className="text-muted" style={{padding: "0.5px 0.5px", textAlign: "center"}}>OPEN</small>}
                    </Col>
                  </Row>
                  <Row style={{paddingTop: '5px'}}>
                    <Col xs="12" sm="12" lg="12">
                      <div>Ship To</div> <small className="text-muted">{`${rowObj.companyName}, ${rowObj.shipAddress1}, ${rowObj.shipAddress2}`}</small>
                    </Col>
                  </Row>
                </div> 
              <br />
            </div>
          }
          
          <hr />
          <Nav className="nav nav-pills">
            <NavItem className="px-3">
              <NavLink className={classnames({ active: this.state.activeTab === '1' })}
                      onClick={() => { this.toggleTab('1'); }} style={{cursor: 'pointer'}}>
                  <i className="fa fa-list-alt" />&nbsp;Order Details
              </NavLink>
            </NavItem>
            <NavItem className="px-3">
              <NavLink className={classnames({ active: this.state.activeTab === '2' }) }
                      onClick={() => { this.toggleTab('2'); }} style={{cursor: 'pointer'}}>
                  {this.state.loadingShipmentData && <i className="fa fa-spinner fa-pulse fa-fw" />}&nbsp;&nbsp;
                  <i className="fa fa-plane" />&nbsp;Shipping Details&nbsp;&nbsp;
                  {!this.state.loadingShipmentData &&
                    <span className="badge badge-pill badge-primary">{this.state.shipmentDetailsCount}</span>
                  }
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={this.state.activeTab} style={{borderStyle: 'none'}}>
            <TabPane tabId="1" style={{padding: '0em'}}>
              <div>{this.orderDetailsTemplate()}</div>
            </TabPane>
            <TabPane tabId="2" style={{padding: '0em'}}>
              <div>{this.shippingDetailsTemplate()}</div>
            </TabPane>
          </TabContent>
          <hr />  
        </div>  
      </div>
    );   
  }

  render() {
    return (
      <div>
        { this.props.datagridRowData && this.dataGridRowTemplate() }
      </div>
    );
  }
}

export default DataGridRowDetails;
