import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Row, Col, InputGroup, InputGroupAddon, InputGroupButtonDropdown, Input, 
        Button,
        DropdownToggle,
        DropdownMenu, DropdownItem
      } from 'reactstrap';
import {salesOrderService} from '../../services';
import {userActions, alertActions} from '../../actions';
import {entityDataGenerator} from '../../helpers';

const tableQuery = 
  [{tabVal: 'Orders', objName: 'soNumber', displayVal: 'Sales Order No.'}, 
   {tabVal: 'Exchanges', objName: 'companyRefNumber', displayVal: 'Customer PO No.'}, 
   {tabVal: 'Invoices', objName: 'invocieNumber', displayVal: 'Invoice No.'},
   {tabVal: 'PartNo', objName: 'pnUpper', displayVal: 'Part No.'},
   {tabVal: 'SerialNo', objName: 'serialNumber', displayVal: 'Serial No.'}];


class SalesOrdersControls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      splitButtonOpen: false,
      queryStr: '',
      queryObjName: null,
      searchBy: null,
      data: [],
      searching: false,
    };
    // Binds methods
    this.salesOrdersControls = this.salesOrdersControls.bind(this);
  }
  
  // Methods of events
  componentWillMount() {
    this.timer =  null;
  }

  toggleSplit(event) {
    this.setState({
      splitButtonOpen: !this.state.splitButtonOpen
    });
  }

  onChange(event) {
    const {value} = event.target;
    clearTimeout(this.timer);
    this.setState({queryStr: value, searching: true}, () => {
      this.timer = setTimeout(() => {
        this.onSearch(value);
      },1000);
    });
  }

  onFilterQuery(key) {
    return tableQuery.filter(value => {return key == value.tabVal ? value.objName : null;} )
  }

  onSearch(queryStr) {
    const tabValue = this.props.tabValue;
    if(!this.state.searchBy) {
      var queryObjName = this.onFilterQuery(tabValue);
    } else {
      let searchBy = this.state.searchBy;
      var queryObjName = tableQuery.filter(value => {return searchBy == value.displayVal ? value.objName : null} );
    }

    let strQryA = queryStr;
    let objNameQryA = queryObjName[0].objName;
    let strQryB = 1;                // added to filter an orders with line item 1 only
    let objNameQryB = 'itemNumber'; // added to filter queries to eliminates duplicate entries
    let rows = this.props.rows;     // set this current grid rows to reduced time of query
    let skip = 0;                   // set 0 to start on beginning of index

    const soControlSearch = objNameQryA != 'invocieNumber' ? 
              salesOrderService.searchOrders(strQryA, objNameQryA, strQryB, objNameQryB, rows, skip) :
              salesOrderService.searchInvoicedList(strQryA, objNameQryA, rows, skip);
    
    soControlSearch.then(result => {
      if(result.data.length > 0) {
        this.doQuerySearch(result, objNameQryA, rows, skip);
      } else {
        this.setState({data: {count: 0, data: []}, queryObjName: objNameQryA, rows: rows, skip: skip, searching: false}, this.updateHandleQueryData);
      }
    }, error => {this.httpErrorHandler(error)});
  }

  doQuerySearch(result, objNameQryA, rows, skip) {
    const tabValue = this.props.tabValue;
    if(objNameQryA != 'invocieNumber') {
      if(tabValue != 'Invoices') {
        this.setState({data: result, queryObjName: objNameQryA, rows: rows, skip: skip, searching: false}, this.updateHandleQueryData);
      } else {
        this.doLinkOrderToShipment(result, objNameQryA, rows, skip);
      }
    } else {
      this.doLinkShipmentToOrder(result, objNameQryA, rows, skip);
    }
  }

  doLinkOrderToShipment(result, objNameQryA, rows, skip) {
    let getInvoiceDataGen = entityDataGenerator.linkOrderToShipment(result.data);
    getInvoiceDataGen.then(invoicedData => {
      // set actual invoiced data results for data count's where order sometimes no invoice yet
      let invoicedDataObj = {count: invoicedData.length, data: invoicedData};
      this.setState({data: invoicedDataObj, queryObjName: objNameQryA, rows: rows, skip: skip, searching: false}, this.updateHandleQueryData);
    }, error => {this.httpErrorHandler(error)});
  }

  doLinkShipmentToOrder(result, objNameQryA, rows, skip) {
    let getInvoiceDataGen = entityDataGenerator.linkShipmentToOrder(result.data);
    getInvoiceDataGen.then(invoicedData => {
      // set result.count for data count's where shipment has invoices
      let invoicedDataObj = {count: result.count, data: invoicedData};
      this.setState({data: invoicedDataObj, queryObjName: objNameQryA, rows: rows, skip: skip, searching: false}, this.updateHandleQueryData);
    }, error => {this.httpErrorHandler(error)});
  }

  httpErrorHandler(error) {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if(httpStatus >= 500 || httpStatus == 404) {  // Added to handle error(s)
      this.props.dispatch(alertActions.error('Network Connection Error.') );
    } else if(httpStatus == 401) {
      userActions.msalLogout()
    } else {
      this.props.dispatch(failure(error.toString()) );
      this.props.dispatch(alertActions.error(error.toString()) );
    }
  }

  updateHandleQueryData() {
    this.props.handleQueryData(this.state);
  }

  salesOrdersControls() {
    const tabValue = this.props.tabValue;
    const optionValue = this.onFilterQuery(tabValue);
    const searchMenu = [{value: `${optionValue[0].objName}`, display: `${optionValue[0].displayVal}`},
                        {value: 'pnUpper', display: 'Part No.'},
                        // {value: 'serialNumber', display: 'Serial No.'}
                      ];

    return (
      <div>
        <InputGroup>
          <InputGroupButtonDropdown  addonType="prepend" isOpen={this.state.splitButtonOpen} toggle={(e) => this.toggleSplit(e)}>
            {!this.state.searchBy && <Button><i className="fa fa-search"/>&nbsp;&nbsp;Search By {optionValue[0].displayVal}</Button> }
            {this.state.searchBy && <Button><i className="fa fa-search"/>&nbsp;&nbsp;Search By {this.state.searchBy}</Button> }

            <DropdownToggle split outline />
            <DropdownMenu onClick={(e) => this.setState({searchBy: e.target.value, queryStr: ''}) } >
              {
                searchMenu.map((table) => 
                  <DropdownItem key={table.value} value={table.display}>
                    {table.display}
                  </DropdownItem>
                )
              }
            </DropdownMenu>

          </InputGroupButtonDropdown>

          <Input type="text" name="keySearch" id="inputKey" placeholder="key search..." onChange={this.onChange.bind(this)} value={this.state.queryStr} />
          {this.state.searching && <div style={{padding: '5px 0'}} className="text-center">&nbsp;&nbsp;<i className="fa fa-spinner fa-pulse fa-1x fa-fw" /></div>}
        </InputGroup>
      </div>
    );
  }

  render() {
    return (
      this.salesOrdersControls()
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const connectedSalesOrdersControls = connect(mapStateToProps)(SalesOrdersControls);
export default connectedSalesOrdersControls;