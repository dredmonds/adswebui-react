import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Container, Form, FormFeedback, Row, Col, Card, CardBody, CardFooter, Button, Input, InputGroup, InputGroupAddon, InputGroupText} from 'reactstrap';

import { userActions } from '../../actions';

class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        company: '',
        username: '',
        email: '',
        phone: ''
      }, submitted: false
    };
    // Binds method of events
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNavLink = this.handleNavLink.bind(this);
  }

  // Methods of events
  handleChange(event) {
    const { name, value } = event.target;
    const { user } = this.state;

    this.setState({
      user: {
        ...user,
        [name]: value
      }
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.setState({ submitted: true });
    const { user } = this.state;
    const { dispatch } = this.props;
    if(user.company && user.username && user.email && user.phone) {
      dispatch(userActions.register(user));
    }
  }

  handleNavLink() {
    this.props.history.push('/');
  }

  render() {
    const { registering } = this.props;
    const { user, submitted } = this.state;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form name="registerForm" id="registerForm" onSubmit={this.handleSubmit}>
                    <h1>Register</h1>
                    <p className="text-muted">Create your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-briefcase"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" name="company" id="company" placeholder="Company" invalid={submitted && !user.company} value={user.company} onChange={this.handleChange} />
                      <FormFeedback>Company name is required</FormFeedback>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" name="username" id="username" placeholder="Username" invalid={submitted && !user.username} value={user.username} onChange={this.handleChange} />
                      <FormFeedback>Username is required</FormFeedback>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-envelope"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" name="email" id="email" placeholder="Email" invalid={submitted && !user.email} value={user.email} onChange={this.handleChange} />
                      <FormFeedback>Email address is required</FormFeedback>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-screen-smartphone"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" name="phone" id="phone" placeholder="Mobile Number" invalid={submitted && !user.phone} value={user.phone} onChange={this.handleChange} />
                      <FormFeedback>Mobile number is required</FormFeedback>
                    </InputGroup>
                    <Row>
                      <Col xs="12" sm="6">
                        <Button color="primary" block>
                          {registering && <i className="fa fa-spinner fa-pulse fa-fw" />}
                          Create Account
                        </Button>
                      </Col>
                      <Col xs="12" sm="6">
                        <Button onClick={this.handleNavLink} color="danger" block>Cancel</Button>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
                {/* <CardFooter className="p-4">
                  <Row>
                    <Col xs="12" sm="12">
                      <Button className="icon-windows" block><span>Office 365</span></Button>
                    </Col>
                    <Col xs="12" sm="6">
                      <Button className="btn-twitter" block><span>twitter</span></Button>
                    </Col>
                  </Row>
                </CardFooter> */}
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { registering } = state.registration;
  return { registering };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export default connectedRegisterPage;