import React, { Component } from 'react';
import {connect} from 'react-redux';
import config from 'config';//

import {Row, Col, Card, CardTitle, CardBody, ListGroup, ListGroupItem} from 'reactstrap';

import {salesOrdersActions, pbhOrdersActions} from '../../actions';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
  }

  onClickDashboardSO() {
    window.location.href = '#/orders/sales';
  }

  render() {
    const {retrievingSO, retrievingSH, retrievingIN} = this.props;
    const data = this.props.orders;
    const user = this.props.user;
    if(!data && !retrievingSO) {
      let dataRowsCache = config.baseDataGridRows * 3; //set buffer to 3x from baseDataGridRows
      this.props.dispatch(salesOrdersActions.getSalesOrders(dataRowsCache.toString()));
      this.props.dispatch(pbhOrdersActions.getPBHOrders(dataRowsCache.toString()));
    }
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12" lg="12">
            <Card>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6" lg="6">
                    <CardTitle><i className="icon-speedometer" />&nbsp;&nbsp;{user && `${user.company.name} Dashboard`}</CardTitle>
                    {/* <CardSubtitle>ADS Dashboard</CardSubtitle> */}
                  </Col>
                  <Col xs="12" sm="6" lg="6" className="text-lg-right text-sm-right text-xs-left"></Col>
                </Row>

                <Row>
                  <Col xs="12" sm="6" lg="4">
                    <div className="dashboard-box orders" onClick={this.onClickDashboardSO.bind(this)}>
                      <i className="fa fa-list-alt" />
                      <div className="chart-wrapper">
                        {/* <Line data={makeSocialBoxData(0)} options={socialChartOpts} height={90}/> */}
                      </div>
                      {retrievingSO && 
                        <div style={{padding: '25px 0'}}>
                          <span className="fa fa-spinner fa-pulse fa-fw"></span>&nbsp;loading...
                        </div> 
                      }
                      {data &&
                        <ul>
                          <li>
                            <strong>{data.sOrders.count}</strong>
                            <span>Orders</span>
                          </li>
                          <li>
                            <strong>{data.pOrders.count}</strong>
                            <span>Exchanges</span>
                          </li>
                          <li>
                            <strong>{data.invoices.count}</strong>
                            <span>Invoices</span>
                          </li>
                        </ul>
                      }
                    </div>
                  </Col>

                </Row>
              </CardBody>

            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const {orders, retrievingSO} = state.salesorder;
  const {user} = state.authentication;
  return {
    orders, retrievingSO, user
  };
}

const connectedDashboardPage = connect(mapStateToProps)(Dashboard)
export default connectedDashboardPage;
