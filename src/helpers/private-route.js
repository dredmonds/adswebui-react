// src/helpers/private-route.js

import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import config from 'config';

export const PrivateRoute = ({ component: Component, ...rest  }) => (
  // verify 'user' entry at sessionStorage, then redirect if false
  <Route {...rest} render={props => (
    sessionStorage.getItem(`ads.user.id.key${config.adsMsalConfig.clientID}`) 
      ? <Component {...props} /> 
      : <Redirect to={{ pathname: '/login', state: {from: props.location} }} />
  )} />
)