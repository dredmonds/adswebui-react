import {salesOrderService, pbhOrderService} from './../services';

export const entityDataGenerator = {
  linkShipmentToOrder,
  NEWlinkShipmentToOrder,
  linkOrderToShipment,
  NEWlinkOrderToShipment,
  genSalesOrdersSummary,
  genPBHOrdersSummary
};

const odataEntityQuery = 
  [{tabVal: 'Orders', objNameQryA: 'routeCode', strQryA: 'S', objNameQryB: 'itemNumber', strQryB: 1}, 
   {tabVal: 'Exchanges', objNameQryA: 'routeCode', strQryA: 'E', objNameQryB: 'itemNumber', strQryB: 1}, 
   {tabVal: 'Invoices', objNameQryA: 'invocieNumber', strQryA: ''}];

function linkShipmentToOrder(shipmentData) {
  var invData = [];
  let invoicesMap = new Promise((resolve, reject) => {
    return shipmentData.map(invObj => {
      var {invocieNumber} = invObj;
      let strQryA = invObj.saleOrderDetailAutoKey;
      let objNameQryA = 'SaleOrderDetailAutoKey';

      const getOrderDetails = salesOrderService.searchOrdersList(strQryA, objNameQryA, 1, 0);

      getOrderDetails.then(orderData => {
        orderData.data.map(ordObj => {
          let invDataObj = {...ordObj, invocieNumber: invocieNumber};
          invData.push(invDataObj); // push data to array
        });
        if(invData.length >= shipmentData.length) {
          // Sort datasource by date created
          invData.sort((a, b) => (b.dateCreated > a.dateCreated) ? 1 : -1);
          return resolve(invData) // return data
        }
      }, err => {reject(err)});

    });
  });

  return new Promise((resolve, reject) => {
    invoicesMap.then(() => {
      return resolve(invData);
    }, err => {
      return reject(err);
    });
  });
}

function NEWlinkShipmentToOrder(shipmentData) {

  const strInvDataQry = (shipData) => {
    var i, invQryStr = '', objNameQry = 'SaleOrderDetailAutoKey';
    for(i = 0; i < shipData.length; i++) {
      let strPadding = i+1 < shipData.length ? ',' : '';
      invQryStr += `${shipData[i].saleOrderDetailAutoKey}${strPadding}`;
    }
    invQryStr = `${objNameQry} in (${invQryStr})`;
    return invQryStr;
  }

  const insertInvNo = (orderData, shipData) => {
    var i, invData = [];
    for(i = 0; i < orderData.length; i++) {
      let invDataObj = {...orderData[i], invocieNumber: shipData[i].invocieNumber};
      invData.push(invDataObj);
    }
    // Sort datasource by date created
    invData.sort((a, b) => (b.dateCreated > a.dateCreated) ? 1 : -1);
    return invData;
  }

  const strQuery = strInvDataQry(shipmentData);
  const getOrdersData = salesOrderService.getOrdersData(strQuery);

  return new Promise((resolve, reject) => {
    getOrdersData.then(ordData => {
      var ordersData = insertInvNo(ordData.data, shipmentData);
      return resolve(ordersData);
    }, err => { 
      return reject(err) 
    });
  });
}

function linkOrderToShipment(orderData) {
  var ordData = [];
  var ordCounter = 0;
  var ordCountNull = 0;
  let orderMap = new Promise((resolve, reject) => {
    return orderData.map(ordObj => {
      var {...orderObj} = ordObj;
      let strQryA = ordObj.SaleOrderDetailAutoKey;
      let objNameQryA = 'saleOrderDetailAutoKey';

      const getShipmentDetails = salesOrderService.searchShipment(strQryA, objNameQryA, 1, 0);

      getShipmentDetails.then(shipmentData => {
        
        ordCounter = shipmentData.data.length > 0 ? ordCounter+1 : ordCounter;
        ordCountNull = shipmentData.data.length == 0 ? ordCountNull+1 : ordCountNull;
        
        shipmentData.data.map(shipObj => {
          let ordDataObj = {...orderObj, invocieNumber: shipObj.invocieNumber};
          ordData.push(ordDataObj); // push data to array
        });
        if(ordCounter >= orderData.length) {
          return resolve(ordData);
        } else if(ordCounter+ordCountNull >= orderData.length) {
          return resolve(ordData);
        }
      }, err => {reject(err)});
    });
  });

  return new Promise((resolve, reject) => {
    orderMap.then(() => {
      return resolve(ordData)
    }, err => {
      return reject(err);
    });
  });
}

function NEWlinkOrderToShipment(orderData) {
  const strSODDataQry = (ordData) => {
    var i, shipQryStr = '', objNameQry = 'saleOrderDetailAutoKey';
    for(i = 0; i < ordData.length; i++) {
      let strPadding = i+1 < ordData.length ? ',' : '';
      shipQryStr += `${ordData[i].SaleOrderDetailAutoKey}${strPadding}`;
    }
    shipQryStr = `${objNameQry} in (${shipQryStr})`;
    return shipQryStr;
  }

  const insertInvNo = (ordData, shipData) => {
    var i, invData = [];
    for(i = 0; i < ordData.length; i++) {
      let invDataObj = {...ordData[i], invocieNumber: shipData[i].invocieNumber};
      invData.push(invDataObj);
    }
    // Sort datasource by date created
    invData.sort((a, b) => (b.dateCreated > a.dateCreated) ? 1 : -1);
    return invData;
  }

  const strQuery = strSODDataQry(orderData);
  const getShipmentData = salesOrderService.getShipmentData(strQuery);

  return new Promise((resolve, reject) => {
    getShipmentData.then(shipmentData => {
      var ordersData = insertInvNo(orderData, shipmentData.data);
      return resolve(ordersData);
    }, err => {
      return reject(err);
    });
  });
}

function genSalesOrdersSummary(rowNums) {
  // Sales Orders Query
  let strQryA = odataEntityQuery[0].strQryA;
  let objNameQryA = odataEntityQuery[0].objNameQryA;  //Get Orders with "S" @routeCode
  let strQryB = odataEntityQuery[0].strQryB; // added to filter an orders with line item 1
  let objNameQryB = odataEntityQuery[0].objNameQryB;
  // Exchanges Query
  let strQryA1 = odataEntityQuery[1].strQryA;
  let objNameQryA1 = odataEntityQuery[1].objNameQryA;  //Get Orders with "E" @routeCode
  let strQryB1 = odataEntityQuery[1].strQryB; // added to filter an orders with line item 1
  let objNameQryB1 = odataEntityQuery[1].objNameQryB;
  // Invoiced Query
  let strQryA2 = odataEntityQuery[2].strQryA;
  let objNameQryA2 = odataEntityQuery[2].objNameQryA;  //Get Orders with >=1 @qtyInvoiced

  const getSOrdersData = salesOrderService.searchOrders(strQryA, objNameQryA, strQryB, objNameQryB, rowNums, 0); // Get SO
  const getPOrdersData = salesOrderService.searchOrders(strQryA1, objNameQryA1, strQryB1, objNameQryB1, rowNums, 0); // Get Exchanges
  const getInvoiceDataList = salesOrderService.searchInvoicedList(strQryA2, objNameQryA2, rowNums, 0); // Get Invoices

  let getSalesOrdersSummary = new Promise((resolve, reject) => {
    return getSOrdersData.then(sOrdersData => {
      var {...sOrders} = sOrdersData;
      getPOrdersData.then(pOrdersData => {
        var {...pOrders} = pOrdersData;
        getInvoiceDataList.then(invoiceData => {
          var {count} = invoiceData;

          // let getInvoicedDataGen = this.linkShipmentToOrder(invoiceData.data);
          let getInvoicedDataGen = this.NEWlinkShipmentToOrder(invoiceData.data);

          getInvoicedDataGen.then(invoicedData => {
            var invoices = {count, data: invoicedData};
            let soDataSummary = {sOrders, pOrders, invoices};
            return resolve(soDataSummary);
          }, err => { return reject(err)});

        }, err => { return reject(err)});
      }, err => { return reject(err)});
    }, err => { return reject(err)});
  });

  return new Promise((resolve, reject) => {
    getSalesOrdersSummary.then(dataSummary => {
      return resolve(dataSummary);
    }, err => {
      return reject(err);
    });
  });
}

function genPBHOrdersSummary(rowNums) {
  return new Promise((resolve, reject) => {
    const getPBHOrdersData = pbhOrderService.getPBHOrdersList(rowNums, 0);

    getPBHOrdersData.then(pbhOrdersData => {
      return resolve(pbhOrdersData);
    }, err => { return reject(err)});
  });
}