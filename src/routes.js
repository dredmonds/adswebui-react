const routes = {
  '/login': 'Login',
  '/register': 'Register',
  '/': 'Home',
  '/dashboard': 'Dashboard',
  '/orders': 'Orders',
  '/orders/sales': 'Sales',
  '/orders/pbh': 'PBH',
  '/orders/adhoc': 'AdHoc',
  '/orders/rfq': 'RFQ',
  '/shipments': 'Shipments',
  '/shipments/shipments': 'Shipments',
  '/inventory': 'Inventory',
  '/inventory/mystock': 'My Stock',
};
export default routes;
