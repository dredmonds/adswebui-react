import React, {Component} from 'react';
import {Link, Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';

import Dashboard from '../../views/Dashboard/';

// Sales Orders
import SalesOrdersMainPage from '../../views/Orders.Sales/';
// PBH Orders
import PBHOrdersMainPage from '../../views/Orders.PBH/';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>
                <Route path="/orders/sales" name="Sales Orders" component={SalesOrdersMainPage}/>
                <Route path="/orders/pbh" name="PBH Orders" component={PBHOrdersMainPage}/>
                <Route path="/orders/adhoc" name="AdHoc Orders" component={SalesOrdersMainPage}/>
                <Route path="/orders/rfq" name="RFQ Orders" component={SalesOrdersMainPage}/>
                <Route path="/shipments/shipments" name="Shipments" component={SalesOrdersMainPage}/>
                <Route path="/inventory/mystock" name="My Stock" component={SalesOrdersMainPage}/>
                <Redirect from="/orders" to="/orders/sales"/>
                <Redirect from="/shipments" to="/shipments/shipments"/>
                <Redirect from="/inventory" to="/inventory/mystock"/>
                <Redirect from="/" to="/dashboard"/>
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Full;
