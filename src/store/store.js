// src/store/store.js

import { createStore, applyMiddleware } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';
import authenticationMiddleware from '../middlewares/authentication.midware';
import salesOrderMiddleware from '../middlewares/salesorder.midware';
import pbhOrderMiddleware from '../middlewares/pbhorder.midware';

const loggerMiddleware = createLogger();
const persistConfig = {
  key: 'root',
  storage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);
const authMidware = authenticationMiddleware();
const salesOrderMidware = salesOrderMiddleware();
const pbhOrderMidware = pbhOrderMiddleware();

export const store = createStore(
  // rootReducer,
  persistedReducer,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
    authMidware,
    salesOrderMidware,
    pbhOrderMidware,
  )
); 