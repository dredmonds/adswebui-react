// src/actions/user.actions.js

import config from 'config';
import { userConstants, salesOrderConstants, alertConstants } from '../constants';
import { alertActions } from './alert.actions';
import { history, adsMsalAuth } from '../helpers';
import { userService } from '../services';

export const userActions = {
  login,
  logout,
  register,
  submitPassword,
  msalLogin,
  msalLogout,
};

function login(username, password) {
  return dispatch => {
    dispatch(request({ username }));

    userService.login(username, password)
    .then(user => {
      // validates "session" and "resultDetail" from http 200 response
      let resultDetail = user.session || user.resultDetail;
      if(resultDetail) {
        dispatch(success( {username, resultDetail} ));
        window.location.href = '#/newpassword';
      } else {
        dispatch(success(user));
        // history.push('/');
        window.location.href = '#/';
      }
    }, error => {
      dispatch(failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    });
  };

  function request(user) { return {type: userConstants.LOGIN_REQUEST, user} }
  function success(user) { return {type: userConstants.LOGIN_SUCCESSS, user} }
  function failure(error) { return {type: userConstants.LOGIN_FAILURE, error} }
}

function logout() {
  userService.logout();
  adsMsalAuth.logout();
  return { type: userConstants.LOGOUT };
}

function register(user) {
  return dispatch => {
    dispatch(request(user));

    userService.register(user)
    .then(user => {
      dispatch(success());
      // history.push('/login');
      dispatch(alertActions.success('Registration successful'));
      window.location.href = '#/login';
    }, error => {
      dispatch(failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    });
  };

  function request(user) { return {type: userConstants.REGISTER_REQUEST, user} }
  function success(user) { return {type: userConstants.REGISTER_SUCCESS, user} }
  function failure(error) { return {type: userConstants.REGISTER_FAILURE, error} }
}

function submitPassword(password, confirmPassword) {
  return dispatch => {
    dispatch(request());

    userService.challengedUser({password, confirmPassword})
    .then(user => {
      dispatch(success());
      dispatch(alertActions.success('Password saved!'));
      window.location.href = '#/login';
    }, error => {
      dispatch(failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    });
  };

  function request(user) { return {type: userConstants.CHALLENGE_REQUEST, user} }
  function success(user) { return {type: userConstants.CHALLENGE_SUCCESS, user} }
  function failure(error) { return {type: userConstants.CHALLENGE_FAILURE, error} }
}

function msalLogin() {
  return dispatch => {
    dispatch(request('MSAL_USER_LOGIN'));
    adsMsalAuth.login().then(response => {
      let {accessToken, ...userObj} = response;
      if(userObj.userPermissions.length > 1) {
        // Set to sessionStorage
        sessionStorage.setItem(`ads.auth.token.key${config.adsMsalConfig.clientID}`, JSON.stringify(accessToken) );
        sessionStorage.setItem(`ads.user.id.key${config.adsMsalConfig.clientID}`, JSON.stringify(userObj) );

        dispatch(success(userObj));
        window.location.href = '#/dashboard';
      }else {
        setTimeout(() => { adsMsalAuth.logout() }, 2000);
        dispatch(failure('No Permissions'));
        dispatch(alertActions.error('Your account has no permissions to use this service.'));
      }
    }, error => {
      dispatch(failure('MSAL_LOGIN_FAILURE'));
      httpErrorHandler(error)
    });
  };

  function request(msalLogin) { return {type: userConstants.MSAL_LOGIN_REQUEST, msalLogin} }
  function success(msalUser) { return {type: userConstants.MSAL_LOGIN_SUCCESSS, msalUser} }
  function failure(error) { return {type: userConstants.MSAL_LOGIN_FAILURE, error} }
}

function msalLogout() {
  userService.logout();
  adsMsalAuth.logout();
  clearState();
}

function clearState() {
  return dispatch => {
    dispatch(clearAlert());
    dispatch(clearAuthUser());
    dispatch(clearStateSO());
  };
  function clearAlert() {return {type: alertConstants.CLEAR}}
  function clearAuthUser() {return {type: userConstants.LOGOUT}}
  function clearStateSO() {return {type: salesOrderConstants.SALES_ORDERS_FAILURE}}
}

function httpErrorHandler(error) {
  return dispatch => {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if(httpStatus >= 500 || httpStatus == 404) {  // Added to handle error(s)
      dispatch(alertActions.error('Network Connection Error.') );
    } else if(httpStatus == 401) {
      userService.logout();
      adsMsalAuth.logout();
      dispatch({type: userConstants.LOGOUT});
    } else {
      dispatch(failure(error.toString()) );
      dispatch(alertActions.error(error.toString()) );
    }
  };
}