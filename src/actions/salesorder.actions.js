// src/actions/salesorders.actions.js

import { salesOrderConstants, userConstants } from '../constants';
import { salesOrderService, userService } from '../services';
import { alertActions } from './alert.actions';
import {adsMsalAuth, entityDataGenerator} from '../helpers';

export const salesOrdersActions = {
  getSalesOrders,
  // getSalesOrderDetails,
  // getSalesOrderSearch
};

function getSalesOrders(rowNums) {
  return dispatch => {
    dispatch(request() );
    const getSalesOrdersSummary = entityDataGenerator.genSalesOrdersSummary(rowNums);

    getSalesOrdersSummary.then(sOrdersData => {
      dispatch(success(sOrdersData) );
    }).catch(err => {
      dispatch(failure(err.toString())); 
      httpErrorHandler(err); 
    });
  };

  function request() { return {type: salesOrderConstants.SALES_ORDERS_REQUEST} }
  function success(orders) { return {type: salesOrderConstants.SALES_ORDERS_SUCCESS, orders} }
  function failure(error) { return {type: salesOrderConstants.SALES_ORDERS_FAILURE, error} }
}

function getSalesOrderDetails(id) {
  return dispatch => {
    dispatch(request(id) );

    salesOrderService.getSalesOrderDetails(id)
    .then(order => {
      dispatch(success(order) );
    }, error => {
      httpErrorHandler(error);
    });
  };

  function request(id) { return {type: salesOrderConstants.SALES_ORDER_DETAILS_REQUEST, id} }
  function success(order) { return {type: salesOrderConstants.SALES_ORDER_DETAILS_SUCCESSS, order} }
  function failure(error) { return {type: salesOrderConstants.SALES_ORDER_DETAILS_FAILURE, error} }
}

function getSalesOrderSearch(query) {
  return dispatch => {
    dispatch(request(query) );

    salesOrderService.getSalesOrderSearch(query)
    .then(orders => {
      dispatch(success(orders) );
    }, error => {
      httpErrorHandler(error);
    });
  };

  function request(query) { return {type: salesOrderConstants.SALES_ORDERS_SEARCH_REQUEST, query} }
  function success(orders) { return {type: salesOrderConstants.SALES_ORDERS_SEARCH_SUCCESS, orders} }
  function failure(error) { return {type: salesOrderConstants.SALES_ORDERS_SEARCH_FAILURE, error} }
}

function httpErrorHandler(error) {
  return dispatch => {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if(httpStatus >= 500 || httpStatus == 404) {  // Added to handle error(s)
      dispatch(alertActions.error('Network Connection Error.') );
    } else if(httpStatus == 401) {
      userService.logout();
      adsMsalAuth.logout();
      dispatch({type: userConstants.LOGOUT});
    } else {
      dispatch(failure(error.toString()) );
      dispatch(alertActions.error(error.toString()) );
    }
  };
}