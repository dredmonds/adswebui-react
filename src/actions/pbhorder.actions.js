// src/actions/phborder.actions.js

import {pbhOrderConstants} from '../constants';
import {pbhOrderService} from '../services';
import {alertActions} from './alert.actions';
import {adsMsalAuth, entityDataGenerator} from '../helpers';

export const pbhOrdersActions = {
  getPBHOrders,
};

function getPBHOrders(rowNums) {
  return dispatch => {
    dispatch(request() );
    const getPBHOrdersSummary = entityDataGenerator.genPBHOrdersSummary(rowNums);

    getPBHOrdersSummary.then(pbhOrdersData => {
      dispatch(success(pbhOrdersData) );
    }).catch(err => {
      dispatch(failure(err.toString())); 
      httpErrorHandler(err); 
    });
  };

  function request() { return {type: pbhOrderConstants.PBH_ORDERS_REQUEST} }
  function success(orders) { return {type: pbhOrderConstants.PBH_ORDERS_SUCCESS, orders} }
  function failure(error) { return {type: pbhOrderConstants.PBH_ORDERS_FAILURE, error} }
}

function httpErrorHandler(error) {
  return dispatch => {
    let httpStatus = error.response != undefined ? error.response.status : 404;
    if(httpStatus >= 500 || httpStatus == 404) {  // Added to handle error(s)
      dispatch(alertActions.error('Network Connection Error.') );
    } else if(httpStatus == 401) {
      userService.logout();
      adsMsalAuth.logout();
      dispatch({type: userConstants.LOGOUT});
    } else {
      dispatch(failure(error.toString()) );
      dispatch(alertActions.error(error.toString()) );
    }
  };
}