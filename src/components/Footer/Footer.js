import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span><a href="http://ajw-group.com">AJW Group</a> &copy; 2018 ADS.</span>
        {/* <span className="ml-auto">Powered by <a href="http://www.ajw-group.com">AJW Group</a></span> */}
      </footer>
    )
  }
}

export default Footer;
