export default {
  items: [
    {
      name: 'ADS Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW'
      }
    },
    {
      name: 'Orders',
      url: '/orders',
      icon: 'fa fa-list-alt',
      children: [
        {
          name: 'Sales (Customer)',
          url: '/orders/sales',
          icon: 'icon-star'
        },
        {
          name: 'PBH (Contracts)',
          url: '/orders/pbh',
          icon: 'icon-star'
        },
        // {
        //   name: 'AdHoc (Repairs)',
        //   url: '/orders/adhoc',
        //   icon: 'icon-star'
        // },
        // {
        //   name: 'RFQ (Consumables)',
        //   url: '/orders/rfq',
        //   icon: 'icon-star'
        // },
      ]
    },
    // {
    //   name: 'Shipments',
    //   url: '/shipments',
    //   icon: 'fa fa-plane',
    //   children: [
    //     {
    //       name: 'Shipments',
    //       url: '/shipments/shipments',
    //       icon: 'icon-star'
    //     },
    //   ]
    // },
    // {
    //   name: 'Inventory',
    //   url: '/inventory',
    //   icon: 'fa fa-cubes',
    //   children: [
    //     {
    //       name: 'My Stock',
    //       url: '/inventory/mystock',
    //       icon: 'icon-star'
    //     },
    //   ]
    // },
    {
      name: 'Logout',
      url: '/login',
      icon: 'icon-logout',
      class: 'mt-auto',
      variant: 'danger',
    }
  ]
};
