import React, {Component} from 'react';
import {connect} from 'react-redux';

import Sidebar from './Sidebar';

class SidebarMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navMenus: []
    };
    
    // Binds methods
    this.navMenus = this.navMenus.bind(this);
  }

  componentDidMount() {
    this.navMenus();
  }

  // Methods of events
  dashboardMenuList() {
    return {
      name: 'ADS Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW'
      }
    };
  }

  orderMenuList() {
    const {user} = this.props;
    let salesObj = user.userPermissions.some(str => str.permission.name == 'Order.Read') 
          ? {name: 'Sales (Customer)', url: '/orders/sales', icon: 'icon-star'} : '';
    let pbhObj = user.userPermissions.some(str => str.permission.name == 'ExchangeOrderRequest.Read')
          ? {name: 'PBH (Contracts)', url: '/orders/pbh', icon: 'icon-star'} : '';
    let adhocObj = user.userPermissions.some(str => str.permission.name == 'RepairOrderRequest.Read')
          ? {name: 'AdHoc (Repairs)', url: '/orders/adhoc', icon: 'icon-star'} : '';
    let rfqObj = user.userPermissions.some(str => str.permission.name == 'ConsumableOrderRequest.Read')
          ? {name: 'RFQ (Consumables)', url: '/orders/rfq', icon: 'icon-star'} : '';

    const orderMenuObj = {
      name: 'Orders',
      url: '/orders',
      icon: 'fa fa-list-alt',
      children: []
    };

    orderMenuObj.children.push(
      salesObj, pbhObj, adhocObj, rfqObj
    );

    return orderMenuObj;
  }

  shipmentMenuList() {
    return {
      name: 'Shipments',
      url: '/shipments',
      icon: 'fa fa-plane',
      children: [
        {
          name: 'Shipments',
          url: '/shipments/shipments',
          icon: 'icon-star'
        },
      ]
    };
  }

  inventoryMenuList() {
    return {
      name: 'Inventory',
      url: '/inventory',
      icon: 'fa fa-cubes',
      children: [
        {
          name: 'My Stock',
          url: '/inventory/mystock',
          icon: 'icon-star'
        },
      ]
    };
  }

  logoutMenuList() {
    return {
      name: 'Logout',
      url: '/login',
      icon: 'icon-logout',
      class: 'mt-auto',
      variant: 'danger',
    };
  }

  navMenus() {
    const navMenuArray = [
        this.dashboardMenuList(),
        this.orderMenuList(),
        this.shipmentMenuList(),
        this.inventoryMenuList(),
        this.logoutMenuList()
    ];
    this.setState({navMenus: navMenuArray}, () => {
      console.log('SidebarMenu:+',this.state.navMenus)
    });
  }

  render() {
    return (
      <div>
        {this.state.navMenus &&
          <Sidebar callbackFromSidebar={this.state.navMenus} />
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {authentication} = state;
  const {user} = authentication;
  return { 
    user
  };
}

const connectedSidebarMenu = connect(mapStateToProps)(SidebarMenu);
export default connectedSidebarMenu;