import React, {Component} from 'react';
import {connect} from 'react-redux';

class SidebarHeader extends Component {

  render() {
    const user = this.props.user;
    // return null
    // Uncomment following code lines to add Sidebar Header
    return (
      <div className="sidebar-header">
        {user && <div>{user.company.name}</div>}
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

const connectedSidebarHeader = connect(mapStateToProps)(SidebarHeader);
export default connectedSidebarHeader;
