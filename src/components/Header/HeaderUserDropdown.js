// src/components/HeaderUserDropdown.js

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  ListGroup, ListGroupItem,
  Button, ButtonGroup, Popover
} from 'reactstrap';

import { userActions } from '../../actions'

class HeaderUserDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = { dropdownOpen: false};
    // Binds methods
    this.dropAccnt = this.dropAccnt.bind(this);
    this.userToggle = this.userToggle.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
    this.userLogout = this.userLogout.bind(this);    
  }

  // Methods of events
  userToggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  userLogout() {
    // this.setState({dropdownOpen: !prevState.dropdownOpen});
    this.props.dispatch( userActions.logout() );
  }

  onMouseLeave() {
    this.setState({dropdownOpen: false});
  }  

  dropAccnt(user) {
    return (
      <ButtonGroup>
        <Button id="userPopover" color="link" onClick={this.userToggle}>
          <div className="text-center">
            <i className="fa fa-user-o fa-lg">&nbsp;</i>
            <i className="fa fa-angle-down">&nbsp;</i>
          </div>
        </Button>
        <Popover placement="bottom" isOpen={this.state.dropdownOpen} target="userPopover" toggle={this.userToggle} onClick={this.userToggle} >
          <ListGroup flush>
            <ListGroupItem header="true">
              <div className="text-center">
                <img src={'img/avatars/userImg.png'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
              </div>
              <br/>
              {
                user &&
                <div className="text-left">
                  <p><i className="icon-user" />&nbsp;&nbsp;{user.name}</p>
                  <p><i className="icon-envelope" />&nbsp;&nbsp;{user.email}</p>
                </div>
              }
            </ListGroupItem>
            <ListGroupItem tag="a" onClick={this.userLogout} action><i className="icon-logout"></i>&nbsp;&nbsp;Logout</ListGroupItem>
          </ListGroup>
        </Popover>
      </ButtonGroup>
    );
  }

  render() {
    const { user } = this.props
    return (
      this.dropAccnt(user)
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

const connectedHeaderUserDropdown = connect(mapStateToProps)(HeaderUserDropdown);
export default connectedHeaderUserDropdown;