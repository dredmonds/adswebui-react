import React from 'react';
import { HashRouter, Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from './helpers';
import { alertActions } from './actions';
import { PrivateRoute } from './helpers';

// Styles
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../scss/style.scss'
// Temp fix for reactstrap
import '../scss/core/_dropdown-menu-right.scss'
// Import Primereact Set
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

// Containers
import FullHomePage from './containers/Full/'

// Views
import LoginPage from './views/Login/'
import RegisterPage from './views/Register/'
import PasswordPage from './views/Password'

class App extends React.Component {
  constructor(props) {
    super(props);
    const {dispatch} = this.props;

    history.listen((location, action) => {
      // clear alert upon location change
      dispatch( alertActions.clear() );
    });
  }

  render() {
    const { alert } = this.props;
    return (
      <div>
        { 
          alert.message &&
          <div className={`alert ${alert.type}`} style={{display: 'flex', justifyContent: 'center', position: 'absolute', opacity: 0.7}} >{alert.message}</div>
        }
        <HashRouter>
          <Switch history={history}>
            <Route path="/login" name="Login Page" component={LoginPage} />
            <Route path="/register" name="Register Page" component={RegisterPage} />
            <Route path="/newpassword" name="Password Page" component={PasswordPage}/>
            <PrivateRoute path="/" name="Home" component={FullHomePage} />
          </Switch>
        </HashRouter>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return { alert };
}
const connectedApp = connect(mapStateToProps)(App);
export default connectedApp;