// src/middlewares/salesorder.midware.js
import config from 'config';
import {entityDataGenerator} from '../helpers';
import {salesOrderConstants} from '../constants';
import {userActions} from '../actions';

const {API, adsMsalConfig, adsDataPollTime} = config;

export default function salesOrderMiddleware() {
  var timer = null; // set timer to null upon transition

  return ({dispatch, getState}) => next => (action) => {
    const {user} = getState().authentication;
    const adsToken = sessionStorage.getItem(`ads.user.id.key${adsMsalConfig.clientID}`);

    if(!adsToken || !user) {return next(action)}// return next if no token
    
    const pollADSData =  async () => {
      let {user} = getState().authentication;

      if(user) {
        // implement polling data (await)
      }
    };

    const updateSalesOrder = async () => {
      let {user} = getState().authentication;
      
      if(user) {
        let {tokenExp} = user;
        let idTokenExp = tokenExp ? tokenExp.exp : null;
        // get current time in miliseconds
        let currentTime = Math.round( (new Date().getTime() / 1000) );

        if(tokenExp && idTokenExp >= currentTime) {
          pollADSData(); // poll ads data
        }

        clearTimeout(timer);
        timer = setTimeout(updateSalesOrder, adsDataPollTime * 60000); // set time interval
      } else {
        return userActions.msalLogout(); // forced to logout if not authenticated
      }
    };

    adsToken && user ? updateSalesOrder() : null;
    return next(action); // return next action
  };
}