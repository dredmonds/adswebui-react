// src/middlewares/authentication.midware.js
import config from 'config';
import {adsMsalAuth} from '../helpers';
import {userConstants} from '../constants';
import {userActions} from '../actions';

const {API, adsMsalConfig, tokenRefreshTime} = config;

export default function authenticationMiddleware() {
  var timer = null; // set timer to null upon transition

  return ({dispatch, getState}) => next => (action) => {
    const {user} = getState().authentication;
    const adsToken = sessionStorage.getItem(`ads.user.id.key${adsMsalConfig.clientID}`);

    if(!adsToken || !user) {return next(action)}

    const refreshToken = async () => {
      let {user} = getState().authentication;
      let getNewToken = adsMsalAuth.getTokenSilent(adsMsalConfig.b2cScopes);

      if(user) {
        getNewToken.then(accessToken => {
          let {tokenExp, ...userObj} = getState().authentication.user;
          let msalUser = {
                ...userObj, 
                'tokenExp': { // set new time expiration 1+ hour
                  'exp': Math.round( (new Date().getTime() + 3600000) / 1000) 
              }
          };
          sessionStorage.setItem(`ads.auth.token.key${adsMsalConfig.clientID}`, JSON.stringify(accessToken) );
          return dispatch({type: userConstants.MSAL_LOGIN_SUCCESSS, msalUser})
        }, error => {
          // do next action to continue, error handling e.g. "alert message" is other alternative
          return next(action); // return next action
        });
      }
    }

    const updateAuth = async () => {
      let {user} = getState().authentication;

      if(user) {
        let {tokenExp} = user;
        let idTokenExp = tokenExp ? tokenExp.exp : null;
        // added 5 minutes from now before refreshing token
        let refreshThreshold = Math.round( (new Date().getTime() + 300000) / 1000);
        
        if(tokenExp && refreshThreshold >= idTokenExp) {
          refreshToken(); // refresh token
        }
        clearTimeout(timer);
        timer = setTimeout(updateAuth, tokenRefreshTime * 60000); // set time interval
      } else {
        return userActions.msalLogout(); // forced to logout if not authenticated
      }
    };

    adsToken && user ? updateAuth() : null;
    return next(action); // return next action
  };
}