// src/middlewares/pbhorder.midware.js
import config from 'config';
import {entityDataGenerator} from '../helpers';
import {pbhOrderConstants} from '../constants';
import {userActions} from '../actions';

const {API, adsMsalConfig, adsDataPollTime, baseDataGridRows} = config;

export default function pbhOrderMiddleware() {
  var timer = null; // set timer to null upon transition

  return ({dispatch, getState}) => next => (action) => {
    const {user} = getState().authentication;
    const adsToken = sessionStorage.getItem(`ads.user.id.key${adsMsalConfig.clientID}`);

    if(!adsToken || !user) {return next(action)}// return next if no token

    const pollADSData =  async () => {
      let {user} = getState().authentication;

      if(user) {
        // implement polling data (await)
      }
    };
    
    const updatePBHOrder = async () => {
      let {user} = getState().authentication;

      if(user) {
        let {tokenExp} = user;
        let idTokenExp = tokenExp ? tokenExp.exp : null;
        // get current time in miliseconds
        let currentTime = Math.round( (new Date().getTime() / 1000) );
  
        if(tokenExp && idTokenExp >= currentTime) {
          pollADSData(); // poll ads data
        }
  
        clearTimeout(timer);
        timer = setTimeout(updatePBHOrder, adsDataPollTime * 60000); // set time interval
      } else {
        return userActions.msalLogout(); // forced to logout if not authenticated
      }
    };

    adsToken && user ? updatePBHOrder() : null;
    return next(action); // return next action
  };
}